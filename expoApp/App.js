import React from 'react'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import {
	View,
	StyleSheet,
	TouchableWithoutFeedback,
	Keyboard,
} from 'react-native'
import { SafeAreaView } from 'react-navigation'
import reducer from './src/redux/reducer'
import MainRouter from './src/router/MainRouter'
import Database from './src/database'
import NateStatusBar from './src/component/StatusBar'
import { setTheme } from './src/redux/actions'

const store = createStore(reducer)

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
})

export default class App extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			data: new Database(),
			isLoaded: false,
		}
	}

	async componentDidMount() {
		await this.state.data.init()

		const theme = await this.state.data.getTheme()
		store.dispatch(setTheme(theme))

		this.setState({ isLoaded: true })
	}

	render() {
		const { data, isLoaded } = this.state

		return (
			<Provider store={store}>
				<NateStatusBar store={store} db={data} />
				<SafeAreaView forceInset={{ top: 'always' }} style={styles.container}>
					<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
						<View style={{ flex: 1 }}>
							{isLoaded ? <MainRouter screenProps={data} /> : <View />}
						</View>
					</TouchableWithoutFeedback>
				</SafeAreaView>
			</Provider>
		)
	}
}
