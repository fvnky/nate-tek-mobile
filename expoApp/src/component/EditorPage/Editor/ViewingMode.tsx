
import React, { Component } from 'react';
import { Base64 } from "js-base64";

import { Editor } from 'slate-react';
import { Value } from "slate";
import cx from 'classnames';
import TextareaAutosize from 'react-autosize-textarea';

import defaultValue from '../../../constants/defaultValue';

import IconEditFromViewer from '../../../assets/icons/IconEditFromViewer';
import IconBack from '../../../assets/icons/IconBack';
import { FOLDER, SUBJECT, DRAFT, DRAFT_ID } from '../../../database/constants';

import styles from './Editor.less';
import VMstyles from './ViewingMode.less';


// Typescript
import {
   ICourse,
   ISubject,
   IFolder,
   EditorTab,
   ToastOptions,
   ToggleModal
} from '../../../typescript/interface';

import PdfViewer from './plugins/pdf/PdfViewer';

import CollapseOnEscape from './plugins/collapse-on-escape';
import SoftBreak from './plugins/soft-break';
import EditorShortcuts from './plugins/editorShortcuts';
import getEntireText from './plugins/getEntireText';
import HoverMenu from './plugins/hoverMenu/hoverMenu';
import Paragraph from './plugins/paragraph/paragraph';
import BasicFormatting from './plugins/basicFormatting';
import Highlight from './plugins/highlight/highlight';
import List from './plugins/list/list';
import Heading from './plugins/heading/heading';
import HtmlPaste from './plugins/htmlPaste';
import SuperScript from './plugins/superScript';
import pdf from './plugins/pdf/pdf';
import image from './plugins/image/image';
import Divider from './plugins/divider/divider';
import Quote from './plugins/quote/quote';
import InsertMenu from './plugins/insertMenu/insertMenu.js';
import Outline from './plugins/outline/outline.js';

interface IProps {
   edit: (docId: string) => void;
   back: () => void;
   db: any;
   theme: string;
   course: ICourse;
   courses: ICourse[];
   subjects: ISubject[];
   folders: IFolder[];
   // activeTab?: EditorTab;
}

interface IState {
   value: any;
}

export default class ViewingMode extends Component<IProps, IState> {
   editor: any;

   plugins = [
      // EditorShortcuts({ nateEditor: this }),
      Paragraph(),
      SoftBreak(),
      CollapseOnEscape(),
      BasicFormatting(),
      Heading(),
      HtmlPaste(),
      SuperScript(),
      image(),
      pdf(),
      Highlight(),
      List(),
      Divider(),
      Quote(),
      // HoverMenu(),
      getEntireText(),
      // InsertMenu()
      // Outline()
   ];

   state = {
      value: null
   }

   componentDidMount() {
      const { db, course, back } = this.props;

      if (!course) {
         back();
         return;
      }
      db._main
         .get(course._id, { attachments: true })
         .then((doc: any) => {
            const base64 = doc._attachments["value.json"].data;
            const value = Value.fromJSON(JSON.parse(Base64.decode(base64)));
            this.setState({ value });
         })
         .catch(err => true);
   }

   getParent = (course: ICourse) => {
      const { subjects, folders } = this.props;

      if (course.parent.collection === SUBJECT) {
         return subjects.find(s => s._id === course.parent.id);
      } else {
         return folders.find(f => f._id === course.parent.id);
      }
   };

   closePdfViewer = () => {
      if (this.editor) this.editor.closePdfViewer();
   };

   setRef = (editor: any) => {
      this.editor = editor;
   };

   onClickEdit = () => {
      const {edit, course} = this.props;
      if (course)
         edit(course._id)
   }

   render() {
      const { theme, course, back } = this.props;
      const { value } = this.state;

      if (!value) return <></>;
      const parentName = course.parent.collection === DRAFT ? 'Draft' : this.getParent(course).name;
      const pdfViewer = value && value.data ? value.data.get('pdfViewer') : null;
      return (
         <>
            <IconBack onClick={back} className={cx(theme, VMstyles.backButton)} />
            <div className={cx(theme, styles.editor5, VMstyles.editor)} >
               <div className={styles.parentFolder} >
                  <div className={cx(theme, styles.parentName)}>{parentName}</div>
               </div>
               <TextareaAutosize
                  placeholder="Untitled"
                  className={cx(theme, styles.courseNameTextArea)}
                  value={course.name}
                  readOnly={true}
               />
               <Editor
                  placeholder="Type something awesome ..."
                  style={{ minHeight: '1000px', cursor: 'text' }}
                  value={value || defaultValue}
                  ref={this.setRef}
                  plugins={this.plugins}
                  spellCheck={true}
                  readOnly={true}
               />
               {pdfViewer && pdfViewer.visible && (
                  <PdfViewer close={this.closePdfViewer} file={pdfViewer.file} />
               )}
            </div>
            <IconEditFromViewer onClick={this.onClickEdit} className={cx(theme, VMstyles.editButton)} />
         </>
      );
   }
}
