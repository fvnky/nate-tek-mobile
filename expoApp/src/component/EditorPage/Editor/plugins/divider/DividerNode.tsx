import React, { Component } from 'react';
import cx from 'classnames';

import withTheme from '../../../../withTheme';

import styles from './DividerNode.less';

interface IProps {
   theme: string;
   isFocused: boolean;
}

class DividerNode extends Component<IProps> {
   render() {
      const { isFocused, theme } = this.props;

      return (
         <div
            className={cx(
               theme,
               styles.divider,
               isFocused ? styles.focused : ''
            )}
         />
      );
   }
}
export default withTheme(DividerNode);
