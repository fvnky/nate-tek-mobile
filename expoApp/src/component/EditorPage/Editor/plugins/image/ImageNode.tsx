import React, { Component } from 'react';
import cx from 'classnames';

import TextareaAutosize from 'react-autosize-textarea';
import ImagePositionToolTip from './ImagePositionToolTip';
import ImageViewer from './ImageViewer';

import IconArrowBottomRight from "../../../../../assets/icons/IconArrowBottomRight"

import withTheme from "../../../../withTheme"

import {
   IMAGE_STYLE_CENTER,
   IMAGE_STYLE_EXTENDED,
   IMAGE_STYLE_LEFT
} from './image';

import styles from './ImageNode.less';

function getClassName(style: string) {
   switch (style) {
      case IMAGE_STYLE_CENTER:
         return styles.imageCenter;
      case IMAGE_STYLE_EXTENDED:
         return styles.imageExtended;
      case IMAGE_STYLE_LEFT:
         return styles.imageLeft;
   }
}

function capValue(value: number, min: number, max: number) {
   if (value > max) return max;
   if (value < min) return min;
   return value;
}

interface IProps {
   theme: string;
   isFocused: boolean;
   editor: any;
   node: any;
   attributes: any;
}

interface IState {
   viewer: boolean;
   dragging: null | { originX: number, originY: number, offsetX: number, offsetY: number };
}

class ImageNode extends Component<IProps, IState> {
   trick: HTMLDivElement;

   state = {
      viewer: false,
      dragging: null
   }

   componentDidUpdate() {
      if (this.props.isFocused) {
         window.addEventListener("mousemove", this.handleMouseMove);
         window.addEventListener("mouseup", this.handleMouseUp);
      } else if (!this.props.isFocused) {
         window.removeEventListener("mousemove", this.handleMouseMove);
         window.removeEventListener("mouseup", this.handleMouseUp);
      }
   }

   componentWillUnmount() {
      window.removeEventListener("mousemove", this.handleMouseMove);
      window.removeEventListener("mouseup", this.handleMouseUp);
   }

   handleMouseDown = (event: React.MouseEvent) => {
      this.setState({ dragging: { originX: event.screenX, originY: event.screenY, offsetX: 0, offsetY: 0 } })
   }

   handleMouseMove = (event: MouseEvent) => {
      if (this.state.dragging) {
         this.setState({
            dragging: {
               originX: this.state.dragging.originX,
               originY: this.state.dragging.originY,
               offsetX: event.screenX - this.state.dragging.originX,
               offsetY: event.screenY - this.state.dragging.originY
            }
         })
      }
   }

   handleMouseUp = (event: MouseEvent) => {
      const { editor, node } = this.props;
      if (this.state.dragging && this.trick) {
         const style = node.data.get("style");
         const width = node.data.get("width") || 80; // For old documents;
         editor.setImageWidth(node, capValue((width + ((this.state.dragging.offsetX / this.trick.clientWidth) * 100 * (style !== IMAGE_STYLE_LEFT ? 2 : 1))), 20, 100));
         this.setState({ dragging: null });
      }
   }

   onClickImage = () => {
      const { isFocused } = this.props;
      if (isFocused) this.setState({ viewer: true });
   };

   closeImageViewer = () => {
      const { editor } = this.props;
      this.setState({ viewer: false });
      editor.focus();
   };

   onClickInput = (e: any) => {
      e.stopPropagation();
      e.preventDefault();
      e.target.focus();
   };

   onInputChange = (e: any) => {
      const { editor, node } = this.props;
      editor.setNodeByKey(node.key, {
         data: node.data.merge({ caption: e.target.value })
      });
   };

   render() {
      const { isFocused, node, attributes, theme, editor } = this.props;
      const { viewer, dragging } = this.state;
      const style = node.data.get("style");
      const className = getClassName(style);
      const src = node.data.get("src");
      const caption = node.data.get("caption");
      let width = node.data.get("width") || 80; // For old documents
      if (this.state.dragging && this.trick) {
         width = capValue((width + ((this.state.dragging.offsetX / this.trick.clientWidth) * 100 * (style !== IMAGE_STYLE_LEFT ? 2 : 1))), 20, 100)
      }

      return (
         <div className={styles.layout}>
            <div className={styles.trickToGetWidth} ref={(d) => { this.trick = d; }} />
            <div
               className={cx(styles.image, className)}
               contentEditable={false}
               style={{ width: width + '%' }}
            >
               <ImagePositionToolTip theme={theme} node={node} editor={editor} style={style} isFocused={(isFocused && editor.value.fragment.text === "") || viewer} />
               <img
                  className={cx(theme, styles.img, isFocused || viewer ? styles.focused : "", dragging ? styles.dragging : "")}
                  src={src}
                  {...attributes}
                  onClick={this.onClickImage}
               />
               {
                  (isFocused || viewer) && (style !== IMAGE_STYLE_EXTENDED) &&
                  <div className={styles.dragHandleContainer}>
                     <span onMouseDown={this.handleMouseDown} className={cx(theme, styles.dragHandle)}>
                        <IconArrowBottomRight />
                     </span>
                  </div>
               }
               <TextareaAutosize
                  placeholder={(isFocused) ? "caption" : ""}
                  className={cx(theme, styles.caption)}
                  onClick={this.onClickInput}
                  value={caption}
                  onChange={this.onInputChange}
               />
               <ImageViewer show={viewer} theme={theme} editor={editor} src={src} caption={caption} close={this.closeImageViewer} />
            </div>
         </div>
      );
   }
}
export default withTheme(ImageNode);
