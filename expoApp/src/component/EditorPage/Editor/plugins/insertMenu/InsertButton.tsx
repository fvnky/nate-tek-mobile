import React, { useEffect, useState } from 'react';
import cx from 'classnames';

import styles from './InsertButton.less';

interface IProps {
   theme: string;
   icon: any;
   label: string;
   onClick?: () => void;
}

const InsertButton: React.FC<IProps> = ({
   theme,
   label,
   icon,
   onClick
}) => {
   return (
      <div className={cx(theme, styles.layout)} onClick={onClick}>
         <icon.type className={styles.icon} />
         <div className={styles.label}>{label}</div>
      </div>
   );
};
export default InsertButton;
