
import React from 'react'
import PlusButton from "./PlusButton";

export default function insertMenu(options = {}) {
  return {
    renderEditor(props, editor, next) {
      const children = next();

      return (
        <>
          <>{children}</>
            <PlusButton editor={editor} value={editor.value} />
         </>
      )
    },
  }
}
