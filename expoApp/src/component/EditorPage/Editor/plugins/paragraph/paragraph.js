import React from "react";

import { DEFAULT_NODE } from "../../constants";


export default function paragraph() {
   return {
      queries: {
         // getNodeForPlusButton(editor) {
         //    const selectedNode = editor.value.blocks.first();
         //    if (selectedNode && editor.value.selection.anchor.offset === 0 && selectedNode.text === "") {
         //       // eslint-disable-next-line react/no-find-dom-node
         //       return findDOMNode(selectedNode)
         //    }
         //    return null;
         // },
      },
      onKeyDown(event, editor, next) {
         // if (event.key === "Backspace"
         //    && editor.value.blocks.size === 1
         //    && editor.value.blocks.get(0).type !== DEFAULT_NODE
         //    && editor.value.blocks.get(0).text === "") {
         //    editor.setBlocks({ type: DEFAULT_NODE })
         //    return;
         // }
         // if (event.key === "Enter"
         //    && editor.value.blocks.size === 1
         //    && editor.value.blocks.get(0).type !== DEFAULT_NODE
         //    && editor.value.blocks.get(0).text !== ""
         //    && editor.value.selection.anchor.offset === editor.value.blocks.get(0).text.length
         //    ) {
         //    editor.insertBlock({ type: DEFAULT_NODE })
         //    return;
         // }
         if (event.key === "Backspace"
            && editor.value.document.nodes.size === 2
            && editor.value.blocks.get(0).type !== DEFAULT_NODE
            && editor.value.blocks.get(0).text === "") {
            editor.removeNodeByKey(editor.value.blocks.get(0).key).moveToStartOfDocument()
            return;
         }
         if (event.key === "Enter"
            && editor.value.blocks.size === 1
            && editor.value.blocks.get(0).type !== DEFAULT_NODE
            && editor.value.blocks.get(0).text !== ""
         ) {
            editor.splitBlock().setBlocks({ type: DEFAULT_NODE })
            return;
         }
         return next();
      },
      renderNode(props, editor, next) {
         const { attributes, children, node, isFocused } = props;
         if (node.type === DEFAULT_NODE) {
            const selectedNode = editor.value.blocks.first();
            let show = false;
            if (
               // isFocused &&
                selectedNode &&
                editor.value.selection.anchor.offset === 0 &&
                selectedNode.text === "" &&
                selectedNode.key === node.key
                )
               show = true;
            return (
               <p {...attributes} id={show ? "plus-button-p-node" : null}>{children}</p>
            );
         }
         return next();
      }
   };
}
