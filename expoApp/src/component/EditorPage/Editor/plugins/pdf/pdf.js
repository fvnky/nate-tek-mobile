import React from "react";
import { Block } from "slate";

import PdfDocument from "./PdfDocument";

import { DEFAULT_NODE } from "../../constants";

export const PDF = "pdf";

export default function pdf(options = { }) {
   return {
      schema: {
         document: {
            last: { type: DEFAULT_NODE },
            normalize: (editor, { code, node, child }) => {
               // eslint-disable-next-line default-case
               switch (code) {
                  case "last_child_type_invalid": {
                     const paragraph = Block.create(DEFAULT_NODE);
                     return editor.insertNodeByKey(node.key, node.nodes.size, paragraph);
                  }
               }
            }
         },
         blocks: {
            pdf: {
               isVoid: true
            }
         }
      },
      commands: {
         insertPdf(editor, name, path, target) {
            if (target) {
               editor.select(target);
            }
            editor.insertBlock({
               type: PDF,
               data: { name, path }
            });
         },
         showPdfViewer(editor, file, page) {
            editor.setData({pdfViewer: { visible: true, file, page }});
         },
         closePdfViewer(editor) {
            editor.setData({pdfViewer: { visible: false }});
         }
      },
      queries: {
         isPdf(editor) {
            return editor.value.blocks.some(node => node.type === PDF);
         }
      },
      renderNode(props, editor, next) {
         const { attributes, node, isFocused } = props;
         switch (node.type) {
            case PDF: {
               const name = node.data.get("name");
               const src = node.data.get("path");
               return <PdfDocument isFocused={isFocused} name={name} file={src} editor={editor}/>;
            }
            default: {
               return next();
            }
         }
      }
   };
}
