import { isKeyHotkey } from "is-hotkey";

export default function SoftBreak() {
   return {
     onKeyDown(event, editor, next) {
       if (isKeyHotkey("shift+Enter")(event)) {
         editor.insertText('\n')
       } else {
         next()
       }
     },
   }
 }
