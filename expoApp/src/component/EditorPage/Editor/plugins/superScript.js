import React from "react";
import { isKeyHotkey } from "is-hotkey";

export const SUPERSCRIPT = "superscript";

export default function superScript(options = {}) {
   return {
      commands: {
         toggleSuperscript(editor) {
            editor.toggleMark(SUPERSCRIPT);
         },
      },
      queries: {
         isSuperscript(editor) {
            return editor.value.activeMarks.some(mark => mark.type === SUPERSCRIPT)
         }
      },
      onKeyDown(event, editor, next) {
         // let mark;

         // if (isKeyHotkey("mod+b")(event)) {
         //    mark = BOLD;
         // } else if (isKeyHotkey("mod+i")(event)) {
         //    mark = ITALIC;
         // } else if (isKeyHotkey("mod+u")(event)) {
         //    mark = UNDERLINED;
         // } else {
         //    return next();
         // }

         // event.preventDefault();
         // editor.toggleMark(mark);
         return next();
      },
      renderMark(props, editor, next) {
         const { children, mark, attributes } = props;

         switch (mark.type) {
            case SUPERSCRIPT:
               return <sup {...attributes}>{children}</sup>;
            default:
               return next();
         }
      }
   };
}
