import React from 'react';
import cx from 'classnames';
import styles from './FormatButton.less';

interface IProps {
   theme: string;
   icon?: any;
   text?: string;
   callback?: () => void;
   isActive?: any;
}

export default function FormatButton(props: IProps) {
   const {
      theme,
      icon,
      text,
      isActive = () => false,
      callback = () => 1
   } = props;

   const onMouseDown = event => {
      event.preventDefault();
      event.stopPropagation();
      callback();
   };

   return (
      <span
         role="button"
         className={cx(theme, styles.iconWrapper)}
         onMouseDown={onMouseDown}
      >
         <icon.type
            className={cx(theme, isActive() ? styles.svgActive : styles.svg)}
         />
      </span>
   );
}
