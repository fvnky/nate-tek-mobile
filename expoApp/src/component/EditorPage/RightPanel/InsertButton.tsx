import React from "react";
import cx from "classnames";
import styles from "./InsertButton.less";

const renderInsertButton = (theme: string, Icon: any, name: string, callback: () => void, isActive: () => boolean = () => false) => {
   const active = isActive();
   return (
      <div className={styles.insertWrapperWrapper}>
         <div role="button" className={cx(theme, styles.insertWrapper, active ? styles.insertWrapperActive : "")} onMouseDown={callback}>
            <Icon className={styles.insertIcon} />
            <span className={styles.legend}>{name}</span>
         </div>
      </div>
   );
};
export default renderInsertButton;
