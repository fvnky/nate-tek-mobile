import * as React from 'react'
import { View, StyleSheet, StatusBar } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import { connect } from 'react-redux'
import { setTheme } from '../redux/actions'

const styles = StyleSheet.create({
	StatusBar: {
		backgroundColor: '#222B45',
		height: 40,
		marginBottom: -45,
	},
})

const lightMode = StyleSheet.create({
	StatusBar: {
		backgroundColor: '#FEFFFE',
		height: 40,
		marginBottom: -45,
	},
})

const darkMode = StyleSheet.create({
	StatusBar: {
		backgroundColor: '#222B45',
		height: 40,
		marginBottom: -45,
	},
})

class NateStatusBar extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		const theme = this.props.myStore.theme ? lightMode : darkMode

		return (
			<View style={theme.StatusBar}>
				<StatusBar
					barStyle={this.props.myStore.theme ? 'dark-content' : 'light-content'}
				/>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	const { myStore } = state
	return { myStore }
}

export default connect(mapStateToProps)(NateStatusBar)
