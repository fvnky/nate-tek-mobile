import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Feather from 'react-native-vector-icons/Feather'

export default class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const style = this.props.theme ? lightMode: darkMode;
    const color = this.props.theme ? '#000': '#fff';

    return (
      <View style={style.container}>
        <View style={style.navbar}>
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Feather style={style.Icon} name="menu" size={30} color={color} ></Feather>
          </TouchableOpacity>
          <Text style={style.title}>{this.props.title}</Text>
          <TouchableOpacity>
            {/* <Feather style={style.Icon} name="search" size={30} color={color} ></Feather> */}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const lightMode = StyleSheet.create({
  container: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    elevation: 3,
    borderBottomWidth: 1,
    borderColor: '#F0F3F8',
  },
  navbar: {
    flex: 1,
    backgroundColor: '#FEFFFE',
    flexDirection: "row",
    justifyContent: 'space-between',
  },
  title: {
    color: "#29324B",
		letterSpacing: 10,
    fontWeight: '100',
    // marginLeft: 25, // Si l'iconne de recherche est active, remettre cette valeur
    marginLeft: -35,
    fontSize: 25,
    paddingVertical: 10,
  },
  Icon: {
    height: 30,
    width: 30,
    margin: 10,
    color: '#222B45'
  },
});

const darkMode = StyleSheet.create({
  container: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    elevation: 3,
    borderBottomWidth: 1,
    borderColor: '#151A30',
  },
  navbar: {
    flex: 1,
    backgroundColor: '#222B45',
    flexDirection: "row",
    justifyContent: 'space-between',
  },
  title: {
    color: "#FEFEFE",
		letterSpacing: 10,
    fontWeight: '100',
    // marginLeft: 25, // Si l'iconne de recherche est active, remettre cette valeur
    marginLeft: -35,
    fontSize: 25,
    paddingVertical: 10,
  },
  Icon: {
    height: 30,
    width: 30,
    margin: 10,
    color: '#FEFFFE'
  },
});
