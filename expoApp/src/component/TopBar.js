import * as React from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'

export default class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const style = this.props.theme? lightMode: darkMode;
    
    return (
      <View style={style.bar}>
        <View style={style.container}>
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Icon style={style.Icon} name="bars" size={35} color={this.props.theme ? "#000": "#fff"} ></Icon>
          </TouchableOpacity>
          <Text style={style.title}>{this.props.title}</Text>
          <TouchableOpacity style={[style.Icon, { width: 30 }]}>
            {/* <Icon style={style.Icon} name="search1" size={35} color={this.props.theme ? "#000": "#fff"} ></Icon> */}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const lightMode = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'white',
      flexDirection: "row",
      justifyContent: 'space-between',
  },

  bar : {
    height: "8%",
    width: "100%",
    flexDirection: "column",
    justifyContent: 'space-between',
    elevation : 3,
    backgroundColor : "white",
  },

  title: {
    color: "black",
    fontSize: 25,

    paddingVertical: 10,
  },
  
  Icon: {
    marginVertical: 10,
    marginHorizontal: 5,
  }, 
});

const darkMode = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#4A4A4A',
      flexDirection: "row",
      justifyContent: 'space-between',
  },

  bar : {
    height: "8%",
    width: "100%",
    flexDirection: "column",
    justifyContent: 'space-between',
    elevation : 3,
    backgroundColor : "#4A4A4A",
  },

  title: {
    color: "#FFFFFF",
    fontSize: 25,

    paddingVertical: 10,
  },
  
  Icon: {
    marginVertical: 10,
    marginHorizontal: 5,
  }, 
});
