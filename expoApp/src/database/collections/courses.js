import _ from 'lodash'

import { COURSE } from '../constants'

import { isNotFound } from '../utils'

import { InvalidParameterFormat, CourseNotFound } from '../errors'

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll() {
	return new Promise((ok, ko) => {
		this._main
			.find({ selector: { collection: COURSE } })
			.then((res) => {
				ok(res.docs)
			})
			.catch(ko)
	})
}

function findById(id) {
	return new Promise((ok, ko) => {
		if (!_.isString(id))
			return ko(new InvalidParameterFormat(`The id parameter is not a String`))

		this._main
			.get(id)
			.then((doc) => {
				if (doc.collection !== COURSE)
					throw new CourseNotFound(
						`The document's id '${doc._id}' does not reference a course`
					)

				ok(doc)
			})
			.catch((err) => {
				if (isNotFound(err))
					throw new CourseNotFound(`The course '${id}' could not be found`)

				throw err
			})
			.catch(ko)
	})
}

export default function() {
	return {
		findAll: findAll.bind(this),
		findById: findById.bind(this),
	}
}
