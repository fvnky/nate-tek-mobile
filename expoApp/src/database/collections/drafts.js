import { DRAFT } from '../constants'

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll() {
	return new Promise((ok, ko) => {
		this._main
			.find({ selector: { 'parent.collection': DRAFT } })
			.then((res) => {
				ok(res.docs)
			})
			.catch(ko)
	})
}

export default function() {
	return {
		findAll: findAll.bind(this),
	}
}
