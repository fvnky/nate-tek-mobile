import { isString } from 'lodash'
import { InvalidParameterFormat, QuizNotFound } from '../errors'
import { isNotFound, parseDoc, parseUpdate, timestamp } from '../utils'
import { QUIZ } from '../constants'
import { QuizSchema } from '../schemas'

function findAll() {
	return new Promise((ok, ko) => {
		this._main
			.find({ selector: { collection: QUIZ } })
			.then((res) => ok(res.docs))
			.catch(ko)
	})
}

function findById() {
	return new Promise((ok, ko) => {
		if (!isString(id))
			return ko(new InvalidParameterFormat(`The id parameter is not a String`))

		this._main
			.get(id)
			.then((doc) => {
				if (doc.collection !== QUIZ)
					throw new QuizNotFound(
						`The document's id '${doc._id}' does not reference a quiz`
					)

				ok(doc)
			})
			.catch((err) => {
				if (isNotFound(err))
					throw new QuizNotFound(`The quiz '${id}' could not be found`)

				throw err
			})
			.catch(ko)
	})
}

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

async function update(doc) {
	await this.fetch()

	const fields = []

	const parsed = await parseDoc(QuizSchema, doc)
	const old = await this._main.get(parsed._id)
	const quiz = await parseUpdate(old, doc, fields)

	quiz.updatedAt = timestamp()
	const res = await this._main.put(quiz)

	const updated = await this._main.get(res.id)
	await this.request.post('/quizs', updated)

	return updated
}

export default function() {
	return {
		update: update.bind(this),
		findAll: findAll.bind(this),
		findById: findById.bind(this),
	}
}
