import _ from 'lodash'

import { SUBJECT } from '../constants'

import {
	isNotFound,
} from '../utils'

import {
	InvalidParameterFormat,
	SubjectNotFound,
} from '../errors'

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll() {
	return new Promise((ok, ko) => {
		this._main
			.find({ selector: { collection: SUBJECT } })
			.then((res) => {
				ok(res.docs)
			})
			.catch(ko)
	})
}

function findById(id) {
	return new Promise((ok, ko) => {
		if (!_.isString(id))
			return ko(
				new InvalidParameterFormat(`The id parameter is not a String`),
			)

		this._main
			.get(id)
			.then((doc) => {
				if (doc.collection !== SUBJECT)
					throw new SubjectNotFound(
						`The document's id '${doc._id}' does not reference a subject`,
					)
				ok(doc)
			})
			.catch((err) => {
				if (isNotFound(err))
					throw new SubjectNotFound(
						`The subject '${id}' could not be found`,
					)

				throw err
			})
			.catch(ko)
	})
}

function findByName(name) {
	return new Promise((ok, ko) => {
		if (!_.isString(name))
			return ko(
				new InvalidParameterFormat(
					`The name parameter is not a String`,
				),
			)

		this._main
			.find({ selector: { collection: SUBJECT } })
			.then((res) => {
				if (_.isEmpty(res.docs)) throw new SubjectNotFound()

				const subject = _.find(res.docs, { name })

				if (!subject) throw new SubjectNotFound()

				ok(subject)
			})
			.catch(ko)
	})
}

export default function() {
	return {
		findAll: findAll.bind(this),
		findById: findById.bind(this),
		findByName: findByName.bind(this),
	}
}
