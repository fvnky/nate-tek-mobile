import { NATE_TODO } from '../constants'
import { TodoSchema } from '../schemas'

import { parseData, timestamp, parseDoc, parseUpdate } from '../utils'

// ================================================ //
// ==================== Create ==================== //
// ================================================ //

async function create(data) {
	await this.fetch()

	let id = ''
	const mandatory = ['name', 'dueAt', 'status']

	try {
		const parsed = await parseData(data, mandatory)
		const {
			data: { id: generated },
		} = await this.request.post('/utils')

		parsed._id = generated
		parsed.createdAt = timestamp()
		parsed.updatedAt = parsed.createdAt
		parsed.content = ''
		parsed.collection = NATE_TODO

		const res = await this._main.put(parsed)
		id = res.id
		const todo = await this._main.get(id)

		const created = await parseDoc(TodoSchema, todo)

		delete created._rev

		await this.request.post('/todos', created)

		return created
	} catch (err) {
		if (!id) throw err

		const doc = await this._main.get(id)
		await this._main.remove(doc._id, doc._rev)

		throw err
	}
}

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

async function update(doc) {
	await this.fetch()

	const fields = []

	const parsed = await parseDoc(TodoSchema, doc)
	const old = await this._main.get(parsed._id)
	const todo = await parseUpdate(old, doc, fields)

	todo.updatedAt = timestamp()
	const res = await this._main.put(todo)

	const updated = await this._main.get(res.id)
	await this.request.post('/todos', updated)

	return updated
}

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll() {
	return new Promise((ok, ko) => {
		this._main
			.find({ selector: { collection: NATE_TODO } })
			.then((res) => ok(res.docs))
			.catch(ko)
	})
}

export default function() {
	return {
		create: create.bind(this),
		update: update.bind(this),
		findAll: findAll.bind(this),
	}
}
