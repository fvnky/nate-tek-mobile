export default class CustomError {
   constructor(message) {
      this.message = message || 'No message';
      this.name = this.constructor.name;
   }
}
