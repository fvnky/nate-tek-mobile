import _ from 'lodash'
import axios from 'axios'
import PouchDB from './PouchDB'
import config from '../../config.json'

import {
	user,
	subjects,
	folders,
	courses,
	dictionary,
	drafts,
	quizs,
	todos,
} from './collections'

import { USER } from './constants/index.js'

const endpoint = `${config.remote.protocol}://${config.remote.host}:${config.remote.port}`

class Database {
	constructor() {
		this._main = new PouchDB(config.local.main, {
			adapter: 'react-native-sqlite',
			auto_compaction: true,
		})
		this._session = new PouchDB(config.local.session, {
			adapter: 'react-native-sqlite',
			auto_compaction: true,
		})
		this.request = axios.create({ baseURL: `${endpoint}/api` })

		this._main.createIndex({
			index: {
				fields: ['collection'],
				name: 'collectionIndex',
				ddoc: 'CollectionDesignDoc',
			},
		})

		this._main.createIndex({
			index: {
				fields: ['parent.collection', 'parent.id'],
				name: 'parentIndex',
				ddoc: 'parentDesignDoc',
			},
		})

		this.user = user.call(this)
		this.subjects = subjects.call(this)
		this.folders = folders.call(this)
		this.courses = courses.call(this)
		this.dictionary = dictionary.call(this)
		this.drafts = drafts.call(this)
		this.quizs = quizs.call(this)
		this.todos = todos.call(this)
	}
}

Database.prototype.init = async function() {
	try {
		await this._session.get(USER)
	} catch (e) {
		await this._session.put({ _id: USER, profile: {}, token: '', theme: false })
	}
}

Database.prototype.fetch = async function() {
	const ref = await this._session.get(USER)

	const {
		data: { user: profile, token },
	} = await this.request.put('/auth/refresh', { token: ref.token })

	await this._session.put({
		_id: USER,
		_rev: ref._rev,
		profile,
		token,
		theme: ref.theme,
	})

	const { rows } = await this._main.allDocs({ include_docs: true })

	const {
		data: { docs },
	} = await this.request.post('/refresh', rows)

	for (const doc of docs) {
		if (doc.value === 'deleted') {
			const loc = await this._main.get(doc.id)
			const a = await this._main.remove(loc)
		} else {
			try {
				const loc = await this._main.get(doc.id)
				doc.value._rev = loc._rev
				await this._main.put(doc.value)
			} catch (e) {
				if (e.name === 'not_found')
					await this._main.put(_.omit(doc.value, ['_rev']))
			}
		}
	}
}

Database.prototype.setTheme = async function(theme) {
	const session = await this._session.get(USER)

	session.theme = theme
	await this._session.put(session)
}

Database.prototype.getTheme = async function() {
	const session = await this._session.get(USER)

	return session.theme
}

Database.prototype.close = function() {
	return this._main.close()
}

export default Database
