import { DICTIONARY } from "../constants";

export default {
   definition: Object.freeze({
      _id: { type: String, required: true },
      _rev: { type: String, required: true },
      createdAt: { type: Number, required: true },
      updatedAt: { type: Number, required: false },
      definition: { type: String, required: false },
      name: { type: String, required: true },
      subject_id: { type: String, required: true },
      collection: DICTIONARY
   }),
   fields: [
      '_id',
      '_rev',
      'createdAt',
      'updatedAt',
      'definition',
      'name',
      'subject_id',
      'collection'
   ]
};
