import { SCHEDULE } from '../constants'

export default {
	definition: Object.freeze({
		_id: { type: String, required: true },
		_rev: { type: String, required: true },
		createdAt: { type: Number, required: true },
		updatedAt: { type: Number, required: false },
		date: { type: String, required: true },
		title: { type: String, required: false },
		classNames: { type: String, required: false },
		room: { type: String, required: false },
		note: { type: String, required: false },
		start: { type: String, required: false },
		end: { type: String, required: false },
		id_subject: { type: String, required: false },
		collection: SCHEDULE,
	}),
	fields: [
		'_id',
		'_rev',
		'createdAt',
		'updatedAt',
		'title',
		'date',
		'classNames',
		'room',
		'note',
		'start',
		'end',
		'id_subject',
		'collection',
	],
}
