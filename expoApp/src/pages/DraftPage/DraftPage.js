import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import { NavigationEvents } from 'react-navigation'

// darkMode needed do not remove
import { lightMode, darkMode } from './../MainPages.style'
import { TouchableOpacity } from 'react-native-gesture-handler'
import CourseCard from '../SubjectPage/CourseCard'

class DraftPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = { drafts: [] }
	}

	componentDidMount() {
		this.getData()
	}

	async getData() {
		this.setState({ drafts: await this.props.screenProps.drafts.findAll() })
	}

	handleCourse(course) {
		this.props.screenProps.selectedCourse = course.textContent
		this.props.navigation.navigate('Course', { course: course })
	}

	render() {
		const theme = this.props.myStore.theme ? lightMode : darkMode
		const themeValue = this.props.myStore.theme

		return (
			<View style={theme.container}>
				<NavigationEvents onWillFocus={() => this.getData()} />
				<View style={theme.container}>
					<Text style={theme.SubjectTitle}>My Drafts</Text>
					<ScrollView>
						{this.state.drafts.map((draft) => {
							return (
								<TouchableOpacity
									key={draft._id}
									style={theme.card}
									onPress={() => this.handleCourse(draft)}
								>
									<CourseCard
										theme={themeValue}
										title={draft.name}
									></CourseCard>
								</TouchableOpacity>
							)
						})}
					</ScrollView>
				</View>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	const { myStore } = state
	return { myStore }
}

export default connect(mapStateToProps)(DraftPage)
