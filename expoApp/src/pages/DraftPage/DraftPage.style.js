import { StyleSheet } from 'react-native'

export const lightMode = StyleSheet.create({
  container: {
    backgroundColor: "#1A2138",
    flex: 1,
  },
  SubjectTitle: {
    color: "#F7F8FC",
    letterSpacing: 2,
    fontSize: 25,
    marginLeft: 30,
    marginTop: 20,
  },

  // TODO A terminer de styliser apres le merge, car je n'ai pas de visuel
  // list: {
  //   justifyContent: 'center',
  // },
  // card: {
  //   flexDirection: "column",
  //   justifyContent: 'center',
  //   paddingVertical: 10,
  //   marginLeft: "5%",
  // },
  // bar : {
  //   width: "100%",
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   elevation : 3,
  //   backgroundColor : "black",
  // },
  // text: {
  //   color: "#FAFAFA",
  //   fontSize: 25,
  // },
});

export const darkMode = StyleSheet.create({
  container: {
    backgroundColor: "#1A2138",
    flex: 1,
  },
  SubjectTitle: {
    color: "#FEFEFE",
    letterSpacing: 2,
    fontSize: 25,
    marginLeft: 30,
    marginTop: 20,
  },

  // TODO A terminer de styliser apres le merge, car je n'ai pas de visuel
  // list: {
  //   justifyContent: 'center',
  // },
  // card: {
  //   flexDirection: "column",
  //   justifyContent: 'center',
  //   paddingVertical: 10,
  //   marginLeft: "5%",
  // },
  // bar : {
  //   width: "100%",
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   elevation : 3,
  //   backgroundColor : "black",
  // },
  // text: {
  //   color: "#FAFAFA",
  //   fontSize: 25,
  // },
});
