import React from 'react';
import { View } from 'react-native';
import { NavigationEvents } from 'react-navigation'

class BackStop extends React.Component {

    gotoHome() {
        const { navigation } = this.props;

        navigation.navigate('Home');
    }

    render() {
        return (
            <View>
                <NavigationEvents onWillFocus={() => this.gotoHome()} />
            </View>
        )
    }
}

export default BackStop;
  