import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { NavigationEvents } from 'react-navigation'

// darkMode needed do not remove
import { lightMode, darkMode } from './LoginPage.style'

class LoginPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	async getData() {
		try {
			await this.props.screenProps.user.loadUser()
			this.props.navigation.navigate('Home')
		} catch (e) {}
	}

	handleLogin = async () => {
		this.props.navigation.navigate('Login2')
	}

	render() {
		const theme = lightMode

		return (
			<View style={theme.myBackground}>
				<NavigationEvents onWillFocus={() => this.getData()} />
				<View style={theme.container}>
					<View style={theme.IconContainerPage1}>
						<Text style={theme.nateTitle}>NATE</Text>
						<Text style={theme.nateSubtitle}>
							Not &bull; A &bull; Text &bull; Editor
						</Text>
					</View>
					<View style={theme.TitleContainerPage1}>
						<Image
							style={theme.NateIcon}
							source={require('../../assets/nate-logo.png')}
						/>
						<Text style={theme.signIn}>Welcome</Text>
						<View style={theme.colContainerCenter}>
							<Text style={theme.text1}>
								Log in to take the full advantage of the NATE ecosystem.
							</Text>
						</View>
					</View>
					<View style={theme.container3}>
						<TouchableOpacity onPress={this.handleLogin}>
							<View style={theme.LoginContainer}>
								<Text style={theme.LoginButton}>LOGIN</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	const { myStore } = state
	return { myStore }
}

export default connect(mapStateToProps)(LoginPage)
