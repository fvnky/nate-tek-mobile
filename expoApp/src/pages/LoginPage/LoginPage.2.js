import React from 'react'
import {
	View,
	Text,
	TextInput,
	Image,
	TouchableOpacity,
	KeyboardAvoidingView,
} from 'react-native'
import { connect } from 'react-redux'
import { NavigationEvents } from 'react-navigation'

// darkMode needed do not remove
import { lightMode, darkMode } from './LoginPage.style'

class LoginPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			login: '',
			loginError: false,
		}
	}

	getData() {}

	handleLogin = async () => {
		const { login } = this.state

		try {
			if (login === '') throw new Error('NOP')

			await this.props.screenProps.user.sendValidation(login)

			this.props.navigation.navigate('Login3', { Login: this.state.login })
		} catch (e) {
			this.setState({ loginError: true })
		}
	}

	handleLoginInput = (input) => {
		const { loginError } = this.state

		if (loginError) {
			this.setState({ loginError: false })
		}
		this.setState({ login: input })
	}

	handlePassWordInput = (Input) => {
		const { loginError } = this.state

		if (loginError) {
			this.setState({ loginError: false })
		}
		this.setState({ password: Input })
	}

	errorMessage() {
		const { loginError } = this.state
		const theme = this.props.myStore.theme ? lightMode : darkMode

		if (loginError) {
			return (
				<View style={theme.ErrorTextContainer}>
					<Text style={theme.ErrorText}>The email address is invalid.</Text>
				</View>
			)
		}
	}

	render() {
		const theme = lightMode

		return (
			<View style={theme.myBackground}>
				<NavigationEvents onWillFocus={() => this.getData()} />
				<KeyboardAvoidingView style={theme.container} behavior='padding'>
					<View style={theme.IconContainer1}>
						<Text style={theme.nateTitle}>NATE</Text>
						<Text style={theme.nateSubtitle}>
							Not &bull; A &bull; Text &bull; Editor
						</Text>
					</View>

					<View style={theme.TitleContainer1}>
						<Image
							style={theme.NateIcon}
							source={require('../../assets/nate-logo.png')}
						/>
						<Text style={theme.signIn}>Sign in</Text>

						<View style={theme.TitleContainer}>
							<View style={theme.inputContainer}>
								<TextInput
									value={this.props.login}
									placeholder='Please enter your email address'
									style={theme.loginInput}
									autoCapitalize='none'
									onChangeText={this.handleLoginInput}
									placeholderTextColor={'grey'}
									underlineColorAndroid='transparent'
									onSubmitEditing={this.handleLogin}
									blurOnSubmit={false}
								/>
							</View>
						</View>
					</View>

					<View style={theme.container3}>
						<TouchableOpacity onPress={this.handleLogin}>
							<View style={theme.LoginContainerBlack}>
								<Text style={theme.LoginButtonBlack}>Login</Text>
							</View>
						</TouchableOpacity>
						{this.errorMessage()}
					</View>
				</KeyboardAvoidingView>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	const { myStore } = state
	return { myStore }
}

export default connect(mapStateToProps)(LoginPage)
