import React from 'react'
import {
	View,
	Text,
	TextInput,
	Image,
	TouchableOpacity,
	KeyboardAvoidingView,
} from 'react-native'
import { connect } from 'react-redux'
import { NavigationEvents } from 'react-navigation'

// darkMode needed do not remove
import { lightMode, darkMode } from './LoginPage.style'

class LoginPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			login: '',
			password: '',
			loginError: false,
		}
	}

	getData() {
		const { navigation } = this.props

		const loginText = navigation.getParam('Login', '')
		this.setState({ login: loginText })
	}

	handleLogin = async () => {
		const { login, password } = this.state

		try {
			if (login === '' || password === '') throw new Error('NOP')

			await this.props.screenProps.user.logIn(login, password)

			this.props.navigation.navigate('Home')
		} catch (e) {
			this.setState({ loginError: true })
		}
	}

	handleLoginInput = (input) => {
		const { loginError } = this.state

		if (loginError) {
			this.setState({ loginError: false })
		}
		this.setState({ login: input })
	}

	handlePassWordInput = (Input) => {
		const { loginError } = this.state

		if (loginError) {
			this.setState({ loginError: false })
		}
		this.setState({ password: Input })
	}

	errorMessage() {
		const { loginError } = this.state
		const theme = this.props.myStore.theme ? lightMode : darkMode
		const themeValue = this.props.myStore.theme

		if (loginError) {
			return (
				<View style={theme.ErrorTextContainer}>
					<Text style={theme.ErrorText}>Incorrect login or password.</Text>
				</View>
			)
		}
	}

	render() {
		const theme = lightMode

		return (
			<View style={theme.myBackground}>
				<NavigationEvents onWillFocus={() => this.getData()} />
				<KeyboardAvoidingView style={theme.container} behavior='padding'>
					<View style={theme.IconContainer1}>
						<Text style={theme.nateTitle}>NATE</Text>
						<Text style={theme.nateSubtitle}>
							Not &bull; A &bull; Text &bull; Editor
						</Text>
					</View>

					<View style={theme.TitleContainer1}>
						<Image
							style={theme.NateIcon}
							source={require('../../assets/nate-logo.png')}
						/>
						<Text style={theme.signIn}>Sign in</Text>

						<View style={theme.helpLabelContainer}>
							<View style={theme.subContainer}>
								<Text style={theme.text1}>
									We have sent you an email with the login code, please check
									your mailbox.
								</Text>
							</View>
						</View>

						<View style={theme.TitleContainer}>
							<View style={theme.inputContainer}>
								<TextInput
									value={this.state.login}
									placeholder='Please enter your email address'
									style={theme.loginInput}
									onChangeText={this.handleLoginInput}
									underlineColorAndroid='transparent'
									onSubmitEditing={() => {
										this.passwordInput.focus()
									}}
									placeholderTextColor={'grey'}
									blurOnSubmit={false}
								/>
							</View>
						</View>
						<View style={theme.TitleContainer}>
							<View style={theme.inputContainer}>
								<TextInput
									value={this.state.password}
									ref={(input) => {
										this.passwordInput = input
									}}
									placeholder='Enter the code received by mail'
									style={theme.loginInput}
									onChangeText={this.handlePassWordInput}
									placeholderTextColor={'grey'}
									underlineColorAndroid='transparent'
									onSubmitEditing={this.handleLogin}
									blurOnSubmit={false}
								/>
							</View>
						</View>
					</View>

					<View style={theme.container3}>
						<TouchableOpacity onPress={this.handleLogin}>
							<View style={theme.LoginContainerBlack}>
								<Text style={theme.LoginButtonBlack}>Login</Text>
							</View>
						</TouchableOpacity>

						{this.errorMessage()}
					</View>
				</KeyboardAvoidingView>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	const { myStore } = state
	return { myStore }
}

export default connect(mapStateToProps)(LoginPage)
