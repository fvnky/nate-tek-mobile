import { StyleSheet } from 'react-native'

// border
// borderRadius: 4,
// borderWidth: 1,
// borderColor: 'red',

export const lightMode = StyleSheet.create({
	myBackground: {
		flex: 1,
		backgroundColor: '#222B45',
		justifyContent: 'flex-end',
	},

	container: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	IconContainerPage1: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#222B45',
		minHeight: 300,
	},

	IconContainer1: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#222B45',
		minHeight: 10,
// border
// borderRadius: 4,
// borderWidth: 1,
// borderColor: 'red',
	},

	IconContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	NateIcon: {
		marginTop: '10%',
		height: 70,
		width: 70,
	},

	signIn: {
		marginTop: 30,
		fontWeight: 'bold',
		fontSize: 30,
	},

	nateTitle: {
		fontSize: 70,
		letterSpacing: 20,
		fontWeight: '100',
		color: 'white',
		marginLeft: 20,

		// borderRadius: 4,
		// borderWidth: 1,
		// borderColor: 'red',
	},

	nateSubtitle: {
		fontSize: 22,
		fontWeight: '100',
		color: 'grey',
		// marginLeft: 0,
	},

	MailIcon: {
		height: 100,
		width: 100,
	},

	TitleContainerPage1: {
		flex: 3,
		flexDirection: 'column',
		alignItems: 'center',
		// justifyContent: 'space-around',
		backgroundColor: 'white',
		borderTopLeftRadius: 40,
		borderTopRightRadius: 40,

		// TODO debug
		// borderRadius: 4,
		// borderWidth: 1,
		// borderColor: 'red',
	},

  TitleContainer1: {
		flex: 3,
		flexDirection: 'column',
		alignItems: 'center',
		backgroundColor: 'white',
		borderTopLeftRadius: 40,
		borderTopRightRadius: 40,

		// TODO debug
		// borderRadius: 4,
		// borderWidth: 1,
		// borderColor: 'red',
	},

	helpLabelContainer: {
		flex: 2,
		flexDirection: 'column',
		alignItems: 'center',
		marginTop: 20,
		color: '#6B18FF',

		// borderRadius: 4,
		// borderWidth: 1,
		// borderColor: 'blue',
	},

	TitleContainer: {
		flex: 2,
		flexDirection: 'column',
		alignItems: 'center',

		// borderRadius: 4,
		// borderWidth: 1,
		// borderColor: 'blue',
	},

	rowContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		width: '100%',
	},

	colContainer: {
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',

		borderRadius: 4,
		borderWidth: 1,
		borderColor: 'blue',
	},

	colContainerCenter: {
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',
		marginTop: 40,
	},

	subContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		// alignItems: 'center',

		// borderRadius: 4,
		// borderWidth: 1,
		// borderColor: 'blue',
	},

	text1: {
		//marginTop: "40%",
		fontSize: 20,
		textAlign: 'center',
		fontWeight: '100',
		color: 'grey',
		width: '80%',
	},

	text2: {
		//marginTop: "40%",
		fontSize: 16,

		fontWeight: '100',
		color: 'grey',
	},

	inputsContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
		bottom: 30,
	},

	inputContainer: {
		backgroundColor: 'white',
		width: '90%',
		height: 65,
		borderWidth: 4,
		borderRadius: 75,
    	borderColor: '#000000',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		bottom: 40,
	},

	login: {
		width: '80%',
		color: 'grey',
	},

	container3: {
		flex: 1,
		backgroundColor: 'white',
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: 'auto',
		marginRight: 'auto',
		width: '90%',
		height: 65,
		//marginTop: 10,
		backgroundColor: 'white',
		borderRadius: 75,
		// ToDO
    borderWidth: 4,
		borderColor: '#000000',
	},

	LoginContainerBlack: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: 'auto',
		marginRight: 'auto',
		width: '90%',
		height: 65,
		//marginTop: 10,
		backgroundColor: 'black',
		borderRadius: 75,
		// ToDO
    borderWidth: 4,
    borderColor: 'black',
	},

	loginInput: {
		width: '90%',
		color: 'black',
		textAlign: 'left',
		fontSize: 20,
		fontWeight: 'bold',

		//boxSizing: "border-box",
	},

	passwordInput: {
		width: '80%',
		color: 'black',
		textAlign: 'left',
		//boxSizing: "border-box",
	},

	LoginButton: {
		fontSize: 20,
		fontWeight: 'bold',
		color: 'black',
		paddingVertical: 16,
	},

	LoginButtonBlack: {
		fontSize: 20,
		fontWeight: 'bold',
		color: 'white',
		paddingVertical: 16,
	},

	ErrorTextContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 30,

		// borderRadius: 4,
		// borderWidth: 1,
		// borderColor: 'red',
	},

	ErrorText: {
		fontSize: 20,
		color: 'black',
	},

	buttonContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	button: {},
})

export const darkMode = StyleSheet.create({
	myBackground: {
		flex: 1,
		backgroundColor: '#676D78',
		justifyContent: 'flex-end',
	},

	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		//marginBottom: "15%",
	},

	IconContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	NateIcon: {
		// marginTop: '25%',
		height: 100,
		width: 100,
	},

	nateTitle: {
		//marginTop: "40%",
		fontSize: 25,

		fontWeight: '100',
		color: '#FFFFFF',
	},

	MailIcon: {
		height: 100,
		width: 100,
	},

	TitleContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	rowContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	colContainer: {
		flexDirection: 'column',
		justifyContent: 'center',
	},

	colContainerCenter: {
		flexDirection: 'column',
		justifyContent: 'center',
		marginTop: 50,
	},

	subContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	text1: {
		fontSize: 20,
		fontWeight: '100',
		color: '#FFFFFF',
	},

	text2: {
		fontSize: 16,
		fontWeight: '100',
		color: '#FFFFFF',
	},

	inputsContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	inputContainer: {
		backgroundColor: '#676D78',
		// elevation: 2,
		marginTop: 7,
		width: '90%',
		height: 40,
		// borderWidth: 1,
		// borderRadius: 12,
		borderBottomWidth: 1,
		borderBottomColor: '#FFFFFF',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},

	login: {
		width: '80%',
		color: 'grey',
	},

	container3: {
		flex: 1,
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '80%',
		height: 45,
		//marginTop: 10,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	loginInput: {
		width: '80%',
		color: '#FAFAFA',
		textAlign: 'left',
		//boxSizing: "border-box",
	},

	passwordInput: {
		width: '80%',
		color: '#FAFAFA',
		textAlign: 'left',
		//boxSizing: "border-box",
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		//color: "black",
		paddingVertical: 10,
		//borderRadius: 50,
	},

	ErrorTextContainer: {
		justifyContent: 'center',
		alignItems: 'center',
	},

	ErrorText: {
		fontSize: 16,
		color: 'red',
	},

	buttonContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	button: {},
})
