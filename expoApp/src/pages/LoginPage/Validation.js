import React from 'react';
import { useStoreState, useStoreActions, action } from 'easy-peasy'

const Validation = (login) => {
    const init = useStoreActions(actions => actions.init)
    const sendValidation = useStoreActions(actions => actions.sendValidation)
  
    init();
    sendValidation(login);
}

export default Validation;
  