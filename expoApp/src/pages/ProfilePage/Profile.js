import React from 'react'
import { Image, Text, View } from 'react-native'
import { connect } from 'react-redux';
import { NavigationEvents } from 'react-navigation';

// darkMode needed do not remove
import { lightMode, darkMode } from './Profile.style';
import Feather from 'react-native-vector-icons/Feather';

class ProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {
        username: 'username',
        firstname: 'firstname',
        lastname: 'surname',
        university: 'schoolname',
        country: 'France',
        curriculum: "curiculum",
        mail: 'firstname.surname@email.com',
        year: '1',
      }
    };
  }

  // fetch user data here and save it in state.profile
  async getData() {
    await this.props.screenProps.fetch()

    const user = await this.props.screenProps.user.getUser()
    this.setState({ profile: user.profile })
  }

  renderUserInfo(icon, label, info) {
    const theme = this.props.myStore.theme? lightMode: darkMode;

    return (
      <View style={theme.infoContainer}>
        <View style={theme.iconContainer}>
          <Feather style={theme.Icon} name={icon} size={35} ></Feather>
        </View>
        <View style={theme.textContainer}>
          <View style={theme.labelContainer}>
            <Text style={theme.labelText}>{label}</Text>
          </View>
          <View style={theme.userInfoContainer}>
            <Text style={theme.iuserInfoText}>{info}</Text>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const theme = this.props.myStore.theme? lightMode: darkMode;
    const { profile } = this.state;

    return (
      <View style={theme.viewContainer}>
        <NavigationEvents onWillFocus={() => this.getData()} />
        <View style={theme.headerContainer}>
          <View style={theme.userNameContainer}>
            <Text style={theme.userFirstame}>{profile.firstname}</Text>
            <Text style={theme.userLastame}>{profile.lastname}</Text>
          </View>
          <View style={theme.userProfilePictureContainer}>
            <Image
              style={theme.userImage}
              source={require('./mocks/avatar.jpg')}
            />
          </View>
        </View>
        <View style={theme.profilInfosContainer}>
          {this.renderUserInfo('map-pin', 'Country', profile.country)}
          {this.renderUserInfo('mail', 'Email', profile.mail)}
          {this.renderUserInfo('award', 'Curriculum', profile.curriculum)}
          {this.renderUserInfo('home', 'University', profile.university)}
          {this.renderUserInfo('triangle', 'Academic Year', profile.year)}
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
	const { myStore } = state;
	return { myStore };
  };

export default connect(mapStateToProps)(ProfilePage);
