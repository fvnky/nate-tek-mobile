import { StyleSheet, Platform } from 'react-native'

export const lightMode = StyleSheet.create({
  // ---------- Page ----------
  viewContainer: {
    backgroundColor: 'red',
    flex: 1
  },

  // ---------- Header ----------
  headerContainer: {
    paddingBottom: 20,
    paddingTop: 35,
    backgroundColor: '#F7F8FC',
    height: 220,
    zIndex: 1,
    borderBottomColor: '#E3E9F1',
    borderBottomWidth: 1,
  },
  userNameContainer: {
    flex: 1,
    paddingLeft: 20,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  userFirstame: {
    color: '#222B45',
    fontSize: 40,
    letterSpacing: 4,
    fontWeight: '800',
    textAlign: 'center',
    textTransform: 'capitalize',
    ...Platform.select({
      ios: {
        fontFamily: "Avenir",
      },
      android: {
        fontFamily: "Roboto",
      },
    }),
  },
  userLastame: {
    color: '#8F9BB3',
    fontSize: 20,
    letterSpacing: 1.5,
    fontWeight: '300',
    textAlign: 'center',
    textTransform: 'capitalize',
    ...Platform.select({
      ios: {
        fontFamily: "Avenir",
      },
      android: {
        fontFamily: "Roboto",
      },
    }),
  },

  userProfilePictureContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  userImage: {
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#E3E9F1',
    height: 150,
    width: 150,
    zIndex: 1,
    marginTop: 125,
  },

  // ---------- Infos ----------
  profilInfosContainer: {
    flex: 1,
    zIndex: 0,
    paddingTop: 100,
    paddingBottom: 30,
    backgroundColor: '#FEFFFE',
    flexDirection: 'column'
  },

  infoContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  iconContainer: {
    justifyContent: 'center',
    paddingLeft: 30,
  },
  Icon: {
    color: '#8F9BB3'
  },
  textContainer: {
    flex: 1,
    paddingLeft: 30,
    justifyContent: 'center',
  },
  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  labelText: {
    color: '#2B344D',
    fontSize: 18,
    fontWeight: '500',
  },
  userInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  iuserInfoText: {
    color: '#8E9AB2',
    fontSize: 16,
    fontWeight: '300',
  },
});

export const darkMode = StyleSheet.create({
  // ---------- Page ----------
  viewContainer: {
    backgroundColor: 'red',
    flex: 1
  },

  // ---------- Header ----------
  headerContainer: {
    paddingBottom: 20,
    paddingTop: 35,
    backgroundColor: '#1A2138',
    height: 220,
    zIndex: 1,
    borderBottomColor: '#101426',
    borderBottomWidth: 1,
  },
  userNameContainer: {
    flex: 1,
    paddingLeft: 20,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  userFirstame: {
    color: '#FAFAFA',
    fontSize: 40,
    letterSpacing: 4,
    fontWeight: '800',
    textAlign: 'center',
    textTransform: 'capitalize',
    ...Platform.select({
      ios: {
        fontFamily: "Avenir",
      },
      android: {
        fontFamily: "Roboto",
      },
    }),
  },
  userLastame: {
    color: '#8F9BB3',
    fontSize: 20,
    letterSpacing: 1.5,
    fontWeight: '300',
    textAlign: 'center',
    textTransform: 'capitalize',
    ...Platform.select({
      ios: {
        fontFamily: "Avenir",
      },
      android: {
        fontFamily: "Roboto",
      },
    }),
  },

  userProfilePictureContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  userImage: {
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#151A30',
    height: 150,
    width: 150,
    zIndex: 1,
    marginTop: 125,
  },

  // ---------- Infos ----------
  profilInfosContainer: {
    flex: 1,
    zIndex: 0,
    paddingTop: 100,
    paddingBottom: 30,
    backgroundColor: '#222B45',
    flexDirection: 'column'
  },

  infoContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  iconContainer: {
    justifyContent: 'center',
    paddingLeft: 30,
  },
  Icon: {
    color: '#151A30'
  },
  textContainer: {
    flex: 1,
    paddingLeft: 30,
    justifyContent: 'center',
  },
  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  labelText: {
    color: '#FEFFFE',
    fontSize: 18,
    fontWeight: '500',
  },
  userInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  iuserInfoText: {
    color: '#8E9AB2',
    fontSize: 16,
    fontWeight: '300',
  },
});
