import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';

export default class TopBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={this.props.theme ? lightMode.top : darkMode.top}>
        <View style={this.props.theme ? lightMode.container: darkMode.container}>
            <View style={this.props.theme ? lightMode.subContainer : darkMode.container}>
                <Text style={this.props.theme ? lightMode.title : darkMode.title}>{this.props.title}</Text>
            </View>
        </View>
      </View>
    );
  }
}

// interface IProps extends RouteComponentProps {
//   title: string;
//   theme: string;
// }

// const TopBar = (props: IProps) => {
//   const { title } = props;

//     return (
//       <View style={lightMode.top}>
//         <View style={lightMode.container}>
//             <Image
//                style={lightMode.NateIcon}
//                source={require('../../assets/SubjectPlaceholder.png')}
//             />
//             <View style={lightMode.subContainer}>
//                 <Text style={lightMode.title}>{title}</Text>
//             </View>
//         </View>
//       </View>
//     );
// }

// export default withRouter(TopBar);

const lightMode = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'white',
      flexDirection: "row",
      justifyContent: 'space-around',
      width: "95%",
      borderRadius: 12,
      height: 100,
  },

  NateIcon: {
    height: 80,
    width: 80,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },

  top: {
    flexDirection: "column",
    justifyContent: 'center',
  },

  subContainer: {
    flex: 3,
    flexDirection: "column",
    justifyContent: 'space-between',
  },

  title: {
    color: "black",
    fontSize: 20,

    paddingVertical: 10,
    marginLeft: 10,
  },

  text: {
    color: "black",
    fontSize: 15,

    paddingVertical: 10,
    marginLeft: 10,
  },
  
  Icon: {
    height: 40,
    width: 40,
    marginTop: 10,
  }, 
});

const darkMode = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#4A4A4A',
      flexDirection: "row",
      justifyContent: 'space-around',
      width: "95%",
      borderRadius: 12,
      height: 100,
  },

  NateIcon: {
    height: 80,
    width: 80,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },

  top: {
    flexDirection: "column",
    justifyContent: 'center',
  },

  subContainer: {
    flex: 3,
    flexDirection: "column",
    justifyContent: 'space-between',
  },

  title: {
    color: "#FFFFFF",
    fontSize: 20,

    paddingVertical: 10,
    marginLeft: 10,
  },

  text: {
    color: "#FFFFFF",
    fontSize: 15,

    paddingVertical: 10,
    marginLeft: 10,
  },
  
  Icon: {
    height: 40,
    width: 40,
    marginTop: 10,
  }, 
});
