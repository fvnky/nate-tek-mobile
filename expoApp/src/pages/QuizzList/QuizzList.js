import React from 'react';
import { View, TouchableOpacity, ScrollView } from 'react-native'
import { connect } from 'react-redux';
import { NavigationEvents, NavigationActions } from 'react-navigation';

// darkMode needed do not remove
import { lightMode, darkMode } from './QuizzList.style';
import TopBar from '../../component/TopBar';
import CourseCard from './CourseCard';

class QuizzList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      QUIZZES: [],
      CurrentPath: null,
      setUp: false,
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
		const quizzes = await this.props.screenProps.quizs.findAll()

    this.setState({ QUIZZES: quizzes});
  }

  handleQuizz(QUIZZ) {
    const { navigation } = this.props;

    navigation.navigate('QuizzPage1', { Quizz: QUIZZ, myBack: 'QuizzList' });
  }

  QuizzesList() {
    const { QUIZZES } = this.state;
    const theme = this.props.myStore.theme ? lightMode : darkMode;
    const themeValue = this.props.myStore.theme

    return (
      <View style={theme.container}>
        {QUIZZES.map(QUIZZ => {
            return(
                <TouchableOpacity style={theme.card} onPress={() => this.handleQuizz(QUIZZ)}>
                    <CourseCard theme={themeValue} title={QUIZZ.name}></CourseCard>
                </TouchableOpacity>
            )
        })}
      </View>
    );
  }

  render() {
    const theme = this.props.myStore.theme ? lightMode : darkMode;
		const themeValue = this.props.myStore.theme;

    return (
      <View style={{ flex: 1 }}>
        <TopBar theme={themeValue} navigation={this.props.navigation} title="Quizz" />
        <NavigationEvents onWillFocus={() => this.getData()} />
        <View style={theme.container}>
          <ScrollView>
            {this.QuizzesList()}
          </ScrollView>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
	const { myStore } = state;
	return { myStore };
  };

export default connect(mapStateToProps)(QuizzList);
