
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import QuizzList   from './QuizzList'
import QuizzPage1   from '../SubjectPage/QuizzPage/QuizzPage.1'
import QuizzPage2   from '../SubjectPage/QuizzPage/QuizzPage.2'
import QuizzPage3   from '../SubjectPage/QuizzPage/QuizzPage.3'

const QuizzListStack = createStackNavigator({
    QuizzList       : { screen : QuizzList },
    QuizzPage1      : { screen : QuizzPage1 },
    QuizzPage2      : { screen : QuizzPage2 },
    QuizzPage3      : { screen : QuizzPage3 },
}, { headerMode : 'none' });

export default createAppContainer(QuizzListStack);
