import React from 'react';
import { View, ScrollView, Text } from 'react-native'
import { connect } from 'react-redux';
import { NavigationEvents } from 'react-navigation';

// darkMode needed do not remove
import { lightMode, darkMode } from './../MainPages.style';

class RecentPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async getData() {

  }

  resentList() {
    return (null)
  }

  render() {
    const theme = this.props.myStore.theme? lightMode: darkMode;

    return (
      <View style={theme.container}>
        <NavigationEvents onWillFocus={() => this.getData()} />
        <View style={theme.container}>
          <Text style={theme.SubjectTitle}>Recent</Text>
          <ScrollView>
            {this.resentList()}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
	const { myStore } = state;
	return { myStore };
  };

export default connect(mapStateToProps)(RecentPage);
