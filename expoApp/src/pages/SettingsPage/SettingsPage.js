import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Switch,
  Picker
} from 'react-native'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NavigationEvents } from 'react-navigation';

// darkMode needed do not remove
import { lightMode, darkMode } from './SettingsPage.style';
import TopBar from "../../component/TabTopBar.js";

import { setTheme } from '../../redux/actions';

class SettingsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: 'black',
      themeValue: false,
      exploreValue: false,
      pushNotification: false,
      newNotification: false,
      laguage: "Francais",
      font: "Modern",
    };
  }

  async getData() {}

  toggleTheme = async () => {
    const { myStore, setTheme, screenProps } = this.props;
    const { theme } = myStore;

    await screenProps.setTheme(!theme)    
    setTheme(!theme);
  }

  // this needs spliting
  render() {
    const theme = this.props.myStore.theme? lightMode: darkMode;
    const themeValue = this.props.myStore.theme;

    return (
      <View style={theme.pageContainer}>
        <NavigationEvents onWillFocus={() => this.getData()} />
        <TopBar theme={themeValue} navigation={this.props.navigation} title="NATE" />

        <Text style={theme.settingsLabel}>Settings</Text>

        <View style={theme.toggleThemeContainer}>
          <View style={theme.labelContainer}>
            <Text style={theme.labelText}>Interface Theme</Text>
            <Text style={theme.labelSubText}>{themeValue ? "Light" : "Dark"}</Text>
          </View>
          <TouchableOpacity style={theme.toggleButton} onPress={async () => this.toggleTheme()}>
            <Switch
              trackColor={{true: '#1EC0CA', false: 'grey'}}
              style={theme.switch}
              onValueChange={this.toggleTheme}
              value={!themeValue} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
	const { myStore } = state;
	return { myStore };
};

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
      setTheme,
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
