import { StyleSheet } from 'react-native';

export const lightMode = StyleSheet.create({
  pageContainer: {
    backgroundColor: "#F7F8FC",
    flex: 1,
  },

  settingsLabel: {
    color: "#232C46",
    letterSpacing: 2,
    fontSize: 30,
    marginLeft: 30,
    marginTop: 20,
  },

  // ---------- Toggle theme button ----------
  toggleThemeContainer: {
    marginTop: 30,
    width: '100%',
    padding: 25,
    flexDirection: "row",
    justifyContent: 'space-between',
  },

  labelContainer: {
    flexDirection: 'column',
  },
  labelText: {
    fontSize: 22,
    fontWeight: '500',
    color: '#424A60',
  },
  labelSubText: {
    fontSize: 18,
    marginTop: 10,
    color: '#919CB4',
  },


  toggleButton: {
    justifyContent: 'center',
  },
});

export const darkMode = StyleSheet.create({
  pageContainer: {
    backgroundColor: "#1A2138",
    flexDirection: 'column',
    flex: 1,
  },

  settingsLabel: {
    color: "#FAFAFA",
    letterSpacing: 2,
    fontSize: 30,
    marginLeft: 30,
    marginTop: 20,
  },

  // ---------- Toggle theme button ----------
  toggleThemeContainer: {
    marginTop: 30,
    width: '100%',
    padding: 25,
    flexDirection: "row",
    justifyContent: 'space-between',
  },

  labelContainer: {
    flexDirection: 'column',
  },
  labelText: {
    fontSize: 22,
    fontWeight: '500',
    color: '#D1D3D9',
  },
  labelSubText: {
    fontSize: 18,
    marginTop: 10,
    color: '#424C65',
  },


  toggleButton: {
    justifyContent: 'center',
  },
});
