import React from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { NavigationEvents } from 'react-navigation';

import { lightMode } from './CoursePage.style';


class CoursePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      COURSE: [],
      CurrentPath: '',
      course: [],
      id: '',
    };
  }

  async CourseContant() {
    const id = this.props.screenProps.selectedCourse;
    const course = await this.props.screenProps.courses.findById(id);
    // const { course } = this.props.navigation.getParam('course', '');

    // const base64 = doc._attachments['value.json'].data;
    // const value = Value.fromJSON(JSON.parse(Base64.decode(base64)));
    this.setState({ course });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {/* <NavigationEvents onWillFocus={() => this.CourseContant()} /> */}
        <ScrollView>
          <Text>
            {this.props.screenProps.selectedCourse}
          </Text>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  const { myStore } = state;
  return { myStore };
};

export default connect(mapStateToProps)(CoursePage);