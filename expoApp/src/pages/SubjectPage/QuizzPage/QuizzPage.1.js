import React from 'react'
import {
	View,
	Text,
	Image,
	TouchableOpacity,
	BackHandler,
} from 'react-native'
import { NavigationEvents } from 'react-navigation';
import { connect } from 'react-redux';

import { lightMode, darkMode } from './QuizzPage.style';
import TopBar from '../../../component/TopBar';

class QuizzPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			Quizz: null,
			myBack: 'SubjectPage',
		}
	}

	componentDidMount() {
		this.getData();
	}

	async getData() {
		const { navigation } = this.props;

		const tmpQuizz = await navigation.getParam('Quizz', null);
		const tmpBack = await navigation.getParam('myBack', null);
		this.setState({ Quizz: tmpQuizz, myBack: tmpBack });
	}

	BackHandler() {
		const { myBack } = this.state;
		const { navigation } = this.props;
	
		if (navigation.isFocused()) {
		  navigation.navigate(myBack);
		  return true;
		}
		return false;
	}


	handleStart = async () => {
		const { Quizz } = this.state;

		this.props.navigation.navigate('QuizzPage2', {Quizz: Quizz})
	}

	bar() {
		const theme = this.props.myStore.theme? lightMode: darkMode;
		
		return (
		  <View style={theme.bar}>
			<View style={theme.container}>
			  <Text style={theme.title}>Quizz</Text>
			</View>
  	  	  </View>
		);
	}

	content() {
		const { Quizz } = this.state;
		const theme = this.props.myStore.theme? lightMode: darkMode;

		if (Quizz === null) {
			return ;
		}
		return (
			<View style={theme.container}>
				<View style={theme.IconContainer}>
					<Image
						style={theme.NateIcon}
						source={require('../../../assets/nate-logo.png')}
					/>
				</View>
				<View style={theme.QuestionContainer}>
					<View style={theme.questionContainer}>
						<Text style={theme.questionTitle}>{Quizz.name}</Text>
					</View>
				</View>
				<View style={{flex: 1}}>
					<TouchableOpacity onPress={this.handleStart}>
						<View style={theme.LoginContainer}>
							<Text style={theme.LoginButton}>Start</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>	
		)
	}

	render() {
		const theme = this.props.myStore.theme? lightMode: darkMode;
		const themeValue = this.props.myStore.theme;
		const { Quizz } = this.state;

		BackHandler.addEventListener('hardwareBackPress', () => this.BackHandler());
		return (
			<View style={theme.myBackground}>
				<TopBar theme={themeValue} navigation={this.props.navigation} title="Quizz" />
		        <NavigationEvents onWillFocus={() => this.getData()} />
				{this.content()}
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	const { myStore } = state;
	return { myStore };
  };

export default connect(mapStateToProps)(QuizzPage);
