import React from 'react'
import {
	View,
	Text,
	Image,
	TouchableOpacity,
	ScrollView,
	CheckBox,
	TextInput,
	KeyboardAvoidingView,
} from 'react-native'
import { NavigationEvents } from 'react-navigation';
import { connect } from 'react-redux';

import { lightMode, darkMode } from './QuizzPage.style';
import TopBar from '../../../component/TopBar';

class QuizzPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			Quizz: null,
			id: 0,
			correct: -1,
			selectedAnwer: null,
			score: 0,
			answer: '',
		}
	}

	componentDidMount() {
		this.getData();
	}

	async getData() {
		const { navigation } = this.props;

		const tmp = await navigation.getParam('Quizz', null);
		this.setState({ Quizz: tmp });
	}

	next() {
		const { Quizz, id, score } = this.state;
		const { navigation } = this.props;

		if ( id + 1 >= Quizz.questions.length ) {
			navigation.navigate('QuizzPage3', {Quizz: Quizz, score: score})
			return ;
		} else {
			this.setState({ id: id + 1, correct: -1, selectAnwer: null });
		}
	}


	// =======================================================
	//
	//
	//							QCM
	//
	//
	// ======================================================


	selectAnwer(key) {
		const { correct } = this.state;

		if ( correct === -1 ) {
			this.setState({selectedAnwer: key});
		}
	}

	checkQCM() {
		const { Quizz, id, selectedAnwer, score } = this.state;

		if ( Quizz.questions[id].form.answer === selectedAnwer ) {
			this.setState({ correct: 1,  score: score + 1});
		} else {
			this.setState({ correct: 0 });
		}
	}

	questions() {
		const { Quizz, id, selectedAnwer, correct } = this.state;
		const theme = this.props.myStore.theme? lightMode: darkMode;

		let proposition = [];
		for (let [key, value] of Object.entries(Quizz.questions[id].form.proposition)) {
			proposition.push({key, value});
		}
		return proposition.map((tmp, index) => {
			let myColor = "#4A4A4A";
			if (correct !== -1) {
				tmp.key === Quizz.questions[id].form.answer ? myColor = "green" : myColor = "#4A4A4A";
				tmp.key === selectedAnwer && tmp.key !== Quizz.questions[id].form.answer ? myColor = "red" : null;
			}
			return (
				<TouchableOpacity
					key={index}
					style={[theme.LoginContainer, { backgroundColor: myColor }]}
					onPress={() => this.selectAnwer(tmp.key)}
				>
					<View style={theme.answerContainer}>
						<View style={{width: 30}}></View>
						<Text style={theme.LoginButton}>{tmp.value}</Text>
						<CheckBox style={theme.checkBox} value={tmp.key === selectedAnwer} />
					</View>
				</TouchableOpacity>
			)
		});
	}

	QCM() {
		const { Quizz, id } = this.state;
		const theme = this.props.myStore.theme? lightMode: darkMode;

		return (
			<View style={theme.QuestionContainer}>
				<View style={[theme.line, { height: 30 }]}>
					<Text>{Quizz.questions[id].form.question}</Text>
				</View>
				<ScrollView>
					{this.questions()}
				</ScrollView>
			</View>
		);
	}


	// =======================================================
	//
	//
	//						Question/Answer
	//
	//
	// ======================================================

	checkQuestionAnswer() {
		const { Quizz, id, answer, score } = this.state;

		if ( Quizz.questions[id].form.answer === answer ) {
			this.setState({ correct: 1,  score: score + 1});
		} else {
			this.setState({ correct: 0 });
		}
	}

	handleAnswerInput(value) {
		this.setState({ answer: value });
	}

	AnswerImput() {
		const { answer, correct } = this.state;
		const theme = this.props.myStore.theme? lightMode: darkMode;

		let myColor = 'grey';
		if ( correct !== -1 ) {
			correct === 1 ? myColor = 'green': myColor = 'red';
		}

		return(
			<View style={theme.TitleContainer}>
				<View style={theme.inputContainer}>
					<TextInput
						value={answer}
						placeholder=''
						style={[theme.loginInput, { color: myColor }]}
						autoCapitalize = 'none'
						onChangeText={(value) => this.handleAnswerInput(value)}
						placeholderTextColor={'grey'}
						underlineColorAndroid='transparent'
						onSubmitEditing={() => this.handleSubmit()}
						blurOnSubmit={false}
					/>
				</View>
			</View>
		);
	}

	questionAnswer() {
		const { Quizz, id } = this.state;
		const theme = this.props.myStore.theme? lightMode: darkMode;

		return (
			<View style={theme.QuestionContainer}>
				<View style={[theme.line, { height: 30 }]}>
					<Text>{Quizz.questions[id].form.question}</Text>
				</View>
				{this.AnswerImput()}
			</View>
		);
	}

	// =======================================================
	//
	//
	//							rest
	//
	//
	// ======================================================


	questionContainer() {
		const { Quizz, id } = this.state;

		if ( Quizz === null ) {
			return ;
		}

		if (Quizz.questions[id].type === "qcm") {
			return this.QCM();
		} else if (Quizz.questions[id].type === "question_answer") {
			return this.questionAnswer();
		}
	}

	handleSubmit() {
		const { Quizz, id, selectedAnwer, answer, correct } = this.state;

		if ( correct !== -1 ) {
			this.next();
			return;
		} else if (Quizz.questions[id].type === "qcm") {
			return this.checkQCM();
		} else if (Quizz.questions[id].type === "question_answer") {
			return this.checkQuestionAnswer();
		}
	}

	myButon() {
		const { correct, selectedAnwer } = this.state;
		const theme = this.props.myStore.theme? lightMode: darkMode;

		let buttonText = "submit";
		selectedAnwer === null ? buttonText = "skip": buttonText = "submit";
		correct !== -1 ? buttonText = "next": null;

		return (
			<View style={{flex: 1}}>
				<TouchableOpacity onPress={() => this.handleSubmit()}>
					<View style={theme.LoginContainer}>
						<Text style={theme.LoginButton}>{buttonText}</Text>
					</View>
				</TouchableOpacity>
			</View>
		);		
	}

	render() {
		const { Quizz, id } = this.state;
		const theme = this.props.myStore.theme? lightMode: darkMode;
		const themeValue = this.props.myStore.theme;

		return (
			<View style={theme.myBackground}>
				<TopBar theme={themeValue} navigation={this.props.navigation} title="Quizz" />
		        <NavigationEvents onWillFocus={() => this.getData()} />
				<KeyboardAvoidingView style={theme.container} behavior="padding">
					<View style={theme.bar}>
						<View style={theme.barContainer}>
							<Text style={theme.title}>Question {id + 1}</Text>
						</View>
					</View>
					<View style={theme.container}>

						<View style={theme.IconContainer}>
							<Image
								style={theme.NateIcon}
								source={require('../../../assets/nate-logo.png')}
							/>
						</View>
						{this.questionContainer()}
						{this.myButon()}

					</View>
				</KeyboardAvoidingView>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	const { myStore } = state;
	return { myStore };
  };

export default connect(mapStateToProps)(QuizzPage);
