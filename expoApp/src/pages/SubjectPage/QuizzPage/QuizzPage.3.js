import React from 'react'
import {
	View,
	Text,
	Image,
	TouchableOpacity,
	BackHandler,
} from 'react-native'
import { NavigationEvents } from 'react-navigation';
import { connect } from 'react-redux';

import { lightMode, darkMode } from './QuizzPage.style';
import TopBar from '../../../component/TopBar';

class QuizzPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			Quizz: null,
			score: 0,
		}
	}

	componentDidMount() {
		this.getData();
	}

	async myUpdate() {
		const { Quizz, score } = this.state;
		const { screenProps } = this.props;

		const pourcentage = (score / Quizz.questions.length) * 100;
		let tmp = Quizz;
		pourcentage > tmp.score ? tmp.score = pourcentage : null;
		const message = await screenProps.quizs.update(tmp);
	}

	async getData() {
		const { navigation } = this.props;
		
		let tmp = await navigation.getParam('Quizz', null);
		this.setState({
			Quizz: tmp,
			score: navigation.getParam('score', 0),
		});
		await this.myUpdate();
	}	

	handleRestart = async () => {
		const { Quizz } = this.state;

		this.props.navigation.navigate('QuizzPage1', {Quizz: Quizz});
	}

	BackHandler() {
		const { navigation } = this.props;
	
		if (navigation.isFocused()) {
		  this.handleRestart();
		  return true;
		}
		return false;
	}

	render() {
		const { Quizz, score } = this.state;
		const theme = this.props.myStore.theme? lightMode: darkMode;
		const themeValue = this.props.myStore.theme;

		BackHandler.addEventListener('hardwareBackPress', () => this.BackHandler());

		if (Quizz === null) {
			return null;
		}

		return (
			<View style={theme.myBackground}>
				<TopBar theme={themeValue} navigation={this.props.navigation} title="Quizz" />
				<NavigationEvents onWillFocus={() => this.getData()} />
				<View style={theme.container}>

					<View style={theme.IconContainer}>
						<Image
							style={theme.NateIcon}
							source={require('../../../assets/nate-logo.png')}
						/>
					</View>

					<View style={theme.QuestionContainer}>
						<View style={theme.questionContainer}>
							<Text style={theme.questionTitle}>Score</Text>
						</View>
						<View style={theme.questionContainer}>
							<Text style={theme.questionTitle}>{score} / {Quizz.questions.length}</Text>
						</View>
					</View>

					<View style={{flex: 1}}>
						<TouchableOpacity onPress={this.handleRestart}>
							<View style={theme.LoginContainer}>
								<Text style={theme.LoginButton}>Restart</Text>
							</View>
						</TouchableOpacity>
					</View>

				</View>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	const { myStore } = state;
	return { myStore };
  };

export default connect(mapStateToProps)(QuizzPage);
