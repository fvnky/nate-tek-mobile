import { StyleSheet } from 'react-native'

export const lightMode = StyleSheet.create({
	myBackground: {
		flex: 1,
		backgroundColor: '#FAFAFA',
		justifyContent: 'flex-end',
	},

	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-around',
		//marginBottom: "15%",
	},

	line: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
	},

	bar : {
		height: "9%",
		width: "100%",
		flexDirection: "column",
		justifyContent: 'space-between',
		elevation : 3,
		backgroundColor : "white",
	},

	barContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		//marginBottom: "15%",
	},

	title: {
		color: "black",
		fontSize: 25,

		paddingVertical: 10,
	},

	IconContainer: {
		flex: 2,
		flexDirection: 'row',
		justifyContent: 'space-around',
		paddingVertical: '10%',
	},

	NateIcon: {
		height: 150,
		width: 150,
	},

	QuestionContainer: {
		flex: 3,
		flexDirection: 'column',
		justifyContent: 'flex-start',
	},

	questionContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	questionTitle: {
		//marginTop: "40%",
		fontSize: 25,

		fontWeight: '100',
		color: 'grey',
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		marginTop: 10,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		paddingVertical: 10,
	},

	answerContainer: {
		width: '90%',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},

	inputContainer: {
		backgroundColor: 'white',
		width: '90%',
		height: 65,
		borderWidth: 4,
		borderRadius: 75,
    	borderColor: '#000000',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		bottom: 40,
	},

	loginInput: {
		width: '90%',
		color: 'black',
		textAlign: 'left',
		fontSize: 20,
		fontWeight: 'bold',

		//boxSizing: "border-box",
	},

	TitleContainer: {
		flex: 2,
		flexDirection: 'column',
		alignItems: 'center',

		// borderRadius: 4,
		// borderWidth: 1,
		// borderColor: 'blue',
	},


	checkBox: {
		width: 30,
		marginTop: 5,
	},

})

export const darkMode = StyleSheet.create({
	myBackground: {
		flex: 1,
		backgroundColor: '#676D78',
		justifyContent: 'flex-end',
	},

	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-around',
		//marginBottom: "15%",
	},

	line: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
	},

	bar : {
		height: "9%",
		width: "100%",
		flexDirection: "column",
		justifyContent: 'space-between',
		elevation : 3,
		backgroundColor : "white",
	},

	barContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		//marginBottom: "15%",
	},

	title: {
		color: "black",
		fontSize: 25,

		paddingVertical: 10,
	},

	IconContainer: {
		flex: 2,
		flexDirection: 'row',
		justifyContent: 'space-around',
		paddingVertical: '10%',
		backgroundColor: 'red',
	},

	QuestionContainer: {
		flex: 3,
		flexDirection: 'column',
		justifyContent: 'flex-start',
	},

	questionContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	questionTitle: {
		//marginTop: "40%",
		fontSize: 25,

		fontWeight: '100',
		color: '#FFFFFF',
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		//marginTop: 10,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		//color: "black",
		paddingVertical: 10,
		//borderRadius: 50,
	},

	inputContainer: {
		backgroundColor: '#676D78',
		// elevation: 2,
		marginTop: 7,
		width: '90%',
		height: 40,
		// borderWidth: 1,
		// borderRadius: 12,
		borderBottomWidth: 1,
		borderBottomColor: '#FFFFFF',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},

	loginInput: {
		width: '90%',
		color: 'black',
		textAlign: 'left',
		fontSize: 20,
		fontWeight: 'bold',

		//boxSizing: "border-box",
	},

	TitleContainer: {
		flex: 2,
		flexDirection: 'column',
		alignItems: 'center',

		// borderRadius: 4,
		// borderWidth: 1,
		// borderColor: 'blue',
	},

})
