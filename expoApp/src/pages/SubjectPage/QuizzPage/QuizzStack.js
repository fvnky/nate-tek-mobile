
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import QuizzPage1   from './QuizzPage.1'
import QuizzPage2   from './QuizzPage.2'
import QuizzPage3   from './QuizzPage.3'

const QuizzStackNavigator = createStackNavigator({
    QuizzPage1      : { screen : QuizzPage1 },
    QuizzPage2      : { screen : QuizzPage2 },
    QuizzPage3      : { screen : QuizzPage3 },
}, { headerMode : 'none' });

export default createAppContainer(QuizzStackNavigator);
