import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, BackHandler } from 'react-native'
import { connect } from 'react-redux';
import { NavigationEvents, NavigationActions } from 'react-navigation';

// darkMode needed do not remove
import { lightMode, darkMode } from './SubjectPage.style';
import SubjectCard from './SubjectCard';
import FolderCard from './FolderCard';
import CourseCard from './CourseCard';

class SubjectPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      SUBJECTS: [],
      FOLDERS: [],
      COURSES: [],
      QUIZZES: [],
      CurrentPath: null,
      setUp: false,
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
		const subjects = await this.props.screenProps.subjects.findAll()
		const folders = await this.props.screenProps.folders.findAll()
		const courses = await this.props.screenProps.courses.findAll()
		const quizzes = await this.props.screenProps.quizs.findAll()

    this.setState({ SUBJECTS: subjects, FOLDERS: folders, COURSES: courses, QUIZZES: quizzes});
  }

  sendToQuizz() {
    this.props.navigation.navigate('QuizzPage1');
  }

  changePath(newPath) {
    this.setState({ CurrentPath: newPath });
  }

  back() {
    const { SUBJECTS, FOLDERS, CurrentPath } = this.state;

    if (CurrentPath === null) {
      return ;
    }
    SUBJECTS.map((SUBJECT) => {
      if (SUBJECT._id == CurrentPath._id) {
          this.setState({CurrentPath : null});
          return ;
      }
    });
    FOLDERS.map((FOLDER) => {
      if (FOLDER._id == CurrentPath._id) {
        SUBJECTS.map((SUBJECT) => {
          if (SUBJECT._id == FOLDER.parent.id) {
              this.setState({CurrentPath : SUBJECT});
              return ;
          }
        });
        FOLDERS.map((ParentFOLDER) => {
          if (ParentFOLDER._id == FOLDER.parent.id) {
              this.setState({CurrentPath : ParentFOLDER});
              return ;
          }
        });
      }
    });
  }

  BackHandler() {
    const { navigation } = this.props;

    if (navigation.isFocused()) {
      this.back();
      return true;
    }
    return false;
  }

  SubjectList() {
    const { SUBJECTS } = this.state;
    const theme = this.props.myStore.theme ? lightMode : darkMode;
    const themeValue = this.props.myStore.theme
    return (
      <View style={theme.container}>
          <Text style={theme.SubjectTitle}>My Subject</Text>
          <ScrollView>
              {SUBJECTS.map((SUBJECT) => {
                  return(
                      <TouchableOpacity key={SUBJECT._id} style={darkMode.card} onPress={() => this.changePath(SUBJECT)} >
                        <SubjectCard theme={themeValue} title={SUBJECT.name} key={SUBJECT.id}></SubjectCard>
                      </TouchableOpacity>
                  )
              })}
          </ScrollView>
      </View>
    );
  }

  FolderList() {
    const { FOLDERS } = this.state;
    const theme = this.props.myStore.theme ? lightMode : darkMode;
    const themeValue = this.props.myStore.theme
    const FilteredFOLDERS = FOLDERS.filter((FOLDER) => FOLDER.parent.id == this.state.CurrentPath._id)

    return (
        <View style={theme.container}>
                {FilteredFOLDERS.map(FOLDER => {
                    return(
                        <TouchableOpacity style={theme.card} onPress={() => this.changePath(FOLDER)}>
                            <FolderCard theme={themeValue} title={FOLDER.name}></FolderCard>
                        </TouchableOpacity>
                    )
                })}
        </View>
    );
  }

  handleCourse(course) {
    this.props.screenProps.selectedCourse = course.textContent;
    this.props.navigation.navigate('Course', { course: course });
  }

  CourseList() {
    const { COURSES } = this.state;
    const theme = this.props.myStore.theme? lightMode: darkMode;
    const themeValue = this.props.myStore.theme;
    const FilteredCOURSES = COURSES.filter((COURSE) => COURSE.parent.id == this.state.CurrentPath._id)

    return (
      <View style={theme.container}>
        {FilteredCOURSES.map(COURSE => {
          return (
            <TouchableOpacity style={theme.card} onPress={() => this.handleCourse(COURSE)}>
              <CourseCard theme={themeValue} title={COURSE.name}></CourseCard>
            </TouchableOpacity>
          )
        })}
      </View>
    );
  }

  handleQuizz(QUIZZ) {
    const { navigation } = this.props;

    const navigateAction = NavigationActions.navigate({
      routeName: 'Home',
      params: {},
      action: NavigationActions.navigate({
        routeName: 'Library',
        params: {},
        action: NavigationActions.navigate({
          routeName: 'Quizz',
          params: { Quizz: QUIZZ, myBack: 'SubjectPage' },
        }),
      }),
    });
    navigation.dispatch(navigateAction);
    // this.props.navigation.navigate('Recent')
    // this.props.navigation.navigate('Quizz', {Quizz: QUIZZ});
    // this.props.navigation.navigate('LibraryStackNavigator');
  }

  QuizzesList() {
    const { QUIZZES } = this.state;
    const theme = this.props.myStore.theme ? lightMode : darkMode;
    const themeValue = this.props.myStore.theme
    const FilteredQUIZZES = QUIZZES.filter((QUIZZ) => QUIZZ.parent.id == this.state.CurrentPath._id)

    return (
      <View style={theme.container}>
              {FilteredQUIZZES.map(QUIZZ => {
                  return(
                      <TouchableOpacity style={theme.card} onPress={() => this.handleQuizz(QUIZZ)}>
                          <CourseCard theme={themeValue} title={QUIZZ.name}></CourseCard>
                      </TouchableOpacity>
                  )
              })}
      </View>
    );
  }


  list() {
    const theme = this.props.myStore.theme ? lightMode : darkMode;

    return (
      <View style={theme.container}>
        {this.SubjectList()}
      </View>
    );
  }

  contant() {
    const theme = this.props.myStore.theme ? lightMode : darkMode;

    if (this.state.CurrentPath == null) {
      return (
        <View style={theme.container}>
          <View style={theme.container}>
            {this.list()}
          </View>
        </View>
      );
    } else {
      return (
        <View style={theme.container}>
          <TouchableOpacity onPress={() => this.back()}>
            <Text style={theme.SubjectTitle}>{this.state.CurrentPath.name}</Text>
          </TouchableOpacity>
          <ScrollView>
            {this.FolderList()}
            {this.CourseList()}
            {this.QuizzesList()}
          </ScrollView>
        </View>
      );
    }
  }

  render() {
    BackHandler.addEventListener('hardwareBackPress', () => this.BackHandler());

    return (
      <View style={{ flex: 1 }}>
        <NavigationEvents onWillFocus={() => this.getData()} />
        {this.contant()}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
	const { myStore } = state;
	return { myStore };
  };

export default connect(mapStateToProps)(SubjectPage);
