import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SubjectPage from './SubjectPage'
// import QuizzPage1 from './QuizzPage/QuizzPage.1'
import CoursePage from './CoursePage/CoursePage';

const MainStack = createStackNavigator({
    SubjectPage: { screen: SubjectPage },
    // QuizzPage1: { screen: QuizzPage1 },
    CoursePage: { screen: CoursePage },

}, { headerMode: 'none' });

export default createAppContainer(MainStack);