import React from 'react';
import {
    View,
    ScrollView,
    Text,
    Image,
    TextInput,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationEvents } from 'react-navigation';
import DateTimePicker from '@react-native-community/datetimepicker';

// darkMode needed do not remove
import TopBar from '../../component/TopBar'
import {lightMode, darkMode} from './TodoList.style';

class TodoList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          inputTodoName: '',
          inputTodoDate: new Date(0),
          TODOS: null,
          dtmode: 'date',
          dtshow: false,
          inputError: false,
        };
    }

    resetState() {
      this.setState({
        inputTodoName: '',
        inputTodoDate: new Date(0),
        dtshow: false,
        inputError: false,
      });
    }

    async getTodos() {
      const { screenProps } = this.props;
      const { todos } = screenProps;

      await screenProps.fetch();
      const todoList = await todos.findAll();
      this.setState({ TODOS: todoList });
    }

    handleTodoInput = (input) => {
		  const { inputError } = this.state

		  if (inputError) {
		  	this.setState({ inputError: false })
		  }
		  this.setState({ inputTodoName: input })
    }

    showDatePicker = () => {
      this.setState({
        dtmode: "date",
        dtshow: true
      })
    }

    showTimePicker = () => {
      this.setState({
        dtmode: "time",
        dtshow: true
      })
    }

    errorMessage() {
	  	const { inputError } = this.state
	  	const theme = this.props.myStore.theme? lightMode: darkMode;

	  	if (inputError) {
	  		return (
	  				<Text style={theme.ErrorText}>
	  					Both todo name and date are required
	  				</Text>
	  		)
	  	}
    }

    updateDatePicker = (event, newdate) => {
      if (typeof(newdate) === "undefined") return;

      this.setState({
        inputTodoDate: newdate,
        dtshow: Platform.OS === 'ios'
      })
    }

    updateTodoName = (newname) => {
      this.setState({
        inputTodoName: newname,
        dtshow: false
      })
    }

    addTodo = async () => {
      if(this.state.inputTodoName === '') {
        this.setState({ inputError: true })
        return
      }
      if(Date.parse(this.state.inputTodoDate) < Date.parse(new Date())) {
        this.setState({ inputError: true })
        return
      }
      await this.props.screenProps.todos.create(
        {
          name: this.state.inputTodoName,
          dueAt: Date.parse(this.state.inputTodoDate),
          status: false }
        );
      this.setState({
        inputTodoName: '',
        inputTodoDate: new Date(0)
      })
      this.setState({ inputError: false })
      this.getTodos()
    }

    switchTodoStatus = async (todo) => {
      todo.status = !todo.status
      await this.props.screenProps.todos.update(todo)
      this.getTodos()
    }

    todoInputCell(theme) {
        var dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        var timeOptions = { hour: '2-digit', minute:'2-digit' }

        return(
          <View style={theme.TodoInputCell}>
            <TouchableOpacity onPress={this.addTodo}>
              <Image
                style={theme.plusIcon}
                source={require('../../assets/Icons/circleplus.png')}
                resizeMode='contain'
              />
            </TouchableOpacity>
            <TextInput
						  value={this.state.inputTodoName}
						  placeholder='Type something to do'
						  style={[theme.todoName, theme.notDoneTodoName]}
						  onChangeText={this.updateTodoName}
						  placeholderTextColor={'#ccc'}
						  underlineColorAndroid='transparent'
						  onSubmitEditing={this.addTodo}
						  blurOnSubmit={false}
						/>
            <Image
              style={theme.calIcon}
              source={require('../../assets/Icons/calendaricon.png')}
              resizeMode='contain'
            />
            <View style={{flexDirection: 'column', alignItems: 'flex-start'}}>
              <TouchableOpacity onPress={this.showDatePicker}>
                <Text style={theme.todoDate}>
                  {Date.parse(this.state.inputTodoDate) == 0 ?
                   'pick date' : this.state.inputTodoDate.toLocaleDateString('en-US', dateOptions)}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.showTimePicker}>
                <Text style={theme.todoDate}>
                  {Date.parse(this.state.inputTodoDate) == 0 ?
                   'pick time' : this.state.inputTodoDate.toLocaleTimeString('en-US', timeOptions)}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      }

    todoDisplayCell(todo, theme) {
      var dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
      var timeOptions = { hour12: true, hour: 'numeric', minute:'numeric' }
      var date = new Date(todo.dueAt)
      var datestr = date.toLocaleDateString('en-US', dateOptions)
      var timestr = date.toLocaleTimeString('en-US', timeOptions)
      var status = todo.status

      return(
        <View style={[theme.TodoDisplayCell, status ? theme.DoneTodoCell : theme.NotDoneTodoCell]} key={todo._id}>
          <TouchableOpacity onPress={() => this.switchTodoStatus(todo)}>
              <Image
              style={status ? theme.tickedIcon : theme.untickedIcon}
              source={status ? require('../../assets/Icons/circletick.png') : require('../../assets/Icons/circle.png')}
              resizeMode='contain'
              />
          </TouchableOpacity>
          <Text style={[theme.todoName, status ? theme.doneTodoName : theme.notDoneTodoName]}>
            {todo.name}
          </Text>
          <Image
            style={theme.calIcon}
            source={require('../../assets/Icons/calendaricon.png')}
            resizeMode='contain'
            />
          <View style={{flexDirection: 'column', alignItems: 'flex-start'}}>
            <Text style={theme.todoDate}>{datestr}</Text>
            <Text style={theme.todoDate}>{timestr}</Text>
          </View>
        </View>
      );
    }

    renderNotDoneTodoList(theme) {
      const { TODOS } = this.state;
      if ( TODOS === null ) {
        return;
      }
      const NotDoneTodos = TODOS.filter((todo) => {return(todo.status === false)})
      if ( NotDoneTodos.length == 0) {
        return(
          <Text style={theme.SectionPlaceHolder}>
            Tasks that require your attention will be displayed here
          </Text>
        );
      }
      return (NotDoneTodos.map( (todo) => {
        return this.todoDisplayCell(todo, theme)}
         ));
    }

    renderDoneTodoList(theme) {
      const { TODOS } = this.state;

      if ( TODOS === null ) {
        return;
      }
      const DoneTodos = TODOS.filter((todo) => {return(todo.status === true)})
      if ( DoneTodos.length == 0) {
        return(
          <Text style={theme.SectionPlaceHolder}>
            Tasks that you completed will be displayed here
          </Text>
        );
      }
      return (DoneTodos.map( (todo) => {
        return this.todoDisplayCell(todo, theme)}
       ));
    }

    render() {
        const theme = this.props.myStore.theme? lightMode: darkMode;
        const themeValue = this.props.myStore.theme;

        return (
            <View style={theme.container}>
              <NavigationEvents onWillFocus={() => this.getTodos()} onWillBlur={() => this.resetState()}/>
                <TopBar theme={themeValue} navigation={this.props.navigation} title="Todo" />
                  {this.errorMessage()}
                  <ScrollView>
                        {this.todoInputCell(theme)}
                      <Text style={theme.SectionHeader}>Not done</Text>
                        <View>
                          {this.renderNotDoneTodoList(theme)}
                        </View>
                      <Text style={theme.SectionHeader}>Done</Text>
                        <View>
                          {this.renderDoneTodoList(theme)}
                        </View>
                  </ScrollView>
                  {/* toggle dtshow on leave */}
                  {this.state.dtshow && (
                  <DateTimePicker
                    testID="dateTimePicker"
                    timeZoneOffsetInMinutes={0}
                    value={this.state.inputTodoDate}
                    mode={this.state.dtmode}
                    is24Hour={true}
                    display="default"
                    onChange={this.updateDatePicker}
                    minimumDate={new Date()}
                  />
                  )}
            </View>
        );
    }
}

const mapStateToProps = (state) => {
	const { myStore } = state;
	return { myStore };
  };

export default connect(mapStateToProps)(TodoList);