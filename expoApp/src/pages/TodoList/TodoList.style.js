import { StyleSheet } from 'react-native'

export const lightMode = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      flexDirection: 'column',

    },
    text: {
      color: 'white',
      paddingVertical: 10,
    },
    SectionHeader: {
      fontSize: 30,
      padding: 10,
      color: '#AAA',
    },
    SectionPlaceHolder: {
      textAlign: 'center',
      fontSize: 15,
      padding: 15,
      color: '#888',
    },
    TodoInputCell: {
      flexDirection: 'row',
      borderRadius: 8,
      backgroundColor: '#FFF',
      borderColor: '#E2E2E2',
      borderWidth: 2,
      alignItems: 'center',
      marginHorizontal: '6%',
      padding: 8
    },
    TodoDisplayCell: {
      flexDirection: 'row',
      alignItems: 'center',
      borderRadius: 8,
      marginHorizontal: '6%',
      padding: 8
    },
    NotDoneTodoCell: {
      backgroundColor: '#F2F2F2',
    },
    DoneTodoCell: {
      backgroundColor: '#D2D2D2',
    },
    todoName: {
        flex: 1,
        fontSize: 15,
    },
    doneTodoName: {
      textDecorationLine: 'line-through',
      color: 'grey',
    },
    notDoneTodoName: {
      color: 'black',
      fontWeight: 'bold',
    },
    todoDate: {
      color: "grey",
      textAlign: 'right'
    },
    calIcon: {
      margin: 8,
      width: 30,
      height: 30,
      opacity: 0.5
    },
    untickedIcon: {
      margin: 8,
      width: 30,
      height: 30,
      opacity: 0.2
    },
    tickedIcon: {
      margin: 8,
      width: 30,
      height: 30,
      opacity: 1
    },
    plusIcon: {
      margin: 8,
      width: 30,
      height: 30,
      opacity: 0.5
    },
    ErrorText: {
      fontSize: 16,
      color: 'red',
    },
});

export const darkMode = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#676D78',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    text: {
        color: 'white',
        paddingVertical: 10,
    },
    SectionHeader: {
      fontSize: 30,
      padding: 10,
      color: '#AAA',
    },
    SectionPlaceHolder: {
      textAlign: 'center',
      fontSize: 15,
      padding: 15,
      color: '#888',
    },
    TodoInputCell: {
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: '#5A5A5A',
      borderColor: '#222',
      borderRadius: 8,
      marginHorizontal: '6%',
      padding: 8
    },
    TodoDisplayCell: {
      flexDirection: 'row',
      alignItems: 'center',
      borderRadius: 8,
      marginHorizontal: '6%',
      padding: 8
    },
    NotDoneTodoCell: {
      backgroundColor: '#4A4A4A',
    },
    DoneTodoCell: {
      backgroundColor: '#2A2A2A',
    },
    todoName: {
        flex: 1,
        fontSize: 15,
    },
    doneTodoName: {
      textDecorationLine: 'line-through',
      color: 'lightgrey',
    },
    notDoneTodoName: {
      color: 'lightgrey',
      fontWeight: 'bold',
    },
    todoDate: {
      color: "grey",
      textAlign: 'right'
    },
    calIcon: {
      margin: 8,
      width: 30,
      height: 30,
      opacity: 0.5
    },
    untickedIcon: {
      margin: 8,
      width: 30,
      height: 30,
      opacity: 0.2
    },
    tickedIcon: {
      margin: 8,
      width: 30,
      height: 30,
      opacity: 1
    },
    plusIcon: {
      margin: 8,
      width: 30,
      height: 30,
      opacity: 0.5
    },
    ErrorText: {
      fontSize: 16,
      color: 'red',
    },
});