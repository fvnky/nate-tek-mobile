import { combineReducers } from 'redux';

const INITIAL_STATE = {
    theme: true,
};

const DataReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SET_THEME': {
      const newTheme = action.payload;
      const newState = { ...state };
      newState.theme = newTheme;
      return newState;
    }
    default: {
      return state;
    }
  }
};

export default combineReducers({
  myStore: DataReducer,
});
