import { Dimensions } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';

import LibraryTabNavigator from './LibraryTabNavigator';
import LibraryStackNavigator from './LibraryStackNavigator';

import ProfileTabNavigator from './ProfileTabNavigator';
import SettingsPage from '../pages/SettingsPage/SettingsPage.js';
import TodoList from '../pages/TodoList/TodoList';

import SideMenuDrawer from './SideMenueDrawer';

const WIDTH = Dimensions.get('window').width;

const DrawerConfig = {
    drawerWidth: WIDTH*0.83,
}

const DrawerNavigator = createDrawerNavigator(
    {
        Library:        { screen: LibraryStackNavigator },
        Profile:        { screen: ProfileTabNavigator },
        Settings:       { screen: SettingsPage },
        TodoList:       { screen: TodoList },
    },
    {
        contentComponent: SideMenuDrawer,
        DrawerConfig: DrawerConfig,
    }
);

export default createAppContainer(DrawerNavigator);