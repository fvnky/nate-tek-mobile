import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import LibraryTabNavigator from './LibraryTabNavigator'

import QuizzStack from '../pages/SubjectPage/QuizzPage/QuizzStack'

const LibraryStackNavigator = createStackNavigator({
    LibraryTabNavigator:    { screen: LibraryTabNavigator },
    Quizz:                  { screen: QuizzStack },
}, { headerMode : 'none' });

export default createAppContainer(LibraryStackNavigator);