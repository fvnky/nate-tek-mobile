import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

// import TopBar                   from  '../component/TopBar';

import { lightMode, darkMode }  from './TabNavigator.style';
import TopBar                   from  '../component/TabTopBar';

import tpmStackNav              from './tmpStackNav'
import DraftPage                from '../pages/DraftPage/DraftPage';

// import LibraryPage              from '../pages/LibraryPage/Library';
// import EditPage                 from '../pages/EditPage/EditPage';

const TabBar = (props) => {
  const {
    navigation,
    getLabelText,
    onTabPress,
    onTabLongPress,
    getAccessibilityLabel,
  } = props;

  const theme = props.myStore.theme? lightMode: darkMode;
  const themeValue = props.myStore.theme;
  const { routes } = navigation.state;

  return (
    <View style={theme.container}>
      <TopBar theme={themeValue}  navigation={navigation} title='NATE'/>
      <View style={theme.row}>
        {routes.map((route, index) => {

          // TODO: styles for active 

          return (
            <TouchableOpacity
              key={route.key}
              style={ index === navigation.state.index ? theme.tabButtonActive : theme.tabButton }
              onPress={() => { onTabPress({ route }); }}
              onLongPress={() => { onTabLongPress({ route }); }}
              accessibilityLabel={getAccessibilityLabel({ route })}
            >
              <Text style={theme.h1}>{getLabelText({ route })}</Text>
            </TouchableOpacity>
          )
        })}
      </View>
    </View>
  )
}

const mapStateToProps = (state) => {
	const { myStore } = state;
	return { myStore };
};

const test = connect(mapStateToProps)(TabBar);


const TabConfig = {
}

const LibraryTabNavigator = createMaterialTopTabNavigator(
  {
    all:        { screen: tpmStackNav },
    Draft:      { screen: DraftPage },
  },
  {
    tabBarComponent: test,
    tabBarOptions: TabConfig,
  }
);

export default createAppContainer(LibraryTabNavigator);

