import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import LoginPage1       from '../pages/LoginPage/LoginPage.1';
import LoginPage3       from '../pages/LoginPage/LoginPage.3';
import LoginPage2       from '../pages/LoginPage/LoginPage.2';
import BackStop         from '../pages/LoginPage/BackStop';
import DrawerNavigator  from './DrawerNavigator'

const MainStack = createStackNavigator({
    Login1          : { screen : LoginPage1 },
    Login2          : { screen : LoginPage2 },
    Login3          : { screen : LoginPage3 },
    BackStop        : { screen : BackStop },
    Home            : { screen : DrawerNavigator},
}, { headerMode : 'none' });

export default createAppContainer(MainStack);
