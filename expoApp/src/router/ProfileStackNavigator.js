import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import ProfileTabNavigatorScreen from './ProfileTabNavigator'

const SettingsStackNavigator = createStackNavigator({
    ProfileTabNavigator:    { screen: ProfileTabNavigatorScreen },
}, { headerMode : 'none' });

export default createAppContainer(SettingsStackNavigator);