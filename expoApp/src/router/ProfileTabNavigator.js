import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import { lightMode, darkMode }  from './TabNavigator.style';
import TopBar                   from  '../component/TabTopBar';

import ProfilePage              from '../pages/ProfilePage/Profile';

const TabBar = (props) => {
  const {
    navigation,
    getLabelText,
    onTabPress,
    onTabLongPress,
    getAccessibilityLabel,
  } = props;

  const theme = props.myStore.theme? lightMode: darkMode;
  const themeValue = props.myStore.theme;
  const { routes } = navigation.state;

  return (
    <View style={theme.container}>
      <TopBar theme={themeValue}  navigation={navigation} title='NATE'/>
      <View style={theme.row}>
        {routes.map((route) => (
          <TouchableOpacity
            key={route.key}
            style={theme.tabButton}
            onPress={() => { onTabPress({ route }); }}
            onLongPress={() => { onTabLongPress({ route }); }}
            accessibilityLabel={getAccessibilityLabel({ route })}
          >
            <Text style={theme.h1}>{getLabelText({ route })}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  )
}

const mapStateToProps = (state) => {
	const { myStore } = state;
	return { myStore };
  };
  
const test = connect(mapStateToProps)(TabBar);

const TabConfig = {
}

const ProfileTabNavigator = createMaterialTopTabNavigator({
    Profile:    { screen: ProfilePage },
    },
    {
      tabBarComponent: test,
      tabBarOptions: TabConfig,
    }
);

const ProfileTabNav = createAppContainer(ProfileTabNavigator);
export default ProfileTabNav;

