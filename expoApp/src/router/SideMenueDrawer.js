import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { NavigationEvents } from 'react-navigation'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Feather from 'react-native-vector-icons/Feather'

class SideMenuDrawer extends Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	async handleLogOut() {
		const { navigation, screenProps } = this.props
		const res = await screenProps.user.logOut()
		navigation.navigate('Login1')
	}

	navLink(nav, text, icon) {
		const { navigation } = this.props
		const theme = this.props.myStore.theme ? lightMode : darkMode
		const color = this.props.theme ? '#000' : '#8F9BB3'

		return (
			<TouchableOpacity
				style={theme.linkContainer}
				onPress={() => navigation.navigate(nav)}
			>
				<Feather style={theme.iconLink} name={icon} size={35} color={color} />
				<Text style={theme.link}>{text}</Text>
			</TouchableOpacity>
		)
	}

	render() {
		const theme = this.props.myStore.theme ? lightMode : darkMode
		const { navigation } = this.props

		return (
			<View style={theme.menuContainer}>
				<View style={theme.menuHeader}>
					{/* <TouchableOpacity onPress={() => navigation.navigate('Profile')}> */}
					<View style={theme.nateLogoContainer}>
						<Image
							style={theme.nateLogo}
							source={require('../assets/nate-logo.png')}
						/>
						<Text style={theme.nateTitle}>NATE</Text>
					</View>
					{/* <View style={theme.userNameContainer}>
                        <Text style={theme.userName}>Q. Camboulives</Text>
                    </View> */}
					{/* </TouchableOpacity> */}
				</View>
				<View style={theme.centerLinks}>
					{this.navLink('Library', 'Subjects', 'book-open')}
					{this.navLink('TodoList', 'Todo', 'check-square')}
					{this.navLink('Profile', 'Profile', 'user')}
					{this.navLink('Settings', 'Settings', 'settings')}
				</View>
				<View style={theme.bottomLinks}>
					<TouchableOpacity
						style={theme.linkContainer}
						onLongPress={() => this.handleLogOut()}
					>
						<Feather
							style={theme.iconLink}
							name='power'
							size={35}
							color={this.props.theme ? '#000' : '#8F9BB3'}
						/>
						<Text style={theme.link}>Logout</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	const { myStore } = state
	return { myStore }
}

export default connect(mapStateToProps)(SideMenuDrawer)

const lightMode = StyleSheet.create({
	// borderColor: 'red',
	// borderWidth: 1

	// ----------- Menu Container -----------
	menuContainer: {
		flex: 1,
		backgroundColor: '#F7F8FC',
	},

	// ----------- Header -----------
	menuHeader: {
		height: 150,
		backgroundColor: '#F7F8FC',
		flexDirection: 'column',
		justifyContent: 'center',
	},
	nateLogoContainer: {
		flexDirection: 'row',
		width: '100%',
	},
	nateLogo: {
		borderRadius: 10,
		width: 50,
		height: 50,
		margin: 10,
	},
	nateTitle: {
		flex: 1,
		fontSize: 40,
		letterSpacing: 20,
		fontWeight: '100',
		color: '#3D445B',
		marginLeft: 20,
		height: 75,
		lineHeight: 65,
		justifyContent: 'center',
		alignItems: 'center',
	},
	userNameContainer: {
		flexDirection: 'column',
		borderWidth: 1,
		borderColor: 'white',
	},
	userName: {
		fontSize: 25,
		fontWeight: '400',
		color: 'white',
	},

	// ----------- Center Links -----------
	centerLinks: {
		flex: 3,
		width: '80%',
		backgroundColor: '#FEFFFE', // TODO rendre le backgroud modifiable
		flexDirection: 'column',
		justifyContent: 'center',
		borderTopRightRadius: 20,
		borderWidth: 1,
		borderColor: '#E3E9F1',
	},

	// ----------- Links -----------
	linkContainer: {
		height: 75,
		flexDirection: 'row',
	},

	iconLink: {
		height: '100%',
		padding: 15,
		marginRight: 20,
		flexDirection: 'column',
		justifyContent: 'center',
		alignContent: 'center',
	},

	link: {
		flex: 1,
		fontSize: 20,
		fontWeight: '400',
		letterSpacing: 2,
		color: '#3D445B',
		height: 75,
		lineHeight: 65,
		justifyContent: 'center',
		alignItems: 'center',
	},

	// ----------- Bottom Links -----------
	bottomLinks: {
		width: '80%',
		height: 150,
		flexDirection: 'column',
		borderWidth: 1,
		borderTopWidth: 0,
		borderColor: '#E3E9F1',
		backgroundColor: '#FEFFFE',
	},
})

const darkMode = StyleSheet.create({
	// ----------- Menu Container -----------
	menuContainer: {
		flex: 1,
		backgroundColor: '#1A2138',
	},

	// ----------- Header -----------
	menuHeader: {
		height: 150,
		backgroundColor: '#1A2138',
		flexDirection: 'column',
		justifyContent: 'center',
	},
	nateLogoContainer: {
		flexDirection: 'row',
		width: '100%',
	},
	nateLogo: {
		borderRadius: 10,
		width: 50,
		height: 50,
		margin: 10,
	},
	nateTitle: {
		flex: 1,
		fontSize: 40,
		letterSpacing: 20,
		fontWeight: '100',
		color: 'white',
		marginLeft: 20,
		height: 75,
		lineHeight: 65,
		justifyContent: 'center',
		alignItems: 'center',
	},
	userNameContainer: {
		flexDirection: 'column',
		borderWidth: 1,
		borderColor: 'white',
	},
	userName: {
		fontSize: 25,
		fontWeight: '400',
		color: 'white',
	},

	// ----------- Center Links -----------
	centerLinks: {
		flex: 3,
		width: '80%',
		backgroundColor: '#222B45', // TODO rendre le backgroud modifiable
		flexDirection: 'column',
		justifyContent: 'center',
		borderTopRightRadius: 20,
		borderWidth: 1,
		borderColor: '#101426',
	},

	// ----------- Links -----------
	linkContainer: {
		height: 75,
		flexDirection: 'row',
	},

	iconLink: {
		height: '100%',
		padding: 15,
		marginRight: 20,
		flexDirection: 'column',
		justifyContent: 'center',
		alignContent: 'center',
	},

	link: {
		flex: 1,
		fontSize: 20,
		fontWeight: '400',
		letterSpacing: 2,
		color: '#D1D0DB',
		height: 75,
		lineHeight: 65,
		justifyContent: 'center',
		alignItems: 'center',
	},

	// ----------- Bottom Links -----------
	bottomLinks: {
		width: '80%',
		height: 150,
		flexDirection: 'column',
		borderWidth: 1,
		borderTopWidth: 0,
		borderColor: '#101426',
		backgroundColor: '#222B45',
	},
})
