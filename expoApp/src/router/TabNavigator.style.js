import { StyleSheet } from "react-native";

export const lightMode = StyleSheet.create({
  container: {
    flexDirection: "column",
    height: 100,
    elevation: 5,
  },
  row: {
    flexDirection: "row",
    justifyContent: "center",
    height: 50,
    backgroundColor: "#FEFFFE",
    borderBottomWidth: 1,
    borderColor: '#F0F3F8',
  },
  tabButton: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  tabButtonActive: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: '#1EC0CA',
    borderBottomWidth: 2,
  },
  h1: {
    fontSize: 14,
    color: "#96A1B8",
    textTransform: "capitalize"
  },
});

export const darkMode = StyleSheet.create({
  container: {
    flexDirection: "column",
    height: 100,
    elevation: 5,
  },
  row: {
    flexDirection: "row",
    justifyContent: "center",
    height: 50,
    backgroundColor: "#222B45",
    borderBottomWidth: 1,
    borderColor: '#151A30',
  },
  tabButton: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  tabButtonActive: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: '#1EC0CA',
    borderBottomWidth: 2,
  },
  h1: {
    fontSize: 14,
    color: "#8E9AB2",
    textTransform: "capitalize"
  }
});
