import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SubjectPage  from '../pages/SubjectPage/SubjectPage';
// import QuizzStack   from '../pages/SubjectPage/QuizzPage/QuizzStack';
import CoursePage from '../pages/SubjectPage/CoursePage/CoursePage';

const tpmStackNav = createStackNavigator({
    SubjectPage:    { screen: SubjectPage },
    // Quizz:          { screen: QuizzStack },
    Course:         { screen: CoursePage},
}, { headerMode : 'none' });

export default createAppContainer(tpmStackNav);