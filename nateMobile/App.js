/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react'
import { View } from 'react-native'

import MainRouter from './android/app/src/router/MainRouter'

// import Database from './android/app/src/localDatabase'
import Database from './android/app/src/database'

export default class App extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			data: new Database(),
			isLoaded: false,
		}
	}

	async componentDidMount() {
		await this.state.data.init()
		this.setState({ isLoaded: true })
	}

	render() {
		const { data, isLoaded } = this.state

		return (
			<View style={{ flex: 1 }}>
				{/* // ? splash screen */}
				{isLoaded ? <MainRouter screenProps={data} /> : <View />}
			</View>
		)
	}
}
