import React, { Component } from 'react';
import { Editor } from 'slate-react';
import cx from 'classnames';
import TextareaAutosize from 'react-autosize-textarea';

import defaultValue from '../../../constants/defaultValue';

import IconFolderOutline from '../../../assets/icons/IconFolderOutline';
import IconFolderFill from '../../../assets/icons/IconFolderFill';
import { FOLDER, SUBJECT, DRAFT, DRAFT_ID } from '../../../database/constants';

import styles from './Editor.less';

// Typescript
import {
   ICourse,
   ISubject,
   IFolder,
   EditorTab,
   ToastOptions,
   ToggleModal
} from '../../../typescript/interface';

import PdfViewer from './plugins/pdf/PdfViewer';

import CollapseOnEscape from './plugins/collapse-on-escape';
import SoftBreak from './plugins/soft-break';
import EditorShortcuts from './plugins/editorShortcuts';
import getEntireText from './plugins/getEntireText';
import HoverMenu from './plugins/hoverMenu/hoverMenu';
import Paragraph from './plugins/paragraph/paragraph';
import BasicFormatting from './plugins/basicFormatting';
import Highlight from './plugins/highlight/highlight';
import List from './plugins/list/list';
import Heading from './plugins/heading/heading';
import HtmlPaste from './plugins/htmlPaste';
import SuperScript from './plugins/superScript';
import pdf from './plugins/pdf/pdf';
import image from './plugins/image/image';
import Divider from './plugins/divider/divider';
import Quote from './plugins/quote/quote';
import InsertMenu from './plugins/insertMenu/insertMenu.js';
import Outline from './plugins/outline/outline.js';

interface IProps {
   toggleModal: ToggleModal;
   db: any;
   showToast: (options: ToastOptions) => void;
   theme: string;
   // saveCourse: (id: string, value: any) => void;
   changeCourseName: (id: string, newName: any) => void;
   index: number;
   courses: ICourse[];
   subjects: ISubject[];
   folders: IFolder[];
   activeTab?: EditorTab;
   onChange: (value: any) => void;
   updateEditorTabIndex: (index: number, tab: EditorTab) => void;
}

interface IState {
   hoverParent: boolean;
}

export default class NateEditor extends Component<IProps, IState> {
   editor: any;

   plugins = [
      EditorShortcuts({ nateEditor: this }),
      Paragraph(),
      SoftBreak(),
      CollapseOnEscape(),
      BasicFormatting(),
      Heading(),
      HtmlPaste(),
      SuperScript(),
      image(),
      pdf(),
      Highlight(),
      List(),
      Divider(),
      Quote(),
      HoverMenu(),
      getEntireText(),
      InsertMenu()
      // Outline()
   ];

   state = {
      hoverParent: false
   };

   setHoverParent = (value) => e => {
      this.setState({ hoverParent: value });
   }

   onClickParent = () => {
      const { toggleModal, courses,activeTab  } = this.props;
      const course = courses.find( (course: ICourse) => course._id === activeTab.id );

      toggleModal("ModalMoveResource", {resource: course});
   }

   getParent = (course: ICourse) => {
      const { subjects, folders } = this.props;

      if (course.parent.collection === SUBJECT) {
         return subjects.find(s => s._id === course.parent.id);
      } else {
         return folders.find(f => f._id === course.parent.id);
      }
   };

   closePdfViewer = () => {
      if (this.editor) this.editor.closePdfViewer();
   };

   setRef = (editor: any) => {
      this.editor = editor;
   };

   onCourseNameChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
      const newValue = e.target.value;
      const lastChar = newValue[newValue.length - 1];
      console.log(this.editor);
      if (lastChar === '\n') {
         if (this.editor) {
            // this.editor.moveAnchorToStartOfDocument();
            this.editor.moveFocusToStartOfDocument();
            this.editor.focus();
         }
         return;
      }
      const { updateEditorTabIndex, index, activeTab } = this.props;
      activeTab.temporaryName = newValue;
      updateEditorTabIndex(index, activeTab);
   };

   onCourseNameBlur = () => {
      const { activeTab, changeCourseName } = this.props;
      if (activeTab.status === 'SAVED') {
         changeCourseName(activeTab.id, activeTab.temporaryName);
      }
   };

   saveCurrentCourse = () => {
      const { activeTab, showToast, db, courses } = this.props;
      if (activeTab.status === "SAVED") {
         const course = courses.find((course: ICourse) => course._id === activeTab.id);
         db.courses
            .update(course, activeTab.value)
            .then(() => showToast({ text: 'Saved', iconName: 'check' }))
            .catch(err => console.error(err));
         // saveCourse(activeTab.id, activeTab.value);
      } else if (activeTab.status === "NEW") {
         const draft = {name: activeTab.temporaryName, parent: {collection: DRAFT, id: DRAFT_ID}}
         db.courses
            .create(draft, activeTab.value)
            .then(() => showToast({ text: 'Saved in drafts', iconName: 'check' }))
            .catch(err => console.error(err));
      }

   };

   render() {
      const { activeTab, onChange, courses, theme } = this.props;
      const { hoverParent } = this.state;
      const course = activeTab ? courses.find((c: ICourse) => c._id === activeTab.id) : null;
      console.warn(">>>", course)
      const parentName = activeTab.status === 'NEW' || course.parent.collection === DRAFT ? 'Draft' : this.getParent(course).name;
      const pdfViewer = activeTab.value ? activeTab.value.data.get('pdfViewer') : null;
      if (activeTab.status === 'LOADING') console.warn('LOADIIING');
      if (this.editor) console.log(this.editor);
      return (
         <div className={cx(theme, styles.editor5)}>
            {/* <div>{activeTab ? `status: ${activeTab.status}, id: ${activeTab.id}` : ""}</div>
            <div>{course ? `Last saved ${timeDifference(new Date().getTime(), getLastSavedOrCreatedAt(course))}` : ""}</div> */}
            <div className={styles.parentFolder} onMouseOver={this.setHoverParent(true)} onMouseLeave={this.setHoverParent(false)} onClick={this.onClickParent}  >
               {/* {!hoverParent ? <IconFolderOutline className={cx(theme, styles.iconFolder)} onMouseEnter={this.setHoverParent(true)} /> :
                  <IconFolderFill onClick={this.onClickParent} className={cx(theme, styles.iconFolder)} onMouseLeave={this.setHoverParent(false)} />} */}
                  {hoverParent &&
                  <IconFolderFill className={cx(theme, styles.iconFolder)} />}
               <div className={cx(theme, styles.parentName)}>{parentName}</div>
            </div>
            <TextareaAutosize
               placeholder="Untitled"
               className={cx(theme, styles.courseNameTextArea)}
               value={activeTab.temporaryName}
               onChange={this.onCourseNameChange}
               onBlur={this.onCourseNameBlur}
            />
            <Editor
               placeholder="Type something awesome ..."
               style={{ minHeight: '1000px', cursor: 'text' }}
               value={
                  activeTab && activeTab.value ? activeTab.value : defaultValue
               }
               onChange={onChange}
               ref={this.setRef}
               plugins={this.plugins}
               spellCheck={true}
            />
            {/* {this.editor && this.editor.getNodeForPlusButton() !== null &&  <IconPlusButton className={styles.plusButton} />} */}
            {pdfViewer && pdfViewer.visible && (
               <PdfViewer close={this.closePdfViewer} file={pdfViewer.file} />
            )}
            {/* {activeTab && activeTab.value && (
               <pre
                  style={{
                     // width: "100%",
                     // height: "50%",
                     // overflowY: "auto",
                     padding: "1em",
                     // background: "black",
                     // color: "lawngreen"
                  }}
               >
                  {JSON.stringify(activeTab.value.toJSON(), null, 2)}
               </pre>
            )} */}
         </div>
      );
   }
}
