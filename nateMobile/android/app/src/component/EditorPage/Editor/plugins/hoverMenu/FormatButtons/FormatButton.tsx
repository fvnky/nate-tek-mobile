import React from 'react';
import cx from 'classnames';
import styles from './FormatButton.less';

interface IProps {
   theme: string;
   icon?: any;
   text?: string;
   callback?: () => void;
   isActive?: any;
}

export default function FormatButton(props: IProps) {
   const {
      theme,
      icon,
      text,
      isActive = () => false,
      callback = () => 1
   } = props;

   const onMouseDown = event => {
      event.preventDefault();
      event.stopPropagation();
      callback();
   };

   if (icon) {
      return (
         <span
            role="button"
            className={
               isActive() ? styles.iconWrapperActive : styles.iconWrapper
            }
            onMouseDown={onMouseDown}
         >
            <icon.type className={cx(theme, styles.svg)} />
         </span>
      );
   }
   return (
      <span role="button" className={styles.iconWrapper} onMouseDown={callback}>
         <span
            className={`${styles.textIcon} ${
               isActive() ? styles.textIconActive : ''
            }`}
         >
            {text}
         </span>
      </span>
   );
}
