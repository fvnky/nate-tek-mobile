// import { Popover } from 'antd';
import * as React from 'react';

import { JSDOM } from 'jsdom';
import fetch from 'node-fetch';
// import IconAddImage from '../../../assets/icons/IconAddImage';

import styles from './ImageFinderJS.less';

const opts = {
   headers: {
      'user-agent':
         'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
   }
};

/*
interface IProps {
   insertImageCallback: (imageUrl: string) => void;
   className: any;
}

interface IState {
   searchInputValue: string,
   images: any
}
*/

export default class ImageFinderJS extends React.Component {
   state = {
      searchInputValue: '',
      images: []
   };

   searchInput = (
      <div className={styles.containerSearchInput}>
         <span className={styles.popOverTitle}>Insert Image</span>
         <input
            type="search"
            placeholder="Enter keywords..."
            onKeyDown={this.handleKeyPress}
            onChange={this.handleChange}
            className={styles.searchInput}
         />
      </div>
   );

   handleChange = event => {
      this.setState({ searchInputValue: event.target.value });
   };

   handleKeyPress = e => {
      console.log(e)

      const { searchInputValue } = this.state;
      console.log(searchInputValue)

      if (e.keyCode === 13) {
         e.stopPropagation();
         console.log('will search for ', e.target.value);
         this.getThumnails(searchInputValue);
      }
   };

   getThumnails = keyword => {
      fetch(
         `https://www.google.com/search?q=${keyword}&espv=2&biw=1366&bih=667&site=webhp&source=lnms&tbm=isch&sa=X&ei=XosDVaCXD8TasATItgE&ved=0CAcQ_AUoAg`,
         opts
      )
         .then(res => {
            res.text()
               .then(body => {
                  const dom = new JSDOM(body, { includeNodeLocations: true });
                  const { document } = dom.window;
                  // const { bodyEl } = document; // implicitly created
                  // const pEl = document.querySelector('p');
                  // const textNode = pEl.firstChild;
                  // const imgEl = document.querySelector('img');

                  //  console.log(body);
                  const _images = [];
                  const temp = document.getElementsByClassName(
                     'rg_meta notranslate'
                  );
                  for (let i = 0; i < 30; i += 1) {
                     const j = JSON.parse(temp[i].innerHTML);
                     /*               console.log('Image ' + i + ' Thumbnail URL:');
              console.log(j.tu);
              console.log('Image ' + i + ' URL:');
              console.log(j.ou); */
                     const _image = {
                        thumbnail: j.tu,
                        original: j.ou
                     };
                     _images.push(_image);
                  }
                  this.setState({
                     images: _images
                  });
                  return body;
               })
               .catch(err => console.error(err));
            return res;
         })
         .catch(err => console.error(err));
   };

   renderThumbnails = () => {
      const { images } = this.state;
      const { insertImageCallback } = this.props;
      if (images.length === 0) {
         return;
      }
      console.log('found images', images);
      const newImages = images.map(image => (
         <div className={styles.imageContainer} key={image.thumbnail}>
            <img
               // eslint-disable-next-line jsx-a11y/no-noninteractive-element-to-interactive-role
               role="button"
               onClick={() => insertImageCallback(image.original)}
               className={styles.image}
               src={image.thumbnail}
               alt=""
            />
         </div>
      ));

      return (
         <div className={styles.imagesContainerContainer}>
            <div className={styles.imagesContainer}>{newImages}</div>
         </div>
      );
   };

   render() {
      return (
         <div className={styles.container}>
            {this.searchInput}
            {this.renderThumbnails()}
            {/* <Popover
               placement="left"
               title={this.searchInput}
               content={this.renderThumbnails()}
               trigger="click"
            >
               <IconAddImage className={styles.addImage} />
            </Popover> */}
         </div>
      );
   }
}
