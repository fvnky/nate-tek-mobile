import React, { useEffect, useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import cx from "classnames";
import { JSDOM } from 'jsdom';
import fetch from 'node-fetch';

import Gallery from "./Gallery";
import NateFileUploader from '../../../../Base/NateFileUploader';
import IconSearch from '../../../../../assets/icons/IconSearch';
import IconUpload from '../../../../../assets/icons/IconUpload';
import AddImage from '../../../../../assets/icons/Editor/AddImage';


import styles from './InsertImagePopup.less';

const opts = {
   headers: {
      'user-agent':
         'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
   }
};

function getBase64(file) {
   return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
   });
}

interface IProps {
   theme: string;
   show: boolean;
   editor: any;
   closeMenu: () => void;
}

const InsertMenu: React.FC<IProps> = ({ closeMenu, show, editor, theme }) => {
   const [inputValue, setInputValue] = useState("");
   const [thumbnails, setThumbnails] = useState([]);

   const onInsertImage = e => {
      const file = e.target.files[0];
      getBase64(file)
         .then(data => editor.insertImage(data))
         .catch(err => console.error(err));
   };

   const onKeyDown = (e) => {
      if (e.key === "Enter" && inputValue !== "") {
         console.warn("searched " , inputValue)
         getThumnails(inputValue);
      }
   }

   const onInputChange = (e) => {
      setInputValue(e.target.value)
   }

   const getThumnails = keyword => {
      fetch(
         `https://www.google.com/search?q=${keyword}&espv=2&biw=1366&bih=667&site=webhp&source=lnms&tbm=isch&sa=X&ei=XosDVaCXD8TasATItgE&ved=0CAcQ_AUoAg`,
         opts
      )
         .then(res => {
            res.text()
               .then(body => {
                  const dom = new JSDOM(body, { includeNodeLocations: true });
                  const { document } = dom.window;
                  // const { bodyEl } = document; // implicitly created
                  // const pEl = document.querySelector('p');
                  // const textNode = pEl.firstChild;
                  // const imgEl = document.querySelector('img');

                  //  console.log(body);
                  const _images = [];
                  const temp = document.getElementsByClassName(
                     'rg_meta notranslate'
                  );
                  for (let i = 0; i < 30; i += 1) {
                     const j = JSON.parse(temp[i].innerHTML);
                     /*               console.log('Image ' + i + ' Thumbnail URL:');
              console.log(j.tu);
              console.log('Image ' + i + ' URL:');
              console.log(j.ou); */
                     const _image = {
                        thumbnail: j.tu,
                        original: j.ou
                     };
                     _images.push(_image);
                  }
                  setThumbnails(_images);
                  // this.setState({
                  //    images: _images
                  // });
                  // console.warn(_images)
                  return body;
               })
               .catch(err => console.error(err));
            return res;
         })
         .catch(err => console.error(err));
   };

   return (
      <CSSTransition
         in={show}
         appear={true}
         timeout={300}
         classNames={{
            enter: styles.enter,
            enterActive: styles.enterActive,
            exit: styles.exit,
            exitActive: styles.exitActive,
         }}
         unmountOnExit
      >
         <div className={styles.insertImagePopup} >
         {/* // onMouseDown={(e) => {e.preventDefault(); e.stopPropagation();}}> */}
            <div className={styles.languette} />
            <div className={styles.languette2} />
            <div className={styles.popup}>
               <div className={styles.header}>
               <div className={cx(theme, styles.upload)}>
               <NateFileUploader onChange={onInsertImage} accept="image/x-png,image/jpeg">
               <IconUpload className={styles.iconUpload} />
                  <div className={styles.uploadLabel}>Upload</div>
               </NateFileUploader>
               </div>
                  <span className={cx(theme, styles.or)}>or</span>
                  <IconSearch className={cx(theme, styles.iconSearch)} />
                  <input autoFocus={true} onKeyDown={onKeyDown} onChange={onInputChange} value={inputValue} className={cx(theme, styles.searchInput)} placeholder="Search the web" />
               </div>
               <Gallery theme={theme} thumbnails={thumbnails} editor={editor} closeMenu={closeMenu}/>
            </div>
         </div>
      </CSSTransition>
   );
};

export default InsertMenu;
