import React from 'react'
import OutlineComponent from "./OutlineComponent";

export default function outline(options = {}) {
  return {
    renderEditor(props, editor, next) {
      const children = next();
      return (
        <>
            <OutlineComponent editor={editor} value={editor.value}/>
          <>{children}</>
         </>
      )
    },
  }
}
