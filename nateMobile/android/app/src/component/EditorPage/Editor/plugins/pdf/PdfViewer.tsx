import React, { Component } from 'react';
import { Document, Page } from 'react-pdf/dist/entry.webpack';

import IconClose from '../../../../../assets/icons/IconClose';

import styles from './PdfViewer.less';

interface IProps {
   file: string;
   close: () => void;
}

interface IState {
   numPages: number;
   pageNumber: number;
}

export default class MyApp extends Component<IProps, IState> {
   state: IState = {
      numPages: null,
      pageNumber: 1
   };

   onDocumentLoadSuccess = ({ numPages }) => {
      this.setState({ numPages });
   };

   render() {
      const { file, close } = this.props;
      const { pageNumber, numPages } = this.state;

      return (
         <div className={styles.layout}>
            <div>
               <IconClose className={styles.iconClose} onClick={close} />
               <Document file={file} onLoadSuccess={this.onDocumentLoadSuccess}>
                  <Page pageNumber={pageNumber} className={styles.page} />
               </Document>
               <p>
                  Page {pageNumber} of {numPages}
               </p>
            </div>
         </div>
      );
   }
}
