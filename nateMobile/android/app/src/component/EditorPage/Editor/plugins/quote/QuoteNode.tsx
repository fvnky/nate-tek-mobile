import React, { Component } from 'react';
import cx from 'classnames';

import withTheme from '../../../../withTheme';

import styles from './QuoteNode.less';

interface IProps {
   theme: string;
   isFocused: boolean;
   children: any;
}

class QuoteNode extends Component<IProps> {
   render() {
      const { isFocused, theme, children } = this.props;

      return (
         <blockquote className={cx(theme, styles.quote)}>{children}</blockquote>
      );
   }
}
export default withTheme(QuoteNode);
