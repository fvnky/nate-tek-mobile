import React from 'react';
import cx from 'classnames';
import styles from './HighlightButton.less';

interface IProps {
   theme: string;
   type: number;
   callback?: (type: number) => void;
   isActive?: (type: number) => boolean;
}

export default function HighlightButton(props: IProps) {
   const { theme, type, isActive = () => false, callback = () => null } = props;

   const onMouseDown = (event: React.MouseEvent) => {
      event.preventDefault();
      event.stopPropagation();
      callback(type);
   };

   return (
      <button
         className={cx(
            theme,
            styles.highlight,
            styles[`highlight-${type}`],
            isActive(type) ? styles.highlightActive : ''
         )}
         // className={`${styles.highlight} ${styles[`highlight-${type}`]} ${theme} ${isActive(type) ? styles.highlightActive : ""}`}
         onMouseDown={onMouseDown}
      />
   );
}
