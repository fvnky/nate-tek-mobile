import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'

export default class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: lightMode,
      color: 'black', 
    };
  }

  render() {
    const { theme, color } = this.state;

    return (
      <View style={theme.bar}>
        <View style={theme.container}>
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Icon style={lightMode.Icon} name="bars" size={35} color={color} ></Icon>
          </TouchableOpacity>
          <Text style={theme.title}>{this.props.title}</Text>
          <TouchableOpacity>
            <Icon style={lightMode.Icon} name="search1" size={35} color={color} ></Icon>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const lightMode = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'white',
      flexDirection: "row",
      justifyContent: 'space-between',
  },

  bar: {
    height: 50,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    elevation: 3,
    backgroundColor: '#676D78',
  },

  title: {
    color: "black",
    fontSize: 25,
    fontFamily: "Source Sans Pro",
    paddingVertical: 10,
  },
  
  Icon: {
    marginVertical: 10,
    marginHorizontal: 5,
  }, 
});

const darkMode = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#4A4A4A',
      flexDirection: "row",
      justifyContent: 'space-between',
  },

  bar: {
    height: 50,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    elevation: 3,
    backgroundColor: '#676D78',
  },

  title: {
    color: "#FFFFFF",
    fontSize: 25,
    fontFamily: "Source Sans Pro",
    paddingVertical: 10,
  },
  
  Icon: {
    height: 40,
    width: 40,
    marginTop: 10,
  }, 
});
