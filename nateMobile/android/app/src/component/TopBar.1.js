import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements'
import { StackNavigator } from 'react-navigation';

export default class TopBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.bar}>
        <View style={styles.container}>
          <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
            <Image
                style={styles.Icon}
                source={require('../assets/Icons/baseline_list_black_48.png')}
            />
          </TouchableOpacity>
          <Text style={styles.title}>{this.props.title}</Text>
          <TouchableOpacity>
            <Image
                style={styles.Icon}
                source={require('../assets/Icons/baseline_search_black_48.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.textcontainer}>
          <TouchableOpacity style={styles.subBox} onPress={this.props.function1}>
            <Text style={styles.text}>{this.props.title1}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.subBox} onPress={this.props.function2}>
            <Text style={styles.text}>{this.props.title2}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.subBox} onPress={this.props.function3}>
            <Text style={styles.text}>{this.props.title3}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'white',
      flexDirection: "row",
      justifyContent: 'space-between',
  },

  textcontainer: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: "row",
    justifyContent: 'space-around',
  },

  subBox: {
    flex: 1,
    flexDirection: "row",
    justifyContent: 'center',
  },

  bar : {
    height: "14%",
    width: "100%",
    flexDirection: "column",
    justifyContent: 'center',
    elevation : 3,
    backgroundColor : "white",
  },

  title: {
    color: "black",
    fontSize: 25,
    fontFamily: "Source Sans Pro",
    paddingVertical: 10,
  },

  text: {
    color: "black",
    fontSize: 15,
    fontFamily: "Source Sans Pro",
    paddingVertical: 10,
  },

  Icon: {
    height: 40,
    width: 40,
    marginTop: 10,
  }, 
});
