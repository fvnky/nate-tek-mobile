import * as React from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'

export default class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={this.props.theme ? darkMode.bar : lightMode.bar}>
        <View style={this.props.theme ? darkMode.container: lightMode.container}>
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Icon style={lightMode.Icon} name="bars" size={35} color={this.props.theme ? "#FFFFFF": "#000000"} ></Icon>
          </TouchableOpacity>
          <Text style={this.props.theme ? darkMode.title: lightMode.title}>{this.props.title}</Text>
          <TouchableOpacity>
            <Icon style={lightMode.Icon} name="search1" size={35} color={this.props.theme ? "#FFFFFF": "#000000"} ></Icon>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const lightMode = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'white',
      flexDirection: "row",
      justifyContent: 'space-between',
  },

  bar : {
    height: "8%",
    width: "100%",
    flexDirection: "column",
    justifyContent: 'space-between',
    elevation : 3,
    backgroundColor : "white",
  },

  title: {
    color: "black",
    fontSize: 25,
    fontFamily: "Source Sans Pro",
    paddingVertical: 10,
  },
  
  Icon: {
    marginVertical: 10,
    marginHorizontal: 5,
  }, 
});

const darkMode = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#4A4A4A',
      flexDirection: "row",
      justifyContent: 'space-between',
  },

  bar : {
    height: "8%",
    width: "100%",
    flexDirection: "column",
    justifyContent: 'space-between',
    elevation : 3,
    backgroundColor : "#4A4A4A",
  },

  title: {
    color: "#FFFFFF",
    fontSize: 25,
    fontFamily: "Source Sans Pro",
    paddingVertical: 10,
  },
  
  Icon: {
    height: 40,
    width: 40,
    marginTop: 10,
  }, 
});
