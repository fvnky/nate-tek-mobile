import { DICTIONARY } from '../constants'

function findAll() {
	return new Promise((ok, ko) => {
		this._main
			.find({ selector: { collection: DICTIONARY } })
			.then((res) => ok(res.docs))
			.catch(ko)
	})
}
export default function() {
	return {
		findAll: findAll.bind(this),
	}
}
