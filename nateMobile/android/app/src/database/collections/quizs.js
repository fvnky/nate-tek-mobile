import { isString } from 'lodash'
import { InvalidParameterFormat, QuizNotFound } from '../errors'
import { isNotFound } from '../utils'
import { QUIZ } from '../constants'

function findAll() {
	return new Promise((ok, ko) => {
		this._main
			.find({ selector: { collection: QUIZ } })
			.then((res) => ok(res.docs))
			.catch(ko)
	})
}

function findById() {
	return new Promise((ok, ko) => {
		if (!isString(id))
			return ko(new InvalidParameterFormat(`The id parameter is not a String`))

		this._main
			.get(id)
			.then((doc) => {
				if (doc.collection !== QUIZ)
					throw new QuizNotFound(
						`The document's id '${doc._id}' does not reference a quiz`
					)

				ok(doc)
			})
			.catch((err) => {
				if (isNotFound(err))
					throw new QuizNotFound(`The quiz '${id}' could not be found`)

				throw err
			})
			.catch(ko)
	})
}

export default function() {
	return {
		findAll: findAll.bind(this),
		findById: findById.bind(this),
	}
}
