import { NATE_TODO } from '../constants'

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll() {
	return new Promise((ok, ko) => {
		this._main
			.find({ selector: { collection: NATE_TODO } })
			.then((res) => ok(res.docs))
			.catch(ko)
	})
}

export default function() {
	return {
		findAll: findAll.bind(this),
	}
}
