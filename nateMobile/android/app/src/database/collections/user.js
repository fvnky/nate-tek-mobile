import _ from 'lodash'

import { USER } from '../constants'
export const PROFILE_PICTURE = 'profile_picture'

// ============================================== //
// ==================== Auth ==================== //
// ============================================== //

async function sendValidation(mail) {
	try {
		await this.request.post(`/auth/validation`, { mail })
	} catch (e) {
		throw e.response ? e.response.data : e
	}
}

async function logIn(mail, code) {
	try {
		const user = await this._session.get(USER)

		const {
			data: { user: profile, token },
		} = await this.request.post('/auth/login', { mail, code })

		this.request.defaults.headers.common.Authorization = `JWT ${token}`

		await this._session.put({
			_id: USER,
			_rev: user._rev,
			profile,
			token,
		})

		await this.fetch()
	} catch (e) {
		throw e.response ? e.response.data : e
	}
}

async function logOut() {
	try {
		const user = await this._session.get(USER)

		await this._session.put({
			_id: USER,
			_rev: user._rev,
			profile: {},
			token: '',
		})

		const { rows } = await this._main.allDocs({
			include_docs: true,
			attachments: true,
		})

		for (const { doc } of rows) await this._main.remove(doc._id, doc._rev)
	} catch (e) {
		throw e.response ? e.response.data : e
	}
}

// ============================================== //
// ==================== Load ==================== //
// ============================================== //

async function loadUser() {
	try {
		const user = await this._session.get(USER)

		const {
			data: { user: profile, token },
		} = await this.request.put('/auth/refresh', { token: user.token })

		this.request.defaults.headers.common.Authorization = `JWT ${token}`

		await this._session.put({
			_id: USER,
			_rev: user._rev,
			profile,
			token,
		})

		await this.fetch()
	} catch (e) {
		const error = e.response ? e.response.data : e

		if (
			error.name === 'InvalidRequestFormat' ||
			error.name === 'InvalidToken' ||
			error.name === 'UserNotFound'
		)
			await this.user.logOut()

		throw error
	}
}

async function getUniversities(query) {
	try {
		const { data } = await this.request.get(`/universities/${query}`)

		return data
	} catch (e) {
		throw e.response ? e.response.data : e
	}
}

async function getCurriculums(query) {
	try {
		const { data } = await this.request.get(`/curriculums/${query}`)

		return data
	} catch (e) {
		throw e.response ? e.response.data : e
	}
}

async function getUser() {
	return this._session.get(USER)
}

export default function() {
	return {
		logIn: logIn.bind(this),
		logOut: logOut.bind(this),
		getUser: getUser.bind(this),
		getCurriculums: getCurriculums.bind(this),
		getUniversities: getUniversities.bind(this),
		sendValidation: sendValidation.bind(this),
		loadUser: loadUser.bind(this),
	}
}
