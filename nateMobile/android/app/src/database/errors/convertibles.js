import { CustomError } from '../factory'

export class ConvertibleNotFound extends CustomError {
  constructor(message) {
    super(message)
    this.message = message || 'The convertible could not be found'
  }
}

export class TypeNotFound extends CustomError {
  constructor(message) {
    super(message)
    this.message = message || 'The type could not be found'
  }
}

export class NoConvertibles extends CustomError {
  constructor(message) {
    super(message)
    this.message = message || 'No convertibles could be found'
  }
}
