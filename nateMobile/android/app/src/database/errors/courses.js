import { CustomError } from '../factory';

export class CourseAlreadyExists extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The course already exists';
   }
}

export class CourseNotFound extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The course could not be found';
   }
}

export class NoCourses extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'No courses could be found';
   }
}
