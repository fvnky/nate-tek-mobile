import { CustomError } from '../factory';

export class UserNotFound extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The user could not be found';
   }
}
