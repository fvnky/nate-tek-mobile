import { DRAFT } from '../constants';

export default {
   definition: Object.freeze({
      _id: { type: String, required: true },
      _rev: { type: String, required: true },
      createdAt: { type: Number, required: true },
      updatedAt: { type: Number, required: false },
      children: [
         {
            collection: { type: String, required: true },
            id: { type: String, required: true }
         }
      ],
      collection: DRAFT
   }),
   fields: [
      '_id',
      '_rev',
      'createdAt',
      'updatedAt',
      'children',
      'collection'
   ]
};
