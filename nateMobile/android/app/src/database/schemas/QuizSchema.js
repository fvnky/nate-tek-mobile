// Constant
import { QUIZ } from '../constants'

export default {
	definition: Object.freeze({
		_id: { type: String, required: true },
		_rev: { type: String, required: true },
		name: { type: String, required: true },
		parent: {
			collection: { type: String, required: true },
			id: { type: String, required: true },
		},
		// children: [{answer: Boolean, type: String, question: String}],
		questions: [
			{
				form: { type: Object, required: true },
				type: { type: String, required: true },
				_id: { type: String, required: true },
			},
		],
		score: { type: Number, required: true },
		createdAt: { type: Number, required: true },
		updatedAt: { type: Number, required: false },
		collection: QUIZ,
	}),
	fields: [
		'_id',
		'_rev',
		'name',
		'parent.collection',
		'parent.id',
		'questions',
		'score',
		'createdAt',
		'updatedAt',
		'collection',
	],
}
