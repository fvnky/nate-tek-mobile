// import PouchDB from 'pouchdb-react-native';

class Database {
  constructor() {
    // this.main = new PouchDB("main");

    this.subject = null;
    this.folder = null;
    this.courses = null;
    this.quizz = null;
    this.selectedQuizz = 1;

    this.user = null;

    this.theme = null;


    // this.subject = listFunctions.call(this);
    // this.generalFunctions = generalFunctions.call(this);
    // this.skillFunctions = skillFunctions.call(this);
    // this.levelFunctions = levelFunctions.call(this);
    // this.inventoryFunctions = inventoryFunctions.call(this);
  }
}

Database.prototype.clear = function () {
    return this.main.destroy()
      .then(() => ('done')).catch((err) => (err));
  };
  

Database.prototype.init = function () {
    this.main = new PouchDB(config.local.main);
    return ('done');
  };

Database.prototype.getSubject = function () {
    // console.warn('character_'+this.selected+'_skill');
    return this.main.get(`SUBJECTS`)
      .then((doc) => {
        this.subject = doc;
        return (doc);
      }).catch((err) => {
        console.warn("#ERROR : " + err);
        return (err);
      });
  };

  Database.prototype.getFolder = function () {
    // console.warn('character_'+this.selected+'_skill');
    return this.main.get(`FOLDERS`)
      .then((doc) => {
        this.folder = doc;
        return (doc);
      }).catch((err) => {
        console.warn("#ERROR : " + err);
        return (err);
      });
  };

  Database.prototype.getCourses = function () {
    // console.warn('character_'+this.selected+'_skill');
    return this.main.get(`COURSES`)
      .then((doc) => {
        this.courses = doc;
        return (doc);
      }).catch((err) => {
        console.warn("#ERROR : " + err);
        return (err);
      });
  };

  Database.prototype.getQuizz = function () {
    // console.warn('character_'+this.selected+'_skill');
    return this.main.get(`QUIZZES`)
      .then((doc) => {
        this.quizz = doc;
        return (doc);
      }).catch((err) => {
        console.warn("#ERROR : " + err);
        return (err);
      });
  };

  Database.prototype.getUser = function () {
    // console.warn('character_'+this.selected+'_skill');
    return this.main.get(`Profile`)
      .then((doc) => {
        this.user = doc;
        return (doc);
      }).catch((err) => {
        console.warn("#ERROR : " + err);
        return (err);
      });
  };

  Database.prototype.getTheme = function () {
    // console.warn('character_'+this.selected+'_skill');
    return this.main.get(`theme`)
      .then((doc) => {
        this.theme = doc;
        return (doc);
      }).catch((err) => {
        this.theme = {
          _id: "theme",
          theme: "darkMode",
          themeValue: true,
        };
        console.warn("#ERROR : " + err);
        return (err);
      });
  };


Database.prototype.createSubject = function () {
  return this.main.put({
    _id: 'SUBJECTS',
    SUBJECTS: [
      {
          id: 1,
          name: 'Math',
          path: '',
          onPressPath: 'math',
          collection: 'SUBJECT',
      },
      {
          id: 2,
          name: 'geometrie',
          path: '',
          onPressPath: 'geometrie',
          collection: 'SUBJECT',
      },
      {
          id: 3,
          name: 'Physics',
          path: '',
          onPressPath: 'physics',
          collection: 'SUBJECT',
      },
      {
          id: 4,
          name: 'Chimie',
          path: '',
          onPressPath: 'geometrie',
          collection: 'SUBJECT',
      },
      {
          id: 5,
          name: 'Francais',
          path: '',
          onPressPath: 'Francais',
          collection: 'SUBJECT',
      },
      {
          id: 6,
          name: 'Anglais',
          path: '',
          onPressPath: 'anglais',
          collection: 'SUBJECT',
      },
      {
          id: 7,
          name: 'Histoire',
          path: '',
          onPressPath: 'histoire',
          collection: 'SUBJECT',
      },
      {
          id: 8,
          name: 'Geographie',
          path: '',
          onPressPath: 'geographie',
          collection: 'SUBJECT',
      },
    ],
    }).then((result) => {
        console.warn(result);
        return (result);
    }).catch((err) => {
        console.warn("#ERROR : " + err);
        return (err);
    });
  };

Database.prototype.createFolder = function () {
    return this.main.put({
        _id: 'FOLDERS',
        FOLDERS: [
          {
              id: 1,
              name: 'math semester 1',
              parrentPath: '',
              path: 'math',
              onPressPath: 'math/math semester 1',
              collection: 'FOLDER',
          },
          {
              id: 2,
              name: 'physics semester 1',
              parrentPath: '',
              path: 'physics',
              onPressPath: 'physics/physics semester 1',
              collection: 'FOLDER',
          },
          {
              id: 3,
              name: 'geometrie semester 1',
              parrentPath: '',
              path: 'geometrie',
              onPressPath: 'geometrie/geometrie semester 1',
              collection: 'FOLDER',
          },
          {
              id: 4,
              name: 'revolution francaise',
              parrentPath: '',
              path: 'histoire',
              onPressPath: 'histoire/revolution francaise',
              collection: 'FOLDER',
          },
          {
              id: 5,
              name: 'la grande guerre',
              parrentPath: '',
              path: 'histoire',
              onPressPath: 'histoire/la grande guerre',
              collection: 'FOLDER',
          },
          {
              id: 6,
              name: 'la seconde guerre mondial',
              parrentPath: '',
              path: 'histoire',
              onPressPath: 'histoire/la seconde guerre mondial',
              collection: 'FOLDER',
          },
        ],
    }).then((result) => {
        console.warn(result);
        return (result);
    }).catch((err) => {
        console.warn("#ERROR : " + err);
        return (err);
    });
  };

  Database.prototype.createCourse = function () {
    return this.main.put({
        _id: 'COURSES',
        COURSES: [
          {
              id: 1,
              name: 'equations',
              path: 'math/math semester 1',
              collection: 'COURSE',
          },
          {
              id: 2,
              name: 'newton',
              path: 'physics/physics semester 1',
              collection: 'COURSE',
          },
          {
              id: 3,
              name: 'trigonometri',
              path: 'geometrie/geometrie semester 1',
              collection: 'COURSE',
          },
          {
              id: 4,
              name: 'les cause de la revolution',
              path: 'histoire/revolution francaise',
              collection: 'COURSE',
          },
          {
              id: 5,
              name: 'napoleon',
              path: 'histoire/revolution francaise',
              collection: 'COURSE',
          },
          {
              id: 6,
              name: 'verdun',
              path: 'histoire/la grande guerre',
              collection: 'COURSE',
          },
  
        ],        
    }).then((result) => {
        console.warn(result);
        return (result);
    }).catch((err) => {
        console.warn("#ERROR : " + err);
        return (err);
    });
  };

  Database.prototype.createQuizz = function () {
    return this.main.put({
        _id: 'QUIZZES',
        QUIZZES: [
          {
              id: 1,
              name: 'equations Quizz',
              path: 'math/math semester 1',
              questions: [
                  {
                      question: "question 1",
                      answers: [
                        {answer: "answer a", correct: false},
                        {answer: "answer b", correct: true},
                        {answer: "answer c", correct: false}
                      ],
                    },
                    {
                      question: "question 2",
                      answers: [
                        {answer: "answer a", correct: true},
                        {answer: "answer b", correct: false},
                        {answer: "answer c", correct: false}
                      ],
                    },
                    {
                      question: "question 3",
                      answers: [
                        {answer: "answer a", correct: false},
                        {answer: "answer b", correct: false},
                        {answer: "answer c", correct: true}
                      ],
                    },
                    {
                      question: "question 4",
                      answers: [
                        {answer: "answer a", correct: false},
                        {answer: "answer b", correct: true},
                        {answer: "answer c", correct: false}
                      ],
                    },
                ],
              collection: 'QUIZZ',
          },
          {
              id: 2,
              name: 'newton Quizz',
              path: 'physics/physics semester 1',
              questions: [
                  {
                      question: "question 1",
                      answers: [
                        {answer: "answer a", correct: false},
                        {answer: "answer b", correct: false},
                        {answer: "answer c", correct: true}
                      ],
                    },
                    {
                      question: "question 2",
                      answers: [
                        {answer: "answer a", correct: true},
                        {answer: "answer b", correct: false},
                        {answer: "answer c", correct: false}
                      ],
                    },
                    {
                      question: "question 3",
                      answers: [
                        {answer: "answer a", correct: true},
                        {answer: "answer b", correct: false},
                        {answer: "answer c", correct: false}
                      ],
                    },
                    {
                      question: "question 4",
                      answers: [
                        {answer: "answer a", correct: false},
                        {answer: "answer b", correct: false},
                        {answer: "answer c", correct: true}
                      ],
                    },
                ],
              collection: 'QUIZZ',
          },
        ],
    }).then((result) => {
        console.warn(result);
        return (result);
    }).catch((err) => {
        console.warn(err);
        return (err);
    });
  };

  Database.prototype.createUser = function () {
    return this.main.put({
        _id: 'Profile',
        name: 'Arthur',
        surName: 'Freton',
        school: 'epitech',
        job: 'student',
        contry: 'france',
        curriculum: "Expert en technologie de l'information",
        academicYear: '2020',
        emails: 'arthur.freton@epitech.eu',
        interests: 'Entrepreneurship, business, Fintech'
    }).then((result) => {
        console.warn(result);
        return (result);
    }).catch((err) => {
        console.warn("#ERROR : " + err);
        return (err);
    });
  }

  Database.prototype.createTheme = function () {
    return this.main.put({
        _id: "theme",
        theme: "darkMode",
        themeValue: true,
    }).then((result) => {
        console.warn(result);
        return (result);
    }).catch((err) => {
        console.warn("#ERROR : " + err);
        return (err);
    });
  }

export default Database;
