import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { NavigationEvents } from 'react-navigation';

// darkMode needed do not remove
import { lightMode, darkMode } from './DevPage.style';
import TopBar from '../../component/TopBar';

export default class DevPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            theme: lightMode,
        };
    }
    
    getData() {

    }

	async handleTest() {
		await this.props.screenProps.fetch()

		const user = await this.props.screenProps.user.getUser()
		const subjects = await this.props.screenProps.subjects.findAll()
		const folders = await this.props.screenProps.folders.findAll()
		const courses = await this.props.screenProps.courses.findAll()
		const dictionary = await this.props.screenProps.dictionary.findAll()
		const drafts = await this.props.screenProps.drafts.findAll()
		const quizs = await this.props.screenProps.quizs.findAll()
		const todos = await this.props.screenProps.todos.findAll()

		console.warn(`User\n`, JSON.stringify(user, null, 2))
		console.warn(`Subjects ${subjects.length}\n`, JSON.stringify(subjects, null, 2))
		console.warn(`Folders ${folders.length}\n`, JSON.stringify(folders, null, 2))
		console.warn(`Courses ${courses.length}\n`, JSON.stringify(courses, null, 2))
		console.warn(`Dictionary ${dictionary.length}\n`, JSON.stringify(dictionary, null, 2))
		console.warn(`Drafts ${drafts.length}\n`, JSON.stringify(drafts, null, 2))
		console.warn(`Quizs ${quizs.length}\n`, JSON.stringify(quizs, null, 2))
		console.warn(`Todos ${todos.length}\n`, JSON.stringify(todos, null, 2))
	}

    // feel free to create new bottons for debuging as follows :
    //
    // <=========================================================>
    //
    // <TouchableOpacity style={this.state.themeValue ? darkMode.buttonContainer : lightMode.buttonContainer} onPress={() => this.newHandlerfonction()}>
    //     <Text style={this.state.themeValue ? darkMode.text : lightMode.text}> Botton text </Text>
    // </TouchableOpacity>
    //
    // <=========================================================>
    //

    render() {
        const {theme} = this.state;

        return (
            <View style={theme.container}>
				<NavigationEvents onWillFocus={() => this.getData()} />
                <TopBar theme={this.state.themeValue} navigation={this.props.navigation} title="Settings" />
                <View style={theme.container}>

                    <TouchableOpacity style={theme.buttonContainer} onPress={() => this.handleTest()}>
                        <Text style={theme.text}>Bonjour</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
