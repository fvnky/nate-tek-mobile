import { StyleSheet } from 'react-native';

export const lightMode = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#F2F2F2',
        marginHorizontal: '10%'
    },
    text: {
        color: 'grey',
        paddingVertical: 10,
    },
});

export const darkMode = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#676D78',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#4A4A4A',
        marginHorizontal: '10%'
    },
    text: {
        color: '#FFFFFF',
        paddingVertical: 10,
    },
});
