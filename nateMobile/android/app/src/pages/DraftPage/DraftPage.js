import React from 'react';
import { View, Text, ScrollView } from 'react-native'
import { NavigationEvents } from 'react-navigation';

// darkMode needed do not remove
import { lightMode, darkMode } from './DraftPage.style';

export default class DraftPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: lightMode,
    };
  }
  
  async getData() {
    
  }
  
  draftList() {
    return (null)
  }

  render() {
    const {theme} = this.state;
    
    return (
      <View style={theme.container}>
        <NavigationEvents onWillFocus={() => this.getData()} />
        <View style={theme.container}>
          <Text style={theme.SubjectTitle}>My Drafts</Text>
          <ScrollView>
            {this.draftList()}
          </ScrollView>
        </View>
      </View>
    );
  }
}
