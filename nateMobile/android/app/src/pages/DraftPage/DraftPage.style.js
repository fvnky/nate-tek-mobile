import { StyleSheet } from 'react-native'

export const lightMode = StyleSheet.create({
    container: {
      backgroundColor: "#F2F2F2",
      flex: 1,
    },
  
    card: {
      flexDirection: "column",
      justifyContent: 'center',
      paddingVertical: 10,
      marginLeft: "5%",
    },
  
    list: {
      justifyContent: 'center',
    },
  
    bar : {
      width: "100%",
      alignItems: 'center',
      justifyContent: 'center',
      elevation : 3,
      backgroundColor : "white",
    },
  
    SubjectTitle: {
      color: "black",
      fontSize: 25,
      fontFamily: "Source Sans Pro",
      marginLeft: "5%",
      marginTop: "5%",
    },
  
    text: {
      color: "black",
      fontSize: 25,
      fontFamily: "Source Sans Pro",
    },
  
  });
  
  export const darkMode = StyleSheet.create({
    container: {
      backgroundColor: "#676D78",
      flex: 1,
    },
  
    card: {
      flexDirection: "column",
      justifyContent: 'center',
      paddingVertical: 10,
      marginLeft: "5%",
    },
  
    list: {
      justifyContent: 'center',
    },
  
    bar : {
      width: "100%",
      alignItems: 'center',
      justifyContent: 'center',
      elevation : 3,
      backgroundColor : "black",
    },
  
    SubjectTitle: {
      color: "#FAFAFA",
      fontSize: 25,
      fontFamily: "Source Sans Pro",
      marginLeft: "5%",
      marginTop: "5%",
    },
  
    text: {
      color: "#FAFAFA",
      fontSize: 25,
      fontFamily: "Source Sans Pro",
    },
  
  });