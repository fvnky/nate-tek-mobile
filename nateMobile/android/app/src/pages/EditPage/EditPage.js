import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity} from 'react-native'
import { NavigationEvents } from 'react-navigation';

import { lightMode, darkMode } from './EditPage.style';

export default class EditPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      themeValue: false,
      theme: lightMode,
      Profile: {
        name: 'firstname',
        surName: 'surname',
        school: 'schoolname',
        job: 'job',
        contry: 'France',
        curriculum: "curiculum",
        academicYear: '',
        emails: 'firstname.surname@email.com',
        interests: '',
      }
    };
  }

  async getData() {
	}

  handleSubmit() {
    // new logic needed
    console.warn('new logic needed');
  }

  handleProfile = () => {

  }

  handleEditor =  () => {

  }

  handleNameChange = (Input) => {
    let tmpProfile = this.state.Profile;
    tmpProfile.name = Input;
    this.setState({Profile: tmpProfile});
  }

  handleSurNameChange = (Input) => {
    let tmpProfile = this.state.Profile;
    tmpProfile.surName = Input;
    this.setState({Profile: tmpProfile});
  }

  handleSchoolChange = (Input) => {
    let tmpProfile = this.state.Profile;
    tmpProfile.school = Input;
    this.setState({Profile: tmpProfile});
  }

  handleInterestsChange = (Input) => {
    let tmpProfile = this.state.Profile;
    tmpProfile.interests = Input;
    this.setState({Profile: tmpProfile});
  }

  editName() {
    const {theme} = this.state;

    return (
      <View style={theme.container2}>
        <View style={theme.rowContainer}>
          <Text style={theme.inputTitle}>Name</Text>
          <View style={theme.inputContainer}>
            <TextInput value={this.state.Profile.name}
              ref={(input) => { this.passwordInput = input;}}
              placeholder={this.state.Profile.name}
              style={theme.Input}
              onChangeText={this.handleNameChange}
              placeholderTextColor={"grey"}
              underlineColorAndroid = "transparent"
              onSubmitEditing={this.handleLogin}
            />
          </View>
        </View>
        <View style={theme.rowContainer}>
          <Text style={theme.inputTitle}>Surname</Text>
          <View style={theme.inputContainer}>
            <TextInput value={this.state.Profile.surName}
              ref={(input) => { this.passwordInput = input;}}
              placeholder={this.state.Profile.surName}
              style={theme.Input}
              onChangeText={this.handleSurNameChange}
              placeholderTextColor={"grey"}
              underlineColorAndroid = "transparent"
              onSubmitEditing={this.handleLogin}
            />
          </View>
        </View>
        <View style={theme.rowContainer}>
          <Text style={theme.inputTitle}>School</Text>
          <View style={theme.inputContainer}>
            <TextInput value={this.state.Profile.school}
              ref={(input) => { this.passwordInput = input;}}
              placeholder={this.state.Profile.name}
              style={theme.Input}
              onChangeText={this.handleSchoolChange}
              placeholderTextColor={"grey"}
              underlineColorAndroid = "transparent"
              onSubmitEditing={this.handleLogin}
            />
          </View>
        </View>
        <View style={theme.rowContainer}>
          <Text style={theme.inputTitle}>interests</Text>
          <View style={theme.inputContainer}>
            <TextInput value={this.state.Profile.interests}
              ref={(input) => { this.passwordInput = input;}}
              placeholder={this.state.Profile.name}
              style={theme.Input}
              onChangeText={this.handleInterestsChange}
              placeholderTextColor={"grey"}
              underlineColorAndroid = "transparent"
              onSubmitEditing={this.handleLogin}
            />
          </View>
        </View>
      </View>
    );
  }

  render() {
    const {theme} = this.state;

    return (
      <View style={theme.container}>
        <NavigationEvents onWillFocus={() => this.getData()} />
        {this.editName()}
        <View style={theme.container3}>
          <TouchableOpacity onPress={() => this.handleSubmit()}>
            <View style={theme.LoginContainer}>
              <Text style={theme.LoginButton}>change Profile</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
