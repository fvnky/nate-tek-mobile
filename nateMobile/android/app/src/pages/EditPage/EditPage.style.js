import { StyleSheet } from 'react-native';

export const lightMode = StyleSheet.create({
    container: {
      backgroundColor: "#FFFFFF",
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
  
    container2: {
      flex: 4,
      backgroundColor: "#FFFFFF",
      flexDirection: 'column',
      justifyContent: 'center',
    },
  
    setting: {
      flex: 1,
      backgroundColor: 'white',
      flexDirection: "row",
      justifyContent: 'space-between',
      width: "100%",
    },
  
    list: {
      justifyContent: 'center',
    },
  
    bar : {
      width: "100%",
      alignItems: 'center',
      justifyContent: 'center',
      elevation : 3,
      backgroundColor : "white",
    },
  
    SubjectTitle: {
      color: "black",
      fontSize: 25,
      fontFamily: "Source Sans Pro",
      marginLeft: "5%",
      marginTop: "5%",
    },
  
    rowContainer: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      width: '90%',
    },
  
    inputTitle: {
      flex: 2,
      color: 'grey',
      paddingTop: 8,
    },
  
    inputContainer: {
      flex: 5,
      backgroundColor: "#FFFFFF",
      height: 40,
      borderBottomWidth: 1,
      borderBottomColor: 'grey',
      flexDirection: "row",
      justifyContent: "flex-start",
    },
  
    Input: {
      color: "grey",
      textAlign: 'left',
    },
  
    text: {
      color: "black",
      fontSize: 20,
      fontFamily: "Source Sans Pro",
    },
    container3 : {
      flex: 1,
    },
    LoginContainer: {
      flexDirection: "row",
      justifyContent: "center",
      marginLeft: "15%",
      width: "70%",
      height: 45,
      //marginTop: 10,
      backgroundColor: "#4A4A4A",
      borderRadius: 75,
    },
    LoginButton: {
      fontSize: 16,
      color: "#FAFAFA",
      //color: "black",
      paddingVertical: 10,
      //borderRadius: 50,
    },
    loginInput: {
      width: "80%",
      color: "#FAFAFA",
      textAlign: 'left',
      //boxSizing: "border-box",
    },
  
  });
  
  export const darkMode = StyleSheet.create({
    container: {
      backgroundColor: "#676D78",
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
  
    container2: {
      flex: 4,
      backgroundColor: "#676D78",
      flexDirection: 'column',
      justifyContent: 'center',
    },
  
    setting: {
      flex: 1,
      backgroundColor: 'white',
      flexDirection: "row",
      justifyContent: 'space-between',
      width: "100%",
    },
  
    list: {
      justifyContent: 'center',
    },
  
    bar : {
      width: "100%",
      alignItems: 'center',
      justifyContent: 'center',
      elevation : 3,
      backgroundColor : "white",
    },
  
    SubjectTitle: {
      color: "black",
      fontSize: 25,
      fontFamily: "Source Sans Pro",
      marginLeft: "5%",
      marginTop: "5%",
    },
  
    rowContainer: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      width: '90%',
    },
  
    inputTitle: {
      flex: 2,
      color: '#FFFFFF',
      paddingTop: 8,
    },
  
    inputContainer: {
      flex: 5,
      backgroundColor: "#676D78",
      height: 40,
      borderBottomWidth: 1,
      borderBottomColor: '#FFFFFF',
      flexDirection: "row",
      justifyContent: "flex-start",
    },
  
    Input: {
      color: "#FAFAFA",
      textAlign: 'left',
    },
  
    text: {
      color: "black",
      fontSize: 20,
      fontFamily: "Source Sans Pro",
    },
    container3 : {
      flex: 1,
    },
    LoginContainer: {
      flexDirection: "row",
      justifyContent: "center",
      marginLeft: "15%",
      width: "70%",
      height: 45,
      //marginTop: 10,
      backgroundColor: "#4A4A4A",
      borderRadius: 75,
    },
    LoginButton: {
      fontSize: 16,
      color: "#FAFAFA",
      //color: "black",
      paddingVertical: 10,
      //borderRadius: 50,
    },
    loginInput: {
      width: "80%",
      color: "#FAFAFA",
      textAlign: 'left',
      //boxSizing: "border-box",
    },
  });
  