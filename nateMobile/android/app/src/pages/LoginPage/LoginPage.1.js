import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { NavigationEvents } from 'react-navigation'

// darkMode needed do not remove
import {lightMode, darkMode} from './LoginPage.style';

export default class LoginPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			theme: lightMode,
		}
	}

	async getData() {
		try {
			await this.props.screenProps.user.loadUser()
			this.props.navigation.navigate('Home')
		} catch (e) {
			console.warn('Could not login automatically', e)
		}
	}

	handleLogin = async () => {
		this.props.navigation.navigate('Login2')
	}

	render() {
		const {theme} = this.state;
		return (
			<View style={theme.myBackground}>
				<NavigationEvents onWillFocus={() => this.getData()} />
				<View style={theme.container}>
					<View style={theme.IconContainer1}>
						<Image
							style={theme.NateIcon}
							source={require('../../assets/placeholderIcon.png')}
						/>
					</View>
					<View style={theme.TitleContainer1}>
						<Text style={theme.nateTitle}>NATE</Text>
					</View>
					<View style={theme.container3}>
						<TouchableOpacity onPress={this.handleLogin}>
							<View style={theme.LoginContainer}>
								<Text style={theme.LoginButton}>Login</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		)
	}
}
