import React from 'react'
import {
	View,
	Text,
	TextInput,
	Image,
	TouchableOpacity,
} from 'react-native'
import { NavigationEvents } from 'react-navigation'

// darkMode needed do not remove
import {lightMode, darkMode} from './LoginPage.style';

export default class LoginPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			login: '',
			loginError: false,
			theme: lightMode,
		}
	}

	getData() {}

	handleLogin = async () => {
		const { login } = this.state

		try {
			if (login === '') throw new Error('NOP')

			await this.props.screenProps.user.sendValidation(login)

			this.props.navigation.navigate('Login3', { Login: this.state.login })
		} catch (e) {
			this.setState({ loginError: true })
		}
	}

	handleLoginInput = (input) => {
		const { loginError } = this.state

		if (loginError) {
			this.setState({ loginError: false })
		}
		this.setState({ login: input })
	}

	handlePassWordInput = (Input) => {
		const { loginError } = this.state

		if (loginError) {
			this.setState({ loginError: false })
		}
		this.setState({ password: Input })
	}

	errorMessage() {
		const { loginError, theme } = this.state

		if (loginError) {
			return (
				<View style={theme.ErrorTextContainer}>
					<Text style={theme.ErrorText}>
						Login or Password is incorrect
					</Text>
				</View>
			)
		}
	}

	render() {
		const { theme } = this.state

		return (
			<View style={theme.myBackground}>
				<NavigationEvents onWillFocus={() => this.getData()} />
				<View style={theme.container}>
					<View style={theme.IconContainer}>
						<View style={theme.rowContainer}>
							<Image 
								style={theme.NateIcon}
								source={require('../../assets/placeholderIcon.png')}
							/>
						</View>
						<View style={theme.rowContainer}>
							<Text style={theme.nateTitle}>Sign in</Text>
						</View>
					</View>
					{this.errorMessage()}
					<View style={theme.TitleContainer}>
						<View style={theme.rowContainer}>
							<View style={theme.inputContainer}>
								<TextInput
									value={this.props.login}
									placeholder='Enter your email address'
									style={theme.loginInput}
									onChangeText={this.handleLoginInput}
									placeholderTextColor={'grey'}
									underlineColorAndroid='transparent'
									onSubmitEditing={this.handleLogin}
									blurOnSubmit={false}
								/>
							</View>
						</View>
					</View>

					<View style={theme.container3}>
						<TouchableOpacity onPress={this.handleLogin}>
							<View style={theme.LoginContainer}>
								<Text style={theme.LoginButton}>Login</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		)
	}
}
