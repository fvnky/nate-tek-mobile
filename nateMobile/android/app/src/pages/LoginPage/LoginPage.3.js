import React from 'react'
import {
	View,
	Text,
	TextInput,
	Image,
	TouchableOpacity,
} from 'react-native'
import { NavigationEvents } from 'react-navigation'

// darkMode needed do not remove
import {lightMode, darkMode} from './LoginPage.style';

export default class LoginPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			login: '',
			password: '',
			loginError: false,
			theme: lightMode,
		}
	}

	getData() {
		const { navigation } = this.props;

		const loginText =  navigation.getParam('Login', '');
		this.setState({login: loginText});
	}

	handleLogin = async () => {
		const { login, password } = this.state

		try {
			if (login === '' || password === '') throw new Error('NOP')

			await this.props.screenProps.user.logIn(login, password)

			this.props.navigation.navigate('Home')
		} catch (e) {
			this.setState({ loginError: true })
		}
	}

	handleLoginInput = (input) => {
		const { loginError } = this.state

		if (loginError) {
			this.setState({ loginError: false })
		}
		this.setState({ login: input })
	}

	handlePassWordInput = (Input) => {
		const { loginError } = this.state

		if (loginError) {
			this.setState({ loginError: false })
		}
		this.setState({ password: Input })
	}

	errorMessage() {
		const { loginError, theme } = this.state

		if (loginError) {
			return (
				<View style={theme.ErrorTextContainer}>
					<Text style={themethemeValue}>
						Login or Password is incorrect
					</Text>
				</View>
			)
		}
	}

	render() {
		const { theme } = this.state;

		return (
			<View style={theme.myBackground}>
				<NavigationEvents onWillFocus={() => this.getData()} />
				<View style={theme.container}>
					<View style={theme.IconContainer}>
						<View style={theme.rowContainer}>
							<Image style={theme.NateIcon}
								source={require('../../assets/placeholderIcon.png')}
							/>
						</View>
						<View style={theme.rowContainer}>
							<Text style={theme.nateTitle}>Sign in</Text>
						</View>
					</View>

					<View style={theme.TitleContainer}>
						<View style={theme.subContainer}>
							<Image style={theme.MailIcon}
								source={require('../../assets/mailPlaceholder.png')}
							/>
						</View>
						<View style={theme.subContainer}>
							<Text style={theme.text1}>We sent you an</Text>
						</View>
						<View style={theme.subContainer}>
							<Text style={theme.text1}>email!</Text>
						</View>
					</View>

					<View style={theme.inputsContainer}>
						{this.errorMessage()}
						<View style={theme.rowContainer}>
							<View style={theme.inputContainer}>
								<TextInput
									value={this.state.login}
									placeholder='Enter your email address'
									style={theme.loginInput}
									onChangeText={this.handleLoginInput}
									underlineColorAndroid='transparent'
									onSubmitEditing={() => {
										this.passwordInput.focus()
									}}
									placeholderTextColor={'grey'}
									blurOnSubmit={false}
								/>
							</View>
						</View>
						<View style={theme.rowContainer}>
							<View style={theme.inputContainer}>
								<TextInput
									value={this.state.password}
									ref={(input) => {
										this.passwordInput = input
									}}
									placeholder='Enter the code received by mail'
									style={theme.passwordInput}
									onChangeText={this.handlePassWordInput}
									placeholderTextColor={'grey'}
									underlineColorAndroid='transparent'
									onSubmitEditing={this.handleLogin}
									blurOnSubmit={false}
								/>
							</View>
						</View>
					</View>

					<View style={theme.container3}>
						<TouchableOpacity onPress={this.handleLogin}>
							<View style={theme.LoginContainer}>
								<Text style={theme.LoginButton}>Login</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		)
	}
}
