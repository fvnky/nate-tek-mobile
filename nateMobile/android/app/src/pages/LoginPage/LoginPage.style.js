import { StyleSheet } from 'react-native'

export const lightMode = StyleSheet.create({
	myBackground: {
		flex: 1,
		backgroundColor: '#FAFAFA',
		justifyContent: 'flex-end',
	},

	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		//marginBottom: "15%",
	},

	IconContainer1: {
		flex: 3,
		flexDirection: 'row',
		justifyContent: 'space-around',
	},

	IconContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	NateIcon: {
		marginTop: '25%',
		height: 100,
		width: 100,
	},

	nateTitle: {
		//marginTop: "40%",
		fontSize: 25,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: 'grey',
	},

	MailIcon: {
		height: 100,
		width: 100,
	},

  	TitleContainer1: {
		flex: 3,
		flexDirection: 'row',
		justifyContent: 'center',
	},

	TitleContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	rowContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	colContainer: {
		flexDirection: 'column',
		justifyContent: 'center',
	},

	subContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	text1: {
		//marginTop: "40%",
		fontSize: 20,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: 'grey',
	},

	text2: {
		//marginTop: "40%",
		fontSize: 16,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: 'grey',
	},

	inputsContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	inputContainer: {
		backgroundColor: '#FAFAFA',
		// elevation: 2,
		marginTop: 7,
		width: '90%',
		height: 40,
		// borderWidth: 1,
		// borderRadius: 12,
		borderBottomWidth: 1,
		borderBottomColor: '#242424',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},

	login: {
		width: '80%',
		color: 'grey',
	},

	container3: {
		flex: 1,
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		//marginTop: 10,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	loginInput: {
		width: '80%',
		color: 'black',
		textAlign: 'left',
		//boxSizing: "border-box",
	},

	passwordInput: {
		width: '80%',
		color: 'black',
		textAlign: 'left',
		//boxSizing: "border-box",
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		//color: "black",
		paddingVertical: 10,
		//borderRadius: 50,
	},

	ErrorTextContainer: {
		justifyContent: 'center',
		alignItems: 'center',
	},

	ErrorText: {
		fontSize: 16,
		color: 'red',
	},

	buttonContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	button: {},
})

export const darkMode = StyleSheet.create({
	myBackground: {
		flex: 1,
		backgroundColor: '#676D78',
		justifyContent: 'flex-end',
	},

	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		//marginBottom: "15%",
	},

	IconContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	NateIcon: {
		marginTop: '25%',
		height: 100,
		width: 100,
	},

	nateTitle: {
		//marginTop: "40%",
		fontSize: 25,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: '#FFFFFF',
	},

	MailIcon: {
		height: 100,
		width: 100,
	},

	TitleContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	rowContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	colContainer: {
		flexDirection: 'column',
		justifyContent: 'center',
	},

	subContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	text1: {
		//marginTop: "40%",
		fontSize: 20,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: '#FFFFFF',
	},

	text2: {
		//marginTop: "40%",
		fontSize: 16,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: '#FFFFFF',
	},

	inputsContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	inputContainer: {
		backgroundColor: '#676D78',
		// elevation: 2,
		marginTop: 7,
		width: '90%',
		height: 40,
		// borderWidth: 1,
		// borderRadius: 12,
		borderBottomWidth: 1,
		borderBottomColor: '#FFFFFF',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},

	login: {
		width: '80%',
		color: 'grey',
	},

	container3: {
		flex: 1,
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		//marginTop: 10,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	loginInput: {
		width: '80%',
		color: '#FAFAFA',
		textAlign: 'left',
		//boxSizing: "border-box",
	},

	passwordInput: {
		width: '80%',
		color: '#FAFAFA',
		textAlign: 'left',
		//boxSizing: "border-box",
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		//color: "black",
		paddingVertical: 10,
		//borderRadius: 50,
	},

	ErrorTextContainer: {
		justifyContent: 'center',
		alignItems: 'center',
	},

	ErrorText: {
		fontSize: 16,
		color: 'red',
	},

	buttonContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	button: {},
})
