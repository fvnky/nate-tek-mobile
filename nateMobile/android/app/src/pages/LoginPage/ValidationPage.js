import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { NavigationEvents } from 'react-navigation'

export default class LoginPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			themeValue: false,
		}
	}

	// getData() not in use for durration of test

	// async getData() {
	// 	const { screenProps } = this.props;

	// 	await screenProps.getTheme();
	// 	if (screenProps.theme !== null) {
	// 		this.setState({themeValue: screenProps.theme.themeValue});
	// 	}
	// }

	getDataTest() {}

	async handleTest() {
		await this.props.screenProps.fetch()

		const user = await this.props.screenProps.user.getUser()
		const subjects = await this.props.screenProps.subjects.findAll()
		const folders = await this.props.screenProps.folders.findAll()
		const courses = await this.props.screenProps.courses.findAll()
		const dictionary = await this.props.screenProps.dictionary.findAll()
		const drafts = await this.props.screenProps.drafts.findAll()
		const quizs = await this.props.screenProps.quizs.findAll()
		const todos = await this.props.screenProps.todos.findAll()

		console.warn(`User\n`, JSON.stringify(user, null, 2))
		console.warn(`Subjects ${subjects.length}\n`, JSON.stringify(subjects, null, 2))
		console.warn(`Folders ${folders.length}\n`, JSON.stringify(folders, null, 2))
		console.warn(`Courses ${courses.length}\n`, JSON.stringify(courses, null, 2))
		console.warn(`Dictionary ${dictionary.length}\n`, JSON.stringify(dictionary, null, 2))
		console.warn(`Drafts ${drafts.length}\n`, JSON.stringify(drafts, null, 2))
		console.warn(`Quizs ${quizs.length}\n`, JSON.stringify(quizs, null, 2))
		console.warn(`Todos ${todos.length}\n`, JSON.stringify(todos, null, 2))
	}

	async handleLogOut() {
		await this.props.screenProps.user.logOut()
		this.props.navigation.navigate('Login1')
	}

	render() {
		return (
			<View
				style={this.state.themeValue ? darkMode.container : lightMode.container}
			>
				<NavigationEvents onWillFocus={() => this.getDataTest()} />
				<View
					style={
						this.state.themeValue
							? darkMode.TitleContainer
							: lightMode.TitleContainer
					}
				>
					<Text
						style={
							this.state.themeValue ? darkMode.nateTitle : lightMode.nateTitle
						}
					>
						SUCCESS
					</Text>
				</View>
				<TouchableOpacity onPress={() => this.handleTest()}>
					<View
						style={
							this.state.themeValue
								? darkMode.LoginContainer
								: lightMode.LoginContainer
						}
					>
						<Text
							style={
								this.state.themeValue
									? darkMode.LoginButton
									: lightMode.LoginButton
							}
						>
							Test
						</Text>
					</View>
				</TouchableOpacity>
				<TouchableOpacity onPress={() => this.handleLogOut()}>
					<View
						style={
							this.state.themeValue
								? darkMode.LoginContainer
								: lightMode.LoginContainer
						}
					>
						<Text
							style={
								this.state.themeValue
									? darkMode.LoginButton
									: lightMode.LoginButton
							}
						>
							LogOut
						</Text>
					</View>
				</TouchableOpacity>
			</View>
		)
	}
}

const lightMode = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FAFAFA',
		flexDirection: 'column',
		justifyContent: 'space-evenly',
	},

	TitleContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	nateTitle: {
		fontSize: 70,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: 'grey',
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		paddingVertical: 10,
	},
})

const darkMode = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FAFAFA',
		flexDirection: 'column',
		justifyContent: 'space-evenly',
	},

	TitleContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	nateTitle: {
		fontSize: 70,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: '#FFFFFF',
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		paddingVertical: 10,
	},
})
