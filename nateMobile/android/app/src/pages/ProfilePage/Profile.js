import React from 'react'
import {
  Image,
  ScrollView,
  Text,
  View,
} from 'react-native'
import { NavigationEvents } from 'react-navigation';

// darkMode needed do not remove
import { lightMode, darkMode } from './Profile.style';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';

export default class ProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: lightMode,
      Profile: {
        name: 'firstname',
        surName: 'surname',
        school: 'schoolname',
        job: 'job',
        contry: 'France',
        curriculum: "curiculum",
        academicYear: '',
        emails: 'firstname.surname@email.com',
        interests: '',
      }
    };
  }

  // fetch user data here and save it in state.Profile
  async getData() {

  }

  renderHeader() {
    const { Profile, theme } = this.state;

    return (
      <View style={theme.headerContainer}>
        <View
          style={lightMode.headerBackground}
          blurRadius={10}
        >
          <View style={theme.headerColumn}>
            <Image
              style={lightMode.userImage}
              source={require('./mocks/profil-pic.jpg')}
            />
            <Text style={theme.userNameText}>{`${Profile.name}, ${Profile.surName}`}</Text>
            <View style={theme.userAddressRow}>
              <View style={theme.userCityRow}>
                <Text style={theme.userCityText}>{`${Profile.school}, ${Profile.job}`}</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }

  renderSchool() {
    const { Profile, theme } = this.state;

    return (
      <View style={theme.emailContainer}>
        <View style={theme.iconRow}>
          <FontAwesome5 style={lightMode.Icon} name="school" size={30} color={'black'} ></FontAwesome5>
        </View>
        <View style={theme.emailRow}>
          <View style={theme.emailColumn}>
            <Text style={theme.emailNameText}>{Profile.school}</Text>
          </View>
          <View style={theme.emailNameColumn}>
            <Text style={theme.emailNameText}>{Profile.curriculum}</Text>
          </View>
        </View>
      </View>
    );  
  }

  renderContry() {
    const { Profile, theme } = this.state;

    return (
      <View style={theme.emailContainer}>
        <View style={theme.iconRow}>
          <Entypo style={lightMode.Icon} name="map" size={30} color={'black'} ></Entypo>
        </View>
        <View style={theme.emailRow}>
          <View style={theme.emailNameColumn}>
            <Text style={theme.emailNameText}>contry</Text>
          </View>
          <View style={theme.emailColumn}>
            <Text style={theme.emailNameText}>{Profile.contry}</Text>
          </View>
        </View>
      </View>
    );  
  }

  renderEmail() {
    const { Profile, theme } = this.state;

    return (
      <View style={theme.emailContainer}>
        <View style={theme.iconRow}>
          <AntDesign style={lightMode.Icon} name="mail" size={35} color={'black'} ></AntDesign>
        </View>
        <View style={theme.emailRow}>
          <View style={theme.emailNameColumn}>
            <Text style={theme.emailNameText}>{Profile.name}</Text>
          </View>
          <View style={theme.emailColumn}>
            <Text style={theme.emailNameText}>{Profile.emails}</Text>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const { theme } = this.state;

    return (
      <ScrollView style={theme.scroll}>
        <NavigationEvents onWillFocus={() => this.getData()} />
        <View style={theme.container}>
          <View containerStyle={theme.cardContainer}>
            {this.renderHeader()}
            {this.renderSchool()}
            {this.renderContry()}
            {this.renderEmail()}
          </View>
        </View>
      </ScrollView>
    )
  }
}
