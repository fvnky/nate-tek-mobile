import { StyleSheet, Platform } from 'react-native'

export const lightMode = StyleSheet.create({
    cardContainer: {
      borderWidth: 0,
      flex: 1,
      margin: 0,
      padding: 0,
    },
    container: {
      flex: 1,
    },
    emailContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      marginBottom: 25,
      paddingTop: 30,
    },
    headerBackground: {
      paddingBottom: 20,
      paddingTop: 35,
      backgroundColor: "grey"
    },
    headerContainer: {},
    headerColumn: {
      backgroundColor: 'transparent',
      ...Platform.select({
        ios: {
          alignItems: 'center',
          elevation: 1,
          marginTop: -1,
        },
        android: {
          alignItems: 'center',
        },
      }),
    },
    placeIcon: {
      //color: 'white',
      //fontSize: 26,
      width: 20,
      height: 20,
      marginRight: 5
    },
    scroll: {
      backgroundColor: '#FFF',
    },
    telContainer: {
      backgroundColor: '#FFF',
      flex: 1,
      paddingTop: 30,
    },
    userAddressRow: {
      alignItems: 'center',
      flexDirection: 'row',
    },
    userCityRow: {
      backgroundColor: 'transparent',
    },
    userCityText: {
      color: '#FFFFFF',
      fontSize: 15,
      fontWeight: '600',
      textAlign: 'center',
    },
    userImage: {
      borderColor: 'grey',
      borderRadius: 85,
      borderWidth: 3,
      height: 170,
      marginBottom: 15,
      width: 170,
    },
    userNameText: {
      color: '#FFFFFF',
      fontSize: 22,
      fontWeight: 'bold',
      paddingBottom: 8,
      textAlign: 'center',
    },
    iconRow: {
      flex: 2,
      justifyContent: 'center',
    },
    emailIcon: {
      height: 25,
      width: 25,
      marginLeft: 30
    },
    emailColumn: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      marginBottom: 5,
    },
    emailNameColumn: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    emailNameText: {
      color: 'gray',
      fontSize: 14,
      fontWeight: '200',
    },
    emailRow: {
      flex: 6,
      flexDirection: 'column',
      justifyContent: 'center',
    },
    emailText: {
      fontSize: 16,
    },
    Icon: {
      marginVertical: 10,
      marginLeft: 30,
    }, 
  });
  
  export const darkMode = StyleSheet.create({
    cardContainer: {
      borderWidth: 0,
      flex: 1,
      margin: 0,
      padding: 0,
    },
    container: {
      flex: 1,
    },
    emailContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      marginBottom: 25,
      paddingTop: 30,
    },
    headerBackground: {
      paddingBottom: 20,
      paddingTop: 35,
      backgroundColor: "grey"
    },
    headerContainer: {},
    headerColumn: {
      backgroundColor: 'transparent',
      ...Platform.select({
        ios: {
          alignItems: 'center',
          elevation: 1,
          marginTop: -1,
        },
        android: {
          alignItems: 'center',
        },
      }),
    },
    placeIcon: {
      //color: 'white',
      //fontSize: 26,
      width: 20,
      height: 20,
      marginRight: 5
    },
    scroll: {
      backgroundColor: '#676D78',
    },
    telContainer: {
      backgroundColor: '#FFF',
      flex: 1,
      paddingTop: 30,
    },
    userAddressRow: {
      alignItems: 'center',
      flexDirection: 'row',
    },
    userCityRow: {
      backgroundColor: 'transparent',
    },
    userCityText: {
      color: '#FFFFFF',
      fontSize: 15,
      fontWeight: '600',
      textAlign: 'center',
    },
    userImage: {
      borderColor: 'grey',
      borderRadius: 85,
      borderWidth: 3,
      height: 170,
      marginBottom: 15,
      width: 170,
    },
    userNameText: {
      color: '#FFFFFF',
      fontSize: 22,
      fontWeight: 'bold',
      paddingBottom: 8,
      textAlign: 'center',
    },
    iconRow: {
      flex: 2,
      justifyContent: 'center',
    },
    emailIcon: {
      height: 25,
      width: 25,
      marginLeft: 30
    },
    emailColumn: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      marginBottom: 5,
    },
    emailNameColumn: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    emailNameText: {
      color: '#FFFFFF',
      fontSize: 14,
      fontWeight: '200',
    },
    emailRow: {
      flex: 6,
      flexDirection: 'column',
      justifyContent: 'center',
    },
    emailText: {
      fontSize: 16,
    },
    Icon: {
      marginVertical: 10,
      marginLeft: 30,
    }, 
  });
  