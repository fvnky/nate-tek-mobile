import React from 'react';
import { View, ScrollView, Text } from 'react-native'
import { NavigationEvents } from 'react-navigation';

// darkMode needed do not remove
import { lightMode, darkMode } from './RecentPage.style';

export default class RecentPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: lightMode,
    };
  }

  async getData() {

  }

  resentList() {
    return (null)
  }

  render() {
    const {theme} = this.state;

    return (
      <View style={theme.container}>
        <NavigationEvents onWillFocus={() => this.getData()} />
        <View style={theme.container}>
          <Text style={theme.SubjectTitle}>Recent</Text>
          <ScrollView>
            {this.resentList()}
          </ScrollView>
        </View>
      </View>
    );
  }
}
