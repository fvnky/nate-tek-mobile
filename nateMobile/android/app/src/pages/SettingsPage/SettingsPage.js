import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Switch,
  Picker
} from 'react-native'
import { NavigationEvents } from 'react-navigation';

// darkMode needed do not remove
import { lightMode, darkMode } from './SettingsPage.style';
import TopBar from "../../component/TopBar.js";

export default class SettingsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: lightMode,
      color: 'black',
      themeValue: false,
      exploreValue: false,
      pushNotification: false,
      newNotification: false,
      laguage: "Francais",
      font: "Modern",
    };
  }

  async getData() {}

  toggleTheme = (value) => {
    // needs new logic

    console.warn('new logic needed')
  }

  toggleExplore = (value) => {
    // needs new logic

    console.warn('new logic needed')
  }

  togglePushNotification = (value) => {
    // needs new logic

    console.warn('new logic needed')
  }

  toggleNewNotification = (value) => {
    // needs new logic

    console.warn('new logic needed')
  }


  // this needs spliting
  render() {
    const { theme, color, themeValue } = this.state;

    return (
      <View style={theme.container}>
        <NavigationEvents onWillFocus={() => this.getData()} />
        <TopBar theme={themeValue} navigation={this.props.navigation} title="Settings" />
        <View style={theme.container}>
          <ScrollView>
            <View style={{ height: 40 }} />
            <Text style={theme.text}>Language</Text>
            <Picker
              selectedValue={this.state.language}
              style={theme.picker}
              onValueChange={(itemValue) =>
                this.setState({ language: itemValue })
              }>
              <Picker.Item label="Francais" value="Francais" color={color}/>
              <Picker.Item label="English" value="English" color={color}/>
              <Picker.Item label="Deutsch" value="Deutsch" color={color}/>
            </Picker>
            <TouchableOpacity style={theme.setting} onPress={() => this.toggleTheme(!themeValue)} >
              <View style={theme.colContainer}>
                <View style={theme.rowContainer}>
                  <Text style={theme.text}>Interface Theme</Text>
                </View>
                <View style={theme.rowContainer}>
                  <Text style={theme.subText}>{themeValue ? "dark" : "light"}</Text>
                </View>
              </View>
              <Switch
                style={theme.switch}
                onValueChange={this.toggleTheme}
                value={themeValue} />
            </TouchableOpacity>
            <TouchableOpacity style={theme.setting} onPress={() => this.toggleExplore(!this.state.exploreValue)} >
              <View style={theme.colContainer}>
                <View style={theme.rowContainer}>
                  <Text style={theme.text}>Explore Search</Text>
                </View>
                <View style={theme.rowContainer}>
                  <Text style={theme.subText}>{this.state.exploreValue ? "enabled" : "disabled"}</Text>
                </View>
              </View>
              <Switch
                style={theme.switch}
                onValueChange={this.toggleExplore}
                value={this.state.exploreValue} />
            </TouchableOpacity>
            <Text style={theme.text}>Main font</Text>
            <Picker
              selectedValue={this.state.font}
              style={theme.picker}
              onValueChange={(itemValue) =>
                this.setState({ font: itemValue })
              }>
              <Picker.Item label="Modern" value="Modern" color={color}/>
              <Picker.Item label="Arial" value="Arial" color={color}/>
              <Picker.Item label="calibri" value="calibri" color={color}/>
            </Picker>
            <TouchableOpacity style={theme.setting} onPress={() => this.togglePushNotification(!this.state.pushNotification)} >
              <View style={theme.colContainer}>
                <View style={theme.rowContainer}>
                  <Text style={theme.text}>Push Notification</Text>
                </View>
                <View style={theme.rowContainer}>
                  <Text style={theme.subText}>{this.state.pushNotification ? "enabled" : "disabled"}</Text>
                </View>
              </View>
              <Switch
                style={theme.switch}
                onValueChange={this.togglePushNotification}
                value={this.state.pushNotification} />
            </TouchableOpacity>
            <TouchableOpacity style={theme.setting} onPress={() => this.toggleNewNotification(!this.state.newNotification)} >
              <View style={theme.colContainer}>
                <View style={theme.rowContainer}>
                  <Text style={theme.text}>new message notification</Text>
                </View>
                <View style={theme.rowContainer}>
                  <Text style={theme.subText}>{this.state.newNotification ? "enabled" : "disabled"}</Text>
                </View>
              </View>
              <Switch
                style={theme.switch}
                onValueChange={this.toggleNewNotification}
                value={this.state.newNotification} />
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
    );
  }
}
