import { StyleSheet } from 'react-native';

export const lightMode = StyleSheet.create({
    container: {
      backgroundColor: "#FAFAFA",
      flex: 1,
    },
  
    setting: {
      flex: 1,
      backgroundColor: '#FAFAFA',
      flexDirection: "row",
      justifyContent: 'space-between',
      width: "100%",
      paddingVertical: 10,
    },
  
    colContainer: {
      flexDirection: "column",
      // justifyContent: 'center',
    },
  
    rowContainer: {
      flexDirection: "row",
      // justifyContent: 'center',
    },
  
    list: {
      justifyContent: 'center',
    },
  
    switch: {
  
    },
  
    picker: {
      marginLeft: 10,
    },
  
    pickerItem: {
      color: "black",
    },
  
    bar: {
      width: "100%",
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 3,
      backgroundColor: "#FAFAFA",
    },
  
    Title: {
      color: "black",
      fontSize: 25,
      // fontFamily: "Source Sans Pro",
      marginLeft: "5%",
      marginTop: "5%",
      paddingBottom: 20,
    },
  
    text: {
      color: "black",
      fontSize: 20,
      // fontFamily: "Source Sans Pro",
      marginLeft: 10
    },
  
    subText: {
      color: "grey",
      fontSize: 15,
      // fontFamily: "Source Sans Pro",
      marginLeft: 10
    },
  
  });
  
  export const darkMode = StyleSheet.create({
    container: {
      backgroundColor: "#676D78",
      flex: 1,
    },
  
    setting: {
      flex: 1,
      backgroundColor: '#676D78',
      flexDirection: "row",
      justifyContent: 'space-between',
      width: "100%",
      paddingVertical: 10,
    },
  
    colContainer: {
      flexDirection: "column",
      // justifyContent: 'center',
    },
  
    rowContainer: {
      flexDirection: "row",
      // justifyContent: 'center',
    },
  
    list: {
      justifyContent: 'center',
    },
  
    switch: {
  
    },
  
    picker: {
      marginLeft: 10,
      color: "green",
    },
  
    pickerItem: {
      color: "#FAFAFA",
    },
  
    bar: {
      width: "100%",
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 3,
      backgroundColor: "#676D78",
    },
  
    Title: {
      color: "#FAFAFA",
      fontSize: 25,
      // fontFamily: "Source Sans Pro",
      marginLeft: "5%",
      marginTop: "5%",
      paddingBottom: 20,
    },
  
    text: {
      color: "#FFFFFF",
      fontSize: 20,
      // fontFamily: "Source Sans Pro",
      marginLeft: 10
    },
  
    subText: {
      color: "#DADADA",
      fontSize: 15,
      // fontFamily: "Source Sans Pro",
      marginLeft: 10
    },
  
  });
  