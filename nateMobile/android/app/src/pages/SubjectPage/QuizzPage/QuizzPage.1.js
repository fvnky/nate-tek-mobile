import React from 'react'
import {
	View,
	Text,
	StyleSheet,
	Image,
	TouchableOpacity
} from 'react-native'
import { NavigationEvents } from 'react-navigation';

export default class LoginPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
            themeValue: false,
            theme: "darkMode",
			Quizzes: {
				QUIZZES: [
					{name: "test"}
				]
			},
			id: 0,
		}
	}
	
	// componentWillMount = () => {
	// 	this.props.screenProps.get("QUIZZES")
	// 	.then((doc) => {
	// 	  // console.warn(doc);
	// 	  this.setState({Quizzes: doc});
	// 	})
	// 	.catch((err) => {
	// 	  console.warn(err)
	// 	});
	// 	this.fetchTheme();
	// }

	// fetchTheme() {
	// 	this.props.screenProps.get("theme").then((doc) => {
	// 		this.setState({
	// 		  themeValue: doc.themeValue,
	// 		  theme: doc.theme,
	// 		});
	// 	  }).catch((err) => {
	// 		console.warn('fail');
	// 	})
	//   }

	// fetchQuizzNum() {
	// 	this.props.screenProps.get("quizz")
	// 	.then((doc) => {
	// 	  //console.warn(doc);
	// 	  this.setState({id : doc.num - 1})
	// 	})
	// 	.catch((err) => {
	// 	  console.warn(err)
	// 	});
	// }

	async getData() {
		const { screenProps } = this.props;
  
		await screenProps.getQuizz();
		await screenProps.getTheme();
		if (screenProps.theme !== null) {
			this.setState({
				Quizzes: screenProps.quizz,
				themeValue: screenProps.theme.themeValue,
				id: screenProps.selectedQuizz - 1,
			});
		}
	}

	handleStart = async () => {
		this.props.navigation.navigate('QuizzPage2', {id: this.state.id})
	}

	bar() {
		return (
		  <View style={this.state.themeValue ? darkMode.bar : lightMode.bar}>
			<View style={this.state.themeValue ? darkMode.container : lightMode.container}>
			  <Text style={this.state.themeValue ? darkMode.title : lightMode.title}>Quizz</Text>
			</View>
  	  	  </View>
		);
	}

	render() {
		// this.fetchQuizzNum();
		// this.fetchTheme();
		// console.warn("quizz");
		return (
			<View style={this.state.themeValue ? darkMode.myBackground : lightMode.myBackground}>
		        <NavigationEvents onWillFocus={() => this.getData()} />
				<View style={this.state.themeValue ? darkMode.container : lightMode.container}>

					<View style={this.state.themeValue ? darkMode.IconContainer : lightMode.IconContainer}>
						<Image
							style={this.state.themeValue ? darkMode.NateIcon : lightMode.NateIcon}
							source={require('../../../assets/placeholderIcon.png')}
						/>
					</View>

					<View style={this.state.themeValue ? darkMode.QuestionContainer : lightMode.QuestionContainer}>
						<View style={this.state.themeValue ? darkMode.questionContainer : lightMode.questionContainer}>
							<Text style={this.state.themeValue ? darkMode.questionTitle : lightMode.questionTitle}>{this.state.Quizzes.QUIZZES[this.state.id].name}</Text>
						</View>
					</View>

					<View style={{flex: 1}}>
						<TouchableOpacity onPress={this.handleStart}>
							<View style={this.state.themeValue ? darkMode.LoginContainer : lightMode.LoginContainer}>
								<Text style={this.state.themeValue ? darkMode.LoginButton : lightMode.LoginButton}>Start</Text>
							</View>
						</TouchableOpacity>
					</View>

				</View>
			</View>
		)
	}
}

const lightMode = StyleSheet.create({
	myBackground: {
		flex: 1,
		backgroundColor: '#FAFAFA',
		justifyContent: 'flex-end',
	},

	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-around',
		//marginBottom: "15%",
	},

	bar : {
		height: "9%",
		width: "100%",
		flexDirection: "column",
		justifyContent: 'space-between',
		elevation : 3,
		backgroundColor : "white",
	},

	barContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		//marginBottom: "15%",
	},

	title: {
		color: "black",
		fontSize: 25,
		fontFamily: "Source Sans Pro",
		paddingVertical: 10,
	},

	IconContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
		paddingVertical: '10%',
	},

	QuestionContainer: {
		flex: 4,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	questionContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	questionTitle: {
		//marginTop: "40%",
		fontSize: 25,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: 'grey',
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		//marginTop: 10,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		//color: "black",
		paddingVertical: 10,
		//borderRadius: 50,
	},

})

const darkMode = StyleSheet.create({
	myBackground: {
		flex: 1,
		backgroundColor: '#676D78',
		justifyContent: 'flex-end',
	},

	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-around',
		//marginBottom: "15%",
	},

	bar : {
		height: "9%",
		width: "100%",
		flexDirection: "column",
		justifyContent: 'space-between',
		elevation : 3,
		backgroundColor : "white",
	},

	barContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		//marginBottom: "15%",
	},

	title: {
		color: "black",
		fontSize: 25,
		fontFamily: "Source Sans Pro",
		paddingVertical: 10,
	},

	IconContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
		paddingVertical: '10%',
	},

	QuestionContainer: {
		flex: 4,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	questionContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	questionTitle: {
		//marginTop: "40%",
		fontSize: 25,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: '#FFFFFF',
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		//marginTop: 10,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		//color: "black",
		paddingVertical: 10,
		//borderRadius: 50,
	},

})
