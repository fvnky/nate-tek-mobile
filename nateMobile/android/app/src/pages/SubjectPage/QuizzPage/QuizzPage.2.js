import React from 'react'
import {
	View,
	Text,
	StyleSheet,
	Image,
	TouchableOpacity,
	CheckBox,
} from 'react-native'
import { NavigationEvents } from 'react-navigation';

export default class LoginPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
            themeValue: false,
            theme: "darkMode",
			currentQuestion: 0,
			points: 0,
			verification: false,
			buttonText: "Submit",
			checkedA: false,
			checkedB: false,
			checkedC: false,
			colorA: '#4A4A4A',
			colorB: '#4A4A4A',
			colorC: '#4A4A4A',
			Quizzes: {
				QUIZZES: [
					{
						questions: [
                        {
                            question: "question 1",
                            answers: [
                              {answer: "loading", correct: false},
                              {answer: "loading", correct: true},
                              {answer: "loading", correct: false}
                            ],
                          },
                          {
                            question: "question 2",
                            answers: [
                              {answer: "loading", correct: true},
                              {answer: "loading", correct: false},
                              {answer: "loading", correct: false}
                            ],
                          },
                          {
                            question: "loading",
                            answers: [
                              {answer: "loading", correct: false},
                              {answer: "loading", correct: false},
                              {answer: "loading", correct: true}
                            ],
                          },
                          {
                            question: "question 4",
                            answers: [
                              {answer: "loading", correct: false},
                              {answer: "loading", correct: true},
                              {answer: "loading", correct: false}
                            ],
                          },
					  ],
					},
					{
						questions: [
                        {
                            question: "question 1",
                            answers: [
                              {answer: "loading", correct: false},
                              {answer: "loading", correct: true},
                              {answer: "loading", correct: false}
                            ],
                          },
                          {
                            question: "question 2",
                            answers: [
                              {answer: "loading", correct: true},
                              {answer: "loading", correct: false},
                              {answer: "loading", correct: false}
                            ],
                          },
                          {
                            question: "loading",
                            answers: [
                              {answer: "loading", correct: false},
                              {answer: "loading", correct: false},
                              {answer: "loading", correct: true}
                            ],
                          },
                          {
                            question: "question 4",
                            answers: [
                              {answer: "loading", correct: false},
                              {answer: "loading", correct: true},
                              {answer: "loading", correct: false}
                            ],
                          },
					  ],
					}
				]
			},
			id: -1,
		}
	}

	// componentWillMount = () => {
	// }

	// getQuizz() {
	// 	this.props.screenProps.get("QUIZZES")
	// 	.then((doc) => {
	// 	  //console.warn("test", doc);
	// 	  this.setState({Quizzes: doc});
	// 	})
	// 	.catch((err) => {
	// 	  console.warn(err)
	// 	});
	// }

	// getTheme() {
	// 	this.props.screenProps.get("theme").then((doc) => {
	// 		this.setState({
	// 		  themeValue: doc.themeValue,
	// 		  theme: doc.theme,
	// 		});
	// 	  }).catch((err) => {
	// 		console.warn('fail');
	// 	  })
	// }

	// componentDidMount = () => {
	// 	setTimeout(() => {
	// 		this.setState({id : this.props.navigation.getParam('id')});
	// 		this.getTheme();
	// 		this.getQuizz();
	// 	}, 1000);
	// }

	async getData() {
		const { screenProps } = this.props;
  
		// console.warn("test");
		await screenProps.getQuizz();
		await screenProps.getTheme();
		if (screenProps.theme !== null) {
			this.setState({
				Quizzes: screenProps.quizz,
				themeValue: screenProps.theme.themeValue,
				id: screenProps.selectedQuizz - 1,
			});
		}
	}

	myCheck = () => {
		this.setState({verification: true, buttonText: "Next"});
		if (this.state.checkedA && !this.state.Quizzes.QUIZZES[this.state.id].questions[this.state.currentQuestion].answers[0].correct) {
			this.setState({colorA: 'red'});
		} else if (this.state.Quizzes.QUIZZES[this.state.id].questions[this.state.currentQuestion].answers[0].correct) {
			this.setState({colorA: 'green'});
			if (this.state.checkedA) {
				this.state.points = this.state.points + 1;
			}
		}
		if (this.state.checkedB && !this.state.Quizzes.QUIZZES[this.state.id].questions[this.state.currentQuestion].answers[1].correct) {
			this.setState({colorB: 'red'});
		} else if (this.state.Quizzes.QUIZZES[this.state.id].questions[this.state.currentQuestion].answers[1].correct) {
			this.setState({colorB: 'green'});
			if (this.state.checkedB) {
				this.state.points = this.state.points + 1;
			}
		}
		if (this.state.checkedC && !this.state.Quizzes.QUIZZES[this.state.id].questions[this.state.currentQuestion].answers[2].correct) {
			this.setState({colorC: 'red'});
		} else if (this.state.Quizzes.QUIZZES[this.state.id].questions[this.state.currentQuestion].answers[2].correct) {
			this.setState({colorC: 'green'});
			if (this.state.checkedC) {
				this.state.points = this.state.points + 1;
			}
		}
	}

	handleSubmit = async () => {
		if (!this.state.verification) {
			this.myCheck();
		} else if (this.state.currentQuestion + 1 >= this.state.Quizzes.QUIZZES[this.state.id].questions.length) {
			this.props.navigation.navigate('QuizzPage3', {points: this.state.points, length: this.state.Quizzes.QUIZZES[this.state.id].questions.length});
		} else {
			this.setState({
				currentQuestion: this.state.currentQuestion + 1,
				verification: false,
				buttonText: "Submit",
				checkedA: false,
				checkedB: false,
				checkedC: false,
				colorA: '#4A4A4A',
				colorB: '#4A4A4A',
				colorC: '#4A4A4A',
			});
		}
	}

	handleAnswer = async () => {

	}

	toggleA = (value) => {
		if (!this.state.verification) {
			this.setState({checkedA: value, checkedB: false, checkedC: false})
		}
	}

	toggleB = (value) => {
		if (!this.state.verification) {
			this.setState({checkedA: false, checkedB: value, checkedC: false})
		}
	}

	toggleC = (value) => {
		if (!this.state.verification) {
			this.setState({checkedA: false, checkedB: false, checkedC: value})
		}
	}

	Questions() {
		if (this.state.id >= 0) {
			return (
				<View style={styles.QuestionContainer}>
					<View style={styles.questionContainer}>
						<Text style={this.state.themeValue ? darkMode.questionTitle : lightMode.questionTitle}>{this.state.Quizzes.QUIZZES[this.state.id].questions[this.state.currentQuestion].question}</Text>
					</View>
					<TouchableOpacity onPress={() => this.toggleA(!this.state.checkedA)} style={{paddingVertical: 10}}>
						<View style={[styles.AnswerContainer, {backgroundColor: this.state.colorA}]}>
							<Text style={styles.LoginButton}>{this.state.Quizzes.QUIZZES[this.state.id].questions[this.state.currentQuestion].answers[0].answer}</Text>
							<CheckBox value={this.state.checkedA} disabled={this.state.verification} onValueChange={() => this.toggleA(!this.state.checkedA)} style={{paddingVertical: 22}}></CheckBox>
						</View>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => this.toggleB(!this.state.checkedB)} style={{paddingVertical: 10}}>
						<View style={[styles.AnswerContainer, {backgroundColor: this.state.colorB}]}>
							<Text style={styles.LoginButton}>{this.state.Quizzes.QUIZZES[this.state.id].questions[this.state.currentQuestion].answers[1].answer}</Text>
							<CheckBox value={this.state.checkedB} disabled={this.state.verification} onValueChange={() => this.toggleB(!this.state.checkedB)} style={{paddingVertical: 22}}></CheckBox>
						</View>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => this.toggleC(!this.state.checkedC)} style={{paddingVertical: 10}}>
						<View style={[styles.AnswerContainer, {backgroundColor: this.state.colorC}]}>
							<Text style={styles.LoginButton}>{this.state.Quizzes.QUIZZES[this.state.id].questions[this.state.currentQuestion].answers[2].answer}</Text>
							<CheckBox value={this.state.checkedC} disabled={this.state.verification} onValueChange={() => this.toggleC(!this.state.checkedC)} style={{paddingVertical: 22}}></CheckBox>
						</View>
					</TouchableOpacity>
				</View>
			);	
		}
	}

	render() {
		// console.warn(this.state.Quizzes);
		// console.warn(this.state.id);
		// this.getTheme();
		// this.getQuizz();
		return (
			<View style={this.state.themeValue ? darkMode.myBackground : lightMode.myBackground}>
		        <NavigationEvents onWillFocus={() => this.getData()} />
		    	<View style={this.state.themeValue ? darkMode.bar : lightMode.bar}>
        			<View style={styles.barContainer}>
          				<Text style={this.state.themeValue ? darkMode.title : lightMode.title}>Question {this.state.currentQuestion + 1}</Text>
        			</View>
      			</View>
				<View style={styles.container}>

					<View style={styles.IconContainer}>
						<Image
							style={styles.NateIcon}
							source={require('../../../assets/placeholderIcon.png')}
						/>
					</View>
					{this.Questions()}
					<View style={{flex: 1}}>
						<TouchableOpacity onPress={this.handleSubmit}>
							<View style={styles.LoginContainer}>
								<Text style={styles.LoginButton}>{this.state.buttonText}</Text>
							</View>
						</TouchableOpacity>
					</View>

				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	myBackground: {
		flex: 1,
		backgroundColor: '#FAFAFA',
		justifyContent: 'flex-end',
	},

	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-around',
		//marginBottom: "15%",
	},

	bar : {
		height: "10%",
		width: "100%",
		flexDirection: "column",
		justifyContent: 'space-between',
		elevation : 3,
		backgroundColor : "white",
	},

	barContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		//marginBottom: "15%",
	},

	title: {
		color: "black",
		fontSize: 25,
		fontFamily: "Source Sans Pro",
		paddingVertical: 10,
	},

	IconContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
		paddingVertical: '10%',
	},

	QuestionContainer: {
		flex: 4,
		flexDirection: 'column',
		justifyContent: 'center',
	},

	questionContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
	},

	questionTitle: {
		//marginTop: "40%",
		fontSize: 25,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: 'grey',
	},

	AnswerContainer: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		//marginTop: 10,
		borderRadius: 75,
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		//marginTop: 10,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		//color: "black",
		paddingVertical: 10,
		//borderRadius: 50,
	},

})

const lightMode = StyleSheet.create({
	myBackground: {
		flex: 1,
		backgroundColor: '#FAFAFA',
		justifyContent: 'flex-end',
	},

	bar : {
		height: "10%",
		width: "100%",
		flexDirection: "column",
		justifyContent: 'space-between',
		elevation : 3,
		backgroundColor : "white",
	},

	title: {
		color: "black",
		fontSize: 25,
		fontFamily: "Source Sans Pro",
		paddingVertical: 10,
	},

	questionTitle: {
		//marginTop: "40%",
		fontSize: 25,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: 'grey',
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		//marginTop: 10,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		//color: "black",
		paddingVertical: 10,
		//borderRadius: 50,
	},
})

const darkMode = StyleSheet.create({
	myBackground: {
		flex: 1,
		backgroundColor: '#676D78',
		justifyContent: 'flex-end',
	},

	bar : {
		height: "10%",
		width: "100%",
		flexDirection: "column",
		justifyContent: 'space-between',
		elevation : 3,
		backgroundColor : "#676D78",
	},

	title: {
		color: "#FFFFFF",
		fontSize: 25,
		fontFamily: "Source Sans Pro",
		paddingVertical: 10,
	},

	questionTitle: {
		//marginTop: "40%",
		fontSize: 25,
		fontFamily: 'open-sans',
		fontWeight: '100',
		color: '#FFFFFF',
	},

	LoginContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginLeft: '15%',
		width: '70%',
		height: 45,
		//marginTop: 10,
		backgroundColor: '#4A4A4A',
		borderRadius: 75,
	},

	LoginButton: {
		fontSize: 16,
		color: '#FAFAFA',
		//color: "black",
		paddingVertical: 10,
		//borderRadius: 50,
	},
})
