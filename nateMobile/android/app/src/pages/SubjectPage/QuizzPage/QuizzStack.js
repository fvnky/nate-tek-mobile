import React from 'react';
import { View } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import QuizzPage1   from './QuizzPage.1'
import QuizzPage2   from './QuizzPage.2'
import QuizzPage3   from './QuizzPage.3'

import TopBar       from  '../../../component/TopBar';

const QuizzStackNavigator = createStackNavigator({
    QuizzPage1      : { screen : QuizzPage1 },
    QuizzPage2      : { screen : QuizzPage2 },
    QuizzPage3      : { screen : QuizzPage3 },
}, { headerMode : 'none' });

const QuizzStack = createAppContainer(QuizzStackNavigator);

export default class LibraryTabNavigatorScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            questions: [
                {
                  question: "question 1",
                  answers: [
                    {answer: "answer a", correct: false},
                    {answer: "answer b", correct: true},
                    {answer: "answer c", correct: false}
                  ],
                },
                {
                  question: "question 2",
                  answers: [
                    {answer: "answer a", correct: true},
                    {answer: "answer b", correct: false},
                    {answer: "answer c", correct: false}
                  ],
                },
                {
                  question: "question 3",
                  answers: [
                    {answer: "answer a", correct: false},
                    {answer: "answer b", correct: false},
                    {answer: "answer c", correct: true}
                  ],
                },
                {
                  question: "question 4",
                  answers: [
                    {answer: "answer a", correct: false},
                    {answer: "answer b", correct: true},
                    {answer: "answer c", correct: false}
                  ],
                },
            ],
            points: 0,
        };
    }
    
    render() {
        return (
            <View style={{ flex:  1 }}>
                <QuizzStack screenProps={this.props.screenProps} />
            </View>
        );
    }
}
