import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, YellowBox } from 'react-native'
import { NavigationEvents } from 'react-navigation';

// darkMode needed do not remove
import { lightMode, darkMode } from './SubjectPage.style';
import SubjectCard from './SubjectCard';
import FolderCard from './FolderCard';
import CourseCard from './CourseCard';

export default class SubjectPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: lightMode,
      themeValue: false,
      SUBJECTS: [],
      FOLDERS: [],
      COURSES: [],
      QUIZZES: [],
      CurrentPath: '',
      setUp: false,
    };
  }

  async getData() {
    this.getCoursesData();
  }

  sendToQuizz() {
    this.props.navigation.navigate('QuizzPage1');
  }

  getCoursesData() {
    this.state.COURSES = this.props.screenProps.courses.FindAll();
    console.warn(`COURSES\n ${this.state.COURSES}`);
  }

  changePath(newPath) {
    this.setState({ CurrentPath: newPath });
  }

  back() {
    this.state.Subjects.SUBJECTS.map((SUBJECT) => {
      if (SUBJECT.onPressPath == this.state.CurrentPath) {
        this.setState({ CurrentPath: SUBJECT.path });
        return;
      }
    });
    this.state.Folders.FOLDERS.map((FOLDER) => {
      if (FOLDER.onPressPath == this.state.CurrentPath) {
        this.setState({ CurrentPath: FOLDER.path });
        return;
      }
    });
  }

  SubjectList() {
    const { SUBJECTS, theme } = this.state;
    return (
      <View style={theme.container}>
        <Text style={theme.SubjectTitle}>My Subject</Text>
        <ScrollView>
          {SUBJECTS.map((SUBJECT) => {
            return (
              <TouchableOpacity style={darkMode.card} onPress={() => this.changePath(SUBJECT.onPressPath)} >
                <SubjectCard theme={this.state.themeValue} title={SUBJECT.name} key={SUBJECT.id}></SubjectCard>
              </TouchableOpacity>
            )
          })}
        </ScrollView>
      </View>
    );
  }

  FolderList() {
    const { FOLDERS, theme } = this.state;
    const FilteredFOLDERS = FOLDERS.filter((FOLDER) => FOLDER.path == this.state.CurrentPath)

    return (
      <View style={theme.container}>
        {FilteredFOLDERS.map(FOLDER => {
          return (
            <TouchableOpacity style={theme.card} onPress={() => this.changePath(FOLDER.onPressPath, FOLDER.name)}>
              <FolderCard theme={this.state.themeValue} title={FOLDER.name}></FolderCard>
            </TouchableOpacity>
          )
        })}
      </View>
    );
  }

  CourseList() {
    const { COURSES, theme } = this.state;
    const FilteredCOURSES = COURSES.filter((COURSE) => COURSE.path == this.state.CurrentPath)

    return (
      <View style={theme.container}>
        {FilteredCOURSES.map(COURSE => {
          return (
            <TouchableOpacity style={theme.card}>
              <CourseCard theme={this.state.themeValue} title={COURSE.name}></CourseCard>
            </TouchableOpacity>
          )
        })}
      </View>
    );
  }

  handleQuizz(id) {
    this.props.screenProps.selectedQuizz = id;
    this.props.navigation.navigate('Quizz', { id: id });
  }

  QuizzesList() {
    const { QUIZZES, theme } = this.state;
    const FilteredQUIZZES = QUIZZES.filter((QUIZZ) => QUIZZ.path == this.state.CurrentPath)

    return (
      <View style={theme.container}>
        {FilteredQUIZZES.map(QUIZZ => {
          return (
            <TouchableOpacity style={theme.card} onPress={() => this.handleQuizz(QUIZZ.id)}>
              <CourseCard theme={this.state.themeValue} title={QUIZZ.name}></CourseCard>
            </TouchableOpacity>
          )
        })}
      </View>
    );
  }


  list() {
    const { theme } = this.state;

    return (
      <View style={theme.container}>
        {this.SubjectList()}
      </View>
    );
  }

  contant() {
    const { theme } = this.state;

    if (this.state.CurrentPath == '') {
      return (
        <View style={theme.container}>
          <View style={theme.container}>
            {this.list()}
          </View>
        </View>
      );
    } else {
      return (
        <View style={theme.container}>
          <TouchableOpacity onPress={() => this.back()}>
            <Text style={theme.SubjectTitle}>{this.state.CurrentPath}</Text>
          </TouchableOpacity>
          <ScrollView>
            {this.FolderList()}
            {this.CourseList()}
            {this.QuizzesList()}
          </ScrollView>
        </View>
      );
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <TouchableOpacity style={theme.buttonContainer} onPress={() => this.handleTest()}>
          <Text style={theme.text}>test</Text>
        </TouchableOpacity>
        <NavigationEvents onWillFocus={() => this.getData()} />
        {this.contant()}
      </View>
    )
  }
}
