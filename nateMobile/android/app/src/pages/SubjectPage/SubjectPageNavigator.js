import { createStackNavigator, createAppContainer } from 'react-navigation';

import SubjectPage  from './SubjectPage'
import QuizzPage1   from  './QuizzPage/QuizzPage.1'

const MainStack = createStackNavigator({
    SubjectPage     : { screen : SubjectPage },
    QuizzPage1      : { screen : QuizzPage1 },
}, { headerMode : 'none' });

export default createAppContainer(MainStack);