import React from 'react';
import { View, StyleSheet } from 'react-native';
import { NavigationEvents } from 'react-navigation';

// darkMode needed do not remove
import TopBar from '../../component/TopBar'

export default class DevPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            themeValue: false,
            theme: "darkMode",
        };
    }

    async getData() {
		const { screenProps } = this.props;
  
		await screenProps.getTheme();
		if (screenProps.theme !== null) {
			this.setState({themeValue: screenProps.theme.themeValue});
		}
	}

    render() {
        return (
            <View style={this.state.themeValue ? darkMode.container : lightMode.container}>
				<NavigationEvents onWillFocus={() => this.getData()} />
                <TopBar theme={this.state.themeValue} navigation={this.props.navigation} title="Todo" />
                <View style={this.state.themeValue ? darkMode.container : lightMode.container}>
                </View>
            </View>
        );
    }
}

const lightMode = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#F2F2F2',
        marginHorizontal: '10%'
    },
    text: {
        color: 'grey',
        paddingVertical: 10,
    },
});

const darkMode = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#676D78',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#4A4A4A',
        marginHorizontal: '10%'
    },
    text: {
        color: '#FFFFFF',
        paddingVertical: 10,
    },
});