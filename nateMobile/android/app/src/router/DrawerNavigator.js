import { Dimensions } from 'react-native';
import { createDrawerNavigator, createAppContainer } from 'react-navigation';

import LibraryTabNavigator from './LibraryTabNavigator';
import ProfileTabNavigator from './ProfileTabNavigator';
import SettingsPage from '../pages/SettingsPage/SettingsPage.js';
import TodoList from '../pages/TodoList/TodoList';
import DevPage from '../pages/DevPage/DevPage.js';

import SideMenuDrawer from './SideMenueDrawer';

const WIDTH = Dimensions.get('window').width;

const DrawerConfig = {
    drawerWidth: WIDTH*0.83,
}

const DrawerNavigator = createDrawerNavigator(
    {
        Library:    { screen: LibraryTabNavigator },
        Profile:    { screen: ProfileTabNavigator },
        Settings:   { screen: SettingsPage },
        TodoList:   { screen: TodoList },
        Dev:        { screen: DevPage},
    },
    {
        contentComponent: SideMenuDrawer,
        DrawerConfig: DrawerConfig,
    }
);

export default createAppContainer(DrawerNavigator);