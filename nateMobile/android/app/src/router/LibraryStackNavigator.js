import { createStackNavigator, createAppContainer } from 'react-navigation';

import LibraryTabNavigatorScreen from './LibraryTabNavigator'

import QuizzStack from '../pages/SubjectPage/QuizzPage/QuizzStack'

const LibraryStackNavigator = createStackNavigator({
    LibraryTabNavigator:    { screen: LibraryTabNavigatorScreen },
    Quizz:      { screen: QuizzStack },
}, { headerMode : 'none' });

export default createAppContainer(LibraryStackNavigator);