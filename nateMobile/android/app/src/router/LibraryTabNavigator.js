import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';

// import TopBar                   from  '../component/TopBar';

import { lightMode, darkMode }  from './TabNavigator.style';
import TopBar                   from  '../component/TabTopBar';

import tpmStackNav              from './tmpStackNav'
import RecentPage               from '../pages/RecentPage/RecentPage';
import DraftPage                from '../pages/DraftPage/DraftPage';

// import LibraryPage              from '../pages/LibraryPage/Library';
// import EditPage                 from '../pages/EditPage/EditPage';

const TabBar = (props) => {
  const {
    navigation,
    getLabelText,
    onTabPress,
    onTabLongPress,
    getAccessibilityLabel,
  } = props;

  const theme = lightMode;
  const themeValue = false;
  const { routes } = navigation.state;

  return (
    <View style={theme.container}>
      <TopBar theme={themeValue}  navigation={navigation} title='NATE'/>
      <View style={theme.row}>
        {routes.map((route) => (
          <TouchableOpacity
            key={route.key}
            style={theme.tabButton}
            onPress={() => { onTabPress({ route }); }}
            onLongPress={() => { onTabLongPress({ route }); }}
            accessibilityLabel={getAccessibilityLabel({ route })}
          >
            <Text style={theme.h1}>{getLabelText({ route })}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  )
}

const TabConfig = {
}

const LibraryTabNavigator = createMaterialTopTabNavigator(
  {
    all:        { screen: tpmStackNav },
    Recent:     { screen: RecentPage },
    Draft:      { screen: DraftPage },
  },
  {
    tabBarComponent: TabBar,
    tabBarOptions: TabConfig,
  }
);

const LibraryTabNav = createAppContainer(LibraryTabNavigator);
export default LibraryTabNav;

// const DarkTabConfig = {
//     tabBarOptions: {
//         activeTintColor: '#FAFAFA',
//         inactiveTintColor: '#FAFAFA',
//         labelStyle: {
//           fontSize: 12,
//         },
//         style: {
//           backgroundColor: '#4A4A4A',
//         },
//     },
// }

// const DarkLibraryTabNavigator = createMaterialTopTabNavigator({
//     all:        { screen: tpmStackNav },
//     Recent:     { screen: RecentPage },
//     Draft:      { screen: DraftPage },
//     },
//     DarkTabConfig
// );

// const DarkTabNav = createAppContainer(DarkLibraryTabNavigator);

// const LightTabConfig = {
//     tabBarOptions: {
//         activeTintColor: '#707070',
//         inactiveTintColor: '#707070',
//         labelStyle: {
//           fontSize: 12,
//         },
//         style: {
//           backgroundColor: '#FFFFFF',
//         },
//     },
// }

// const LightLibraryTabNavigator = createMaterialTopTabNavigator({
//     all:        { screen: tpmStackNav },
//     Recent:     { screen: RecentPage },
//     Draft:      { screen: DraftPage },
//     },
//     LightTabConfig
// );

// const LightTabNav = createAppContainer(LightLibraryTabNavigator);

// export default class LibraryTabNavigatorScreen extends React.Component {
//     constructor(props) {
//       super(props);
//       this.state = {
//         themeValue: false,
//         theme: "darkMode",
//       };
//     }

//     async getData() {
//       const { screenProps } = this.props;
    
//       await screenProps.getTheme();
//       if (screenProps.theme !== null) {
//         this.setState({themeValue: screenProps.theme.themeValue});
//       }
//     }

//     contant() {
//       if (this.state.themeValue) {
//         return (
//             <View style={{ flex:  1 }}>
//                 <TopBar theme={this.state.themeValue} navigation={this.props.navigation} title='NATE'/>
//                 <DarkTabNav screenProps={this.props.screenProps}/>
//             </View>
//         );
//       } else {
//         return (
//             <View style={{ flex:  1 }}>
//                 <TopBar theme={this.state.themeValue} navigation={this.props.navigation} title='NATE'/>
//                 <LightTabNav screenProps={this.props.screenProps}/>
//             </View>
//         );
//       }
//     }
  
//     render() {
//       return (
//         <View style={{ flex:  1 }}>
//           <NavigationEvents onWillFocus={() => this.getData()} />
//           {this.contant()}
//         </View>
//       );
//     }
// }
