import { createStackNavigator, createAppContainer } from 'react-navigation';

import ProfileTabNavigatorScreen from './ProfileTabNavigator'

const SettingsStackNavigator = createStackNavigator({
    ProfileTabNavigator:    { screen: ProfileTabNavigatorScreen },
}, { headerMode : 'none' });

export default createAppContainer(SettingsStackNavigator);