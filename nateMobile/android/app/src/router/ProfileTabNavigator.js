import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';

import { lightMode, darkMode }  from './TabNavigator.style';
import TopBar                   from  '../component/TabTopBar';

import ProfilePage              from '../pages/ProfilePage/Profile';
import EditPage                 from '../pages/EditPage/EditPage';

const TabBar = (props) => {
  const {
    navigation,
    getLabelText,
    onTabPress,
    onTabLongPress,
    getAccessibilityLabel,
  } = props;

  const theme = lightMode;
  const themeValue = false;
  const { routes } = navigation.state;

  return (
    <View style={theme.container}>
      <TopBar theme={themeValue}  navigation={navigation} title='NATE'/>
      <View style={theme.row}>
        {routes.map((route) => (
          <TouchableOpacity
            key={route.key}
            style={theme.tabButton}
            onPress={() => { onTabPress({ route }); }}
            onLongPress={() => { onTabLongPress({ route }); }}
            accessibilityLabel={getAccessibilityLabel({ route })}
          >
            <Text style={theme.h1}>{getLabelText({ route })}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  )
}

const TabConfig = {
}

const ProfileTabNavigator = createMaterialTopTabNavigator({
    Profile:    { screen: ProfilePage },
    Edit:       { screen: EditPage },
    },
    {
      tabBarComponent: TabBar,
      tabBarOptions: TabConfig,
    }
);

const ProfileTabNav = createAppContainer(ProfileTabNavigator);
export default ProfileTabNav;

