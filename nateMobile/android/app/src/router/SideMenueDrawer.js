import React,  { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default class SideMenuDrawer extends Component {
    constructor(props) {
        super(props);
        this.state = {
          themeValue: false,
        };
      }
    
    async getData() {}

	async handleLogOut() {
        const { navigation, screenProps } = this.props;
		await screenProps.user.logOut()
		navigation.navigate('Login1')
	}
        
    navLink (nav, text) {
        const { navigation } = this.props;
        const { themeValue } = this.state;
        
        return (
            <TouchableOpacity style={themeValue ? darkMode.linkContainer : lightMode.linkContainer} onPress={() => navigation.navigate(nav)}>
                <Text style={themeValue ? darkMode.link : lightMode.link}>{text}</Text>
            </TouchableOpacity>
        )
    }

    render () {
        const { themeValue } = this.state

        return (
            <View style={themeValue ? darkMode.container : lightMode.container}>
				<NavigationEvents onWillFocus={() => this.getData()}  />
                <TouchableOpacity style={themeValue ? darkMode.returnBar : lightMode.returnBar} onPress={() => this.handleLogOut()}>
                    <AntDesign style={themeValue ? darkMode.returnIcon : lightMode.returnIcon} name="arrowleft" size={20} color="#FFFFFF" />
                    <Text style={themeValue ? darkMode.returnTitle : lightMode.returnTitle}>log out</Text>
                </TouchableOpacity>
                <View style={themeValue ? darkMode.topLinks : lightMode.topLinks}>
                    <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                    <View style={themeValue ? darkMode.rowContainer : lightMode.rowContainer}>
                        <Image
                            style={themeValue ? darkMode.ProfileIcon : lightMode.ProfileIcon}
                            source={require('../assets/placeholderIcon.png')}
                        />
                    </View>
                    <View style={themeValue ? darkMode.rowContainer : lightMode.rowContainer}>
                        <Text style={themeValue ? darkMode.title : lightMode.title}>Name Hear</Text>
                    </View>
                    </TouchableOpacity>
                </View>
                <View style={themeValue ? darkMode.bottomLinks : lightMode.bottomLinks}>
                    {this.navLink('Library', 'Library')}
                    {this.navLink('Profile', 'Profile')}
                    {this.navLink('Settings', 'Settings')}
                    {this.navLink('TodoList', 'Todo')}
                    {this.navLink('Dev', 'Dev')}
                </View>
            </View>
        )
    }
}

const lightMode = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red',
    },
    returnBar: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        backgroundColor: '#676D78',
        elevation: 10,
        // borderBottomColor: '#000000',
        // borderBottomWidth: 1,
    },
    returnIcon: {
        paddingVertical: 10,
        paddingLeft: 10,
    },
    returnTitle: {
        width: 300,
        color: '#FFFFFF',
        paddingVertical: 10,
        paddingLeft: 10,
    },
        topLinks: {
        flex: 2,
        backgroundColor: '#F2F2F2',
        flexDirection: "column",
        justifyContent: 'center',
    },
    ProfileIcon: {
        borderRadius: 12,
    },
    rowContainer: {
        flexDirection: "row",
        justifyContent: "center",
    },
    title: {
        paddingTop: 20,
        color: '#242424',
    },
    bottomLinks: {
        flex: 3,
        backgroundColor: '#FAFAFA',
        flexDirection: "column",
        justifyContent: 'center',
    },
    linkContainer: {
        height: 75,
        flexDirection: "row",
        justifyContent: 'center',
    },
    link: {
        flex: 1,
        fontSize: 20,
        textAlign: 'center',

    },
});

const darkMode = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red',
    },
    returnBar: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        backgroundColor: '#676D78',
        elevation: 10,
    },
    returnIcon: {
        paddingVertical: 10,
        paddingLeft: 10,
    },
    returnTitle: {
        width: 300,
        color: '#FFFFFF',
        paddingVertical: 10,
        paddingLeft: 10,
    },
    topLinks: {
        flex: 2,
        backgroundColor: '#4A4A4A',
        flexDirection: "column",
        justifyContent: 'center',
    },
    ProfileIcon: {
        borderRadius: 12,
    },
    rowContainer: {
        flexDirection: "row",
        justifyContent: "center",
    },
    title: {
        paddingTop: 20,
        color: '#FFFFFF',
    },
    bottomLinks: {
        flex: 3,
        backgroundColor: '#676D78',
        flexDirection: "column",
        justifyContent: 'center',
    },
    linkContainer: {
        height: 75,
        flexDirection: "row",
        justifyContent: 'center',
    },
    link: {
        flex: 1,
        fontSize: 20,
        textAlign: 'center',
        color: '#FFFFFF',
        paddingTop: 20,
    },
});