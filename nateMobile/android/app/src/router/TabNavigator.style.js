import { StyleSheet } from 'react-native';

export const lightMode = StyleSheet.create({
    container: {
      flexDirection: 'column',
      height: 100,
      elevation: 2,
    },
    row: {
      flexDirection: 'row',
      justifyContent: 'center',
      height: 50,
      backgroundColor: '#ffffff',
    },
    tabButton: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    h1: {
      fontSize: 14,
      color: 'grey',
    },
  });
  
  export const darkMode = StyleSheet.create({
    container: {
      flexDirection: 'column',
      height: 100,
      elevation: 2,
    },
    row: {
      flexDirection: 'row',
      justifyContent: 'center',
      height: 50,
      backgroundColor: '#676D78',
    },
    tabButton: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    h1: {
      fontSize: 14,
      color: '#FFF',
    },
  });
  