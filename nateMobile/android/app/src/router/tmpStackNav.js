import { createStackNavigator, createAppContainer } from 'react-navigation';

import SubjectPage  from '../pages/SubjectPage/SubjectPage';
import QuizzStack   from '../pages/SubjectPage/QuizzPage/QuizzStack';

const tpmStackNav = createStackNavigator({
    SubjectPage:    { screen: SubjectPage },
    Quizz:          { screen: QuizzStack },
}, { headerMode : 'none' });

export default createAppContainer(tpmStackNav);