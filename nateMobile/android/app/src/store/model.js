import {action} from 'easy-peasy'
import Database from '../database'

export default {
    todos:  [
        {
            id: 1,
            title: 'Take out trash',
            completed: true,
        },
        {
            id: 2,
            title: 'Pickup kids from school',
            completed: false,
        },
        {
            id: 3,
            title: 'Dinner with boss',
            completed: false,
        }
    ],
    SUBJECTS: [
        {
            id: 1,
            name: 'Math',
            path: '',
            onPressPath: 'math',
            collection: 'SUBJECT',
        },
        {
            id: 2,
            name: 'geometrie',
            path: '',
            onPressPath: 'geometrie',
            collection: 'SUBJECT',
        },
        {
            id: 3,
            name: 'Physics',
            path: '',
            onPressPath: 'physics',
            collection: 'SUBJECT',
        },
        {
            id: 4,
            name: 'Chimie',
            path: '',
            onPressPath: 'geometrie',
            collection: 'SUBJECT',
        },
        {
            id: 5,
            name: 'Francais',
            path: '',
            onPressPath: 'Francais',
            collection: 'SUBJECT',
        },
        {
            id: 6,
            name: 'Anglais',
            path: '',
            onPressPath: 'anglais',
            collection: 'SUBJECT',
        },
        {
            id: 7,
            name: 'Histoire',
            path: '',
            onPressPath: 'histoire',
            collection: 'SUBJECT',
        },
        {
            id: 8,
            name: 'Geographie',
            path: '',
            onPressPath: 'geographie',
            collection: 'SUBJECT',
        },
    ],
    FOLDERS: [
        {
            id: 1,
            name: 'math semester 1',
            parrentPath: '',
            path: 'math',
            onPressPath: 'math/math semester 1',
            collection: 'FOLDER',
        },
        {
            id: 2,
            name: 'physics semester 1',
            parrentPath: '',
            path: 'physics',
            onPressPath: 'physics/physics semester 1',
            collection: 'FOLDER',
        },
        {
            id: 3,
            name: 'geometrie semester 1',
            parrentPath: '',
            path: 'geometrie',
            onPressPath: 'geometrie/geometrie semester 1',
            collection: 'FOLDER',
        },
        {
            id: 4,
            name: 'revolution francaise',
            parrentPath: '',
            path: 'histoire',
            onPressPath: 'histoire/revolution francaise',
            collection: 'FOLDER',
        },
        {
            id: 5,
            name: 'la grande guerre',
            parrentPath: '',
            path: 'histoire',
            onPressPath: 'histoire/la grande guerre',
            collection: 'FOLDER',
        },
        {
            id: 6,
            name: 'la seconde guerre mondial',
            parrentPath: '',
            path: 'histoire',
            onPressPath: 'histoire/la seconde guerre mondial',
            collection: 'FOLDER',
        },
    ],
    COURSES: [
        {
            id: 1,
            name: 'equations',
            path: 'math/math semester 1',
            collection: 'COURSE',
        },
        {
            id: 2,
            name: 'newton',
            path: 'physics/physics semester 1',
            collection: 'COURSE',
        },
        {
            id: 3,
            name: 'trigonometri',
            path: 'geometrie/geometrie semester 1',
            collection: 'COURSE',
        },
        {
            id: 4,
            name: 'les cause de la revolution',
            path: 'histoire/revolution francaise',
            collection: 'COURSE',
        },
        {
            id: 5,
            name: 'napoleon',
            path: 'histoire/revolution francaise',
            collection: 'COURSE',
        },
        {
            id: 6,
            name: 'verdun',
            path: 'histoire/la grande guerre',
            collection: 'COURSE',
        },

    ],
    QUIZZES: [
        {
            id: 1,
            name: 'equations Quizz',
            path: 'math/math semester 1',
            question: {},
            collection: 'QUIZZ',
        },
        {
            id: 2,
            name: 'newton Quizz',
            path: 'physics/physics semester 1',
            question: {},
            collection: 'QUIZZ',
        },
    ],
    CurrentPath: {
        path: '',
        name: '',
    },
    myDB: {
        db: new Database,
    },
    // Actions
    changePath: action((state, path, name) => {
        state.CurrentPath.path = path;
        state.CurrentPath.name = name;
        // console.warn(state.CurrentPath.path);
        // console.warn(state.CurrentPath.name);
        // console.warn(name);
    }),
    back: action((state) => {
        state.SUBJECTS.map((SUBJECT) => {
            if (SUBJECT.onPressPath == state.CurrentPath.path) {
                state.CurrentPath.path = SUBJECT.path;
                return ;
            }
        });
        state.FOLDERS.map((FOLDER) => {
            if (FOLDER.onPressPath == state.CurrentPath.path) {
                state.CurrentPath.path = FOLDER.path;
                return ;
            }
        });
    }),
    init: action((state) => {
        state.myDB.db.init();
    }),
    sendValidation: action((state, login) => {
        state.myDB.db.user.sendValidation(login)
        .then(() => console.log('done'))
        .catch((err) => console.log(err));
    }),
}