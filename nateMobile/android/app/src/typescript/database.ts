import { IDbDocument, ISubject, IFolder, ICourse, IUser } from "./collections";

export interface IDatabase {
   subjects: {
      findAll: FindAll,
      findById: FindSubjectById,
      create: CreateSubject,
      update: UpdateSubject,
      remove: RemoveSubject
   };
   folders: {
      findById: FindFolderById,
      create: CreateFolder,
      update: UpdateFolder,
      remove: RemoveFolder
   };
   courses: {
      findById: FindCourseById,
      create: CreateCourse,
      update: UpdateCourse,
      remove: RemoveCourse
   };
   _main: {
      get: GetId;
      put: any; // (doc: IDbDocument) => Promise<IDbDocument>;
   };
   init: any;
}

// _main
export type GetId = (id: string) => Promise<IDbDocument>;

// subjects
export type FindAll = () => Promise<ISubject[]>;
export type FindSubjectById = (id: string) => Promise<ISubject>;
export type CreateSubject = (newSubject: {
   name: string,
   icon: string | null
}) => Promise<ISubject>;
export type UpdateSubject = (toUpdate: ISubject) => Promise<ISubject>;
export type RemoveSubject = (toRemove: ISubject) => Promise<ISubject>;

// folders
export type FindFolderById = (id: string) => Promise<IFolder>;
export type CreateFolder = (newFolder: {
   name: string,
   parent: { id: string, collection: string }
}) => Promise<IFolder>;
export type UpdateFolder = (toUpdate: IFolder) => Promise<IFolder>;
export type RemoveFolder = (toRemove: IFolder) => Promise<IFolder>;

// courses
export type FindCourseById = (id: string) => Promise<ICourse>;
export type CreateCourse = (newCourse: {
   name: string,
   parent: { id: string, collection: string }
}, content?: Object) => Promise<ICourse>;
export type UpdateCourse = (toUpdate: ICourse, content?: Object) => Promise<ICourse>;
export type RemoveCourse = (toRemove: ICourse) => Promise<ICourse>;

// user
export type UpdateUser = (toUpdate: IUser) => Promise<IUser>;

/*
export interface INewFolder {
   name: string;
   parent: IParent;
}
*/

// export type CreateCourse = (newCourse: {
//    name: string,
//    folderId: string
// }) => Promise<ICourse>;
// export type CreateFolder = (newFolder: INewFolder) => Promise<IFolder>;
