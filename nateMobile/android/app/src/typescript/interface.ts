export {
   IDatabase,
   GetId,
   FindAll,
   CreateSubject,
   UpdateSubject,
   RemoveSubject,
   CreateFolder,
   UpdateFolder,
   RemoveFolder,
   FindCourseById,
   CreateCourse,
   UpdateCourse,
   RemoveCourse,
   UpdateUser
} from './database';
export {
   IParent,
   ISubject,
   IFolder,
   ICourse,
   IDraft,
   IDbDocument,
   IUser,
   IReducerUser
} from './collections';

export interface IMatchParams {
   [key: string]: string;
}

export type EditorTab = {
   status: string;
   id: string;
   temporaryName: string;
   value?: any;
   pdfViewer: { file: string; page: number };
};
export type ToastOptions = {
   text: string;
   iconName?: string;
   timeout?: number;
};
